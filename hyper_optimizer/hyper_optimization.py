import os
from functools import partial
import typing as typ
import optuna

from MSSR.multi_scale_SR import MultiScaleSR
from MSSR.utils import similarity_on_hypersphere, feature_generation, overlapping_feature_generation
from distributions.hyperspherical_uniform import HypersphericalUniform
from distributions.von_mises_fisher import VonMisesFisher
from math_utils.surprise_framework import SurpriseFramework
from tests.MSSR.tests_parameters import structural_parameters_uniform_one_hot, \
    structural_parameters_uniformHypersphere_1, structural_parameters_uniformHypersphere_low_D
import numpy as np
import tensorflow_probability as tfp
import sys


def hp_objective_feature_recovery(trial: optuna.Trial, structural_parameters: dict,
                                  surprise_framework: typ.Optional[SurpriseFramework]):
    distribution = structural_parameters['distribution']
    feature_type = structural_parameters['feature_type']
    number_of_features = structural_parameters['number_of_features']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = structural_parameters['features_dim']
    number_of_batch = structural_parameters['number_of_batch']
    batch_size = structural_parameters['batch_size']

    t_min = trial.suggest_loguniform('t_min', 1e-3, 1)
    t_btw = trial.suggest_loguniform('t_btw', 1e-3, 1)
    learning_rate = trial.suggest_loguniform('learning_rate', 1e-5, 1e-1)
    precision = trial.suggest_int('precision', 1, 32)

    mssr = MultiScaleSR(features_dim,
                        number_of_windows,
                        t_min, t_btw,
                        learning_rate, precision,
                        )
    possible_features, p = feature_generation(distribution,
                                              feature_type,
                                              number_of_features,
                                              features_dim)
    signal = []
    indices = []
    for i in range(number_of_batch):
        idx = np.random.choice(list(range(number_of_features)), batch_size, p=p)
        batch = possible_features[idx]
        indices.extend(idx)
        signal.extend(batch)
        if surprise_framework is not None:
            surprise_coef = surprise_framework.surprise_score(batch, processed_batch=True)
            mssr.process_sample_batch(batch, surprise_coef)
            surprise_framework.process_batch(inplace=True, mean_batch=batch)
        else:
            mssr.process_sample_batch(batch)
    mssr.set_episode()
    memory = list(reversed(mssr.last_memory_status))
    memory = np.array(memory)
    signal = np.array(signal)
    return similarity_on_hypersphere(input_signal=signal[:-1], output_signal=memory)


def hp_objective_recovery_overlapping(trial: optuna.Trial, structural_parameters: dict):
    distribution = structural_parameters['distribution']
    number_of_one = structural_parameters['number_of_one']
    space_power = structural_parameters['space_power']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = 2 ** space_power * number_of_one
    number_of_batch = structural_parameters['number_of_batch']
    batch_size = structural_parameters['batch_size']

    t_min = trial.suggest_loguniform('t_min', 1e-3, 1)
    t_btw = trial.suggest_loguniform('t_btw', 1e-3, 1)
    learning_rate = trial.suggest_loguniform('learning_rate', 1e-5, 1e-1)
    precision = trial.suggest_int('precision', 1, 32)

    mssr = MultiScaleSR(features_dim,
                        number_of_windows,
                        t_min, t_btw,
                        learning_rate, precision,
                        )
    possible_features, p = overlapping_feature_generation(distribution, number_of_one, space_power)

    signal = []
    indices = []
    self_content_score = np.ones(shape=(batch_size,))
    for i in range(number_of_batch):
        idx = np.random.choice(list(range(len(possible_features))), batch_size, p=p)
        batch = possible_features[idx]
        indices.extend(idx)
        signal.extend(batch)

        mssr.process_sample_batch(batch, self_content_score)
    mssr.set_episode()
    memory = list(reversed(mssr.last_memory_status))
    memory = np.array(memory)
    signal = np.array(signal)
    return similarity_on_hypersphere(input_signal=signal[:-1], output_signal=memory)


def hp_objective_recovery_mixture_hypersphere(trial: optuna.Trial, structural_parameters: dict):
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = structural_parameters['features_dim']
    number_of_batch = structural_parameters['number_of_batch']
    batch_size = structural_parameters['batch_size']
    min_sample, max_sample = structural_parameters['min_max_uniform_sample']
    number_of_mixtures = structural_parameters['number_of_mixtures']

    t_min = trial.suggest_loguniform('t_min', 1e-3, 1)
    t_btw = trial.suggest_loguniform('t_btw', 1e-3, 1)
    learning_rate = trial.suggest_loguniform('learning_rate', 1e-5, 1e-1)
    precision = trial.suggest_int('precision', 1, 32)

    mssr = MultiScaleSR(features_dim,
                        number_of_windows,
                        t_min, t_btw,
                        learning_rate, precision,
                        )

    mean_vector = []
    concentration_vector = []
    exp_rate = np.random.uniform(sys.float_info.epsilon, 1)

    uniform_distribution = HypersphericalUniform(features_dim - 1)

    for i in range(number_of_mixtures):
        number_of_sample = np.random.randint(min_sample, max_sample + 1)
        samples = uniform_distribution.sample(sample_shape=(number_of_sample,))
        samples = samples.numpy()
        mean = np.mean(samples, axis=0, dtype=np.float32)
        mean_norm = np.linalg.norm(mean)
        mean_norm_square = mean_norm ** 2
        mean /= mean_norm
        concentration = mean_norm * (features_dim - mean_norm_square) / (1 - mean_norm_square)
        mean_vector.append(mean)
        concentration_vector.append(concentration)
    concentration_vector = np.array(concentration_vector, dtype=np.float32)
    exp_distribution = tfp.distributions.Exponential(rate=exp_rate)
    mixture_proba = exp_distribution.sample(sample_shape=(number_of_mixtures,))
    mixture_proba /= np.sum(mixture_proba)
    mixture_components = []
    for i in range(number_of_mixtures):
        mixture_components.append(VonMisesFisher(mean_vector[i], [concentration_vector[i]]))
    categorical_distribution = tfp.distributions.Categorical(probs=mixture_proba)
    # mixture_distribution = tfp.distributions.Mixture(cat=categorical_distribution, components=mixture_components)
    signal = []
    self_content_score = np.ones(shape=(batch_size,))
    mixture_indice = (categorical_distribution.sample(sample_shape=(1,))).numpy()[0]
    print(mixture_indice)
    for i in range(number_of_batch):
        print(f'Batch N-{i + 1} to {number_of_batch}')
        batch = mixture_components[mixture_indice].sample(sample_shape=(batch_size,)).numpy()
        batch = batch.squeeze()
        signal.extend(batch)
        mssr.process_sample_batch(batch, self_content_score, False)
        # change mixture
        mixture_indice = (categorical_distribution.sample(sample_shape=(1,))).numpy()[0]

    mssr.set_episode(signal[0])
    memory = list(reversed(mssr.last_memory_status))
    memory = np.array(memory)
    signal = np.array(signal)
    return similarity_on_hypersphere(input_signal=signal, output_signal=memory)


def hp_objective_recovery_uniform_hypersphere(trial: optuna.Trial, structural_parameters: dict):
    # train signal reconstruction from feature on the uniform hypersphere
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = structural_parameters['features_dim']
    number_of_batch = structural_parameters['number_of_batch']
    batch_size = structural_parameters['batch_size']

    t_min = trial.suggest_loguniform('t_min', 1e-3, 1)
    t_btw = trial.suggest_loguniform('t_btw', 1e-3, 1)
    learning_rate = trial.suggest_loguniform('learning_rate', 1e-5, 1e-1)
    precision = trial.suggest_int('precision', 1, 32)

    mssr = MultiScaleSR(features_dim,
                        number_of_windows,
                        t_min, t_btw,
                        learning_rate, precision,
                        )
    distribution = HypersphericalUniform(features_dim - 1)
    signal = []
    self_content_score = np.ones(shape=(batch_size,))
    for i in range(number_of_batch):
        print(f'Batch N-{i + 1} to {number_of_batch}')
        batch = distribution.sample(sample_shape=(batch_size,))
        batch = batch.numpy()
        signal.extend(batch)
        mssr.process_sample_batch(batch, self_content_score, False)

    mssr.set_episode(signal[0])
    memory = list(reversed(mssr.last_memory_status))
    memory = np.array(memory)
    signal = np.array(signal)
    return similarity_on_hypersphere(input_signal=signal[:-1], output_signal=memory)


def create_MSSR_study(study_db: str, study_name: str, objective_function,
                      number_of_trial: int, mssr_parameters: dict):
    # Handle whitechars, '/' trailing
    # and name separated by space problems
    study_db = (study_db.strip()).strip('/')
    study_db = study_db.replace(' ', '_')

    study_dir = './db/' + study_db
    abs_path = os.path.abspath(study_dir)

    objective = partial(objective_function,
                        structural_parameters=mssr_parameters)
    study = optuna.create_study(study_name=study_name,
                                storage=f'sqlite:///{abs_path}.db',
                                load_if_exists=True)
    study.optimize(objective,
                   n_trials=number_of_trial)
    return study.best_params, study.best_value


def get_best_results_from_study(study_db: str = '',
                                study_name: str = ''):
    if study_db == '' or study_name == '':
        raise ValueError('study database and study name must be defined')
    # Handle whitechars, '/' trailing
    # and name separated by space problems
    study_db = (study_db.strip()).strip('/')
    study_db = study_db.replace(' ', '_')

    study_dir = './db/' + study_db
    abs_path = os.path.abspath(study_dir)
    study = optuna.create_study(study_name=study_name,
                                storage=f'sqlite:///{abs_path}.db',
                                load_if_exists=True)
    return study.best_params, study.best_value


if __name__ == '__main__':
    # create_MSSR_study(study_db='test_hd', study_name='uniform_128d_20f_one_hot',
    #                   objective_function=hp_objective_feature_recovery, number_of_trial=200,
    #                   mssr_parameters=structural_parameters_uniform_one_hot)
    create_MSSR_study(study_db='test_low_D', study_name='hypersphere_mixture_10d_100b_128bs_v1',
                      objective_function=hp_objective_recovery_mixture_hypersphere, number_of_trial=200,
                      mssr_parameters=structural_parameters_uniformHypersphere_low_D)
    # # test_signal_recovery_with_HP(structural_parameters_uniform_sparse, hp_uniform_sparse_hd)
    # # create_MSSR_study(study_db='test_hd', study_name="overlapping_logarithmic",
    #                   objective_function=hp_objective_recovery_overlapping, number_of_trial=200,
    #                   mssr_parameters=structural_parameters_logarithmic_overlapping)
    best_params, best_value = get_best_results_from_study('test_hd', 'uniform_128d_20f_one_hot')
    print(best_params)
    print(best_value)
