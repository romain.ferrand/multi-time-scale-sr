from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
from tensorflow.keras.regularizers import L1L2


class DenseSAEncoder(layers.Layer):

    def __init__(self, h_dim_layers=None, z_dim=128,
                 activation='tanh', sparse=False, name="encoder", **kwargs):
        super(DenseSAEncoder, self).__init__(name, **kwargs)
        if h_dim_layers is None:
            h_dim_layers = []
        self.encode_layers = []
        for i, layer_h_dim in enumerate(h_dim_layers):
            self.encode_layers.append(layers.Dense(units=layer_h_dim, name=f'encode_h{i}', activation=activation,
                                                   kernel_initializer='orthogonal', dtype=tf.float32))
        self.sphere_encoder = layers.Dense(
            units=z_dim, name='sphere',
            activation=lambda _x: tf.nn.l2_normalize(_x, axis=-1),
            kernel_initializer='orthogonal', dtype=tf.float32,
            activity_regularizer=(L1L2(0.01, 0) if sparse else None))

    def call(self, inputs, **kwargs):
        enc_output = inputs
        for layer in self.encode_layers:
            enc_output = layer(enc_output, **kwargs)
        z = self.sphere_encoder(enc_output, **kwargs)
        return z


class DenseSADecoder(layers.Layer):

    def __init__(self, output_dim, h_dim_layers=None,
                 activation='tanh', name="decoder", **kwargs):
        super(DenseSADecoder, self).__init__(name, **kwargs)
        if h_dim_layers is None:
            h_dim_layers = []
        self.decode_layers = []
        for i, layer_h_dim in enumerate(h_dim_layers):
            self.decode_layers.append(
                tf.keras.layers.Dense(
                    units=layer_h_dim,
                    name=f'decode_h{len(h_dim_layers) - i - 1}',
                    kernel_initializer='orthogonal',
                    activation=activation, dtype=tf.float32)
            )
        self.logits = tf.keras.layers.Dense(
            units=output_dim, name='logits',
            kernel_initializer='orthogonal',
            activation=None, dtype=tf.float32
        )

    def call(self, z, **kwargs):
        output_decode = z
        for layer in self.decode_layers:
            output_decode = layer(output_decode, **kwargs)
        return self.logits(output_decode, **kwargs)
