from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.initializers import Constant

import tensorflow as tf

from distributions.hyperspherical_uniform import HypersphericalUniform
from distributions.von_mises_fisher import VonMisesFisher


class DenseSVAEncoder(layers.Layer):
    """Maps observation vector to (z_mean, z_log_var, z)."""

    def __init__(self, h_dim_layers=None,
                 z_dim=128, activation='tanh',
                 name="encoder", log_var_bias=1, **kwargs):
        super(DenseSVAEncoder, self).__init__(name, **kwargs)
        if h_dim_layers is None:
            h_dim_layers = []
        self.encode_layers = []
        for i, layer_h_dim in enumerate(h_dim_layers):
            self.encode_layers.append(layers.Dense(units=layer_h_dim, name=f'encode_h{i}', activation=activation,
                                                   kernel_initializer='orthogonal', dtype=tf.float32))
        self.dense_mean = layers.Dense(
            units=z_dim, name='dense_mean',
            activation=lambda _x: tf.nn.l2_normalize(_x, axis=-1),
            kernel_initializer='orthogonal', dtype=tf.float32)
        self.dense_log_var = tf.keras.layers.Dense(
            units=1, name='dense_log_var',
            kernel_initializer='orthogonal',
            activation=tf.nn.softplus,
            bias_initializer=Constant(log_var_bias)
        )
        self.q_z = None

    def call(self, inputs, **kwargs):
        enc_output = inputs
        for layer in self.encode_layers:
            enc_output = layer(enc_output, **kwargs)
        z_mean = self.dense_mean(enc_output, **kwargs)
        z_log_var = self.dense_log_var(enc_output, **kwargs)
        z_log_var += 1
        self.q_z = VonMisesFisher(z_mean, z_log_var)
        z = self.q_z.sample()
        return z_mean, z_log_var, z


class DenseSVADecoder(layers.Layer):
    """Maps observation vector to (z_mean, z_log_var, z)."""

    def __init__(self, output_dim, h_dim_layers=None, activation='tanh', name="decoder", **kwargs):
        super(DenseSVADecoder, self).__init__(name, **kwargs)
        if h_dim_layers is None:
            h_dim_layers = []
        self.decode_layers = []
        for i, layer_h_dim in enumerate(h_dim_layers):
            self.decode_layers.append(tf.keras.layers.Dense(units=layer_h_dim,
                                                            name=f'decode_h{len(h_dim_layers) - i - 1}',
                                                            kernel_initializer='orthogonal',
                                                            activation=activation, dtype=tf.float32))
        self.logits = tf.keras.layers.Dense(units=output_dim, name='logits',
                                            kernel_initializer='orthogonal',
                                            activation=None, dtype=tf.float32)

    def call(self, z, **kwargs):
        output_decode = z
        for layer in self.decode_layers:
            output_decode = layer(output_decode, **kwargs)
        return self.logits(output_decode, **kwargs)


class DenseSVAE(keras.Model):
    def __init__(self, inputs_dim, h_dim, z_dim, activation='relu', name="dense_SVAE", *args, **kwargs):
        """
        Combines the encoder and decoder into an end-to-end model for training.
        :param inputs_dim: inputs shape
        :param h_dim: dimension of the hidden layers
        :param z_dim: dimension of the latent representation
        :param name: name of the model
        :param activation: callable activation function or string
        :param distribution: string either `normal` or `vmf`,
        indicates which distribution to use
        """
        super(DenseSVAE, self).__init__(name=name, *args, **kwargs)
        self.inputs_dim, self.h_dim, self.z_dim, self.activation = \
            inputs_dim, h_dim, z_dim, activation
        self.encoder = DenseSVAEncoder(h_dim=self.h_dim, z_dim=self.z_dim, **kwargs)
        self.decoder = DenseSVADecoder(output_dim=inputs_dim, h_dim=self.h_dim, **kwargs)
        self.q_z = self.encoder.q_z
        self.p_z = HypersphericalUniform(self.z_dim - 1, dtype=tf.float32)

    def call(self, inputs, training=None, mask=None):
        self.p_z = HypersphericalUniform(self.z_dim - 1, dtype=inputs.dtype)
        z_mean, z_log_var, z = self.encoder(inputs)
        self.q_z = self.encoder.q_z
        reconstructed = self.decoder(z)
        kl_loss = - self.q_z.kl_divergence(self.p_z)
        self.add_loss(kl_loss)
        return reconstructed, z, z_mean, z_log_var
