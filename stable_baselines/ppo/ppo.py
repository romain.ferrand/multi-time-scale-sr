import datetime
import typing as tp
from typing import Union

import gym
import numpy as np
import tensorflow as tf
import time
from gym import spaces
from tensorflow.keras.optimizers import Adam

from stable_baselines.common import logger
from stable_baselines.common.base_class import BaseRLModel, SetVerbosity
from stable_baselines.common.buffers import RolloutBuffer
from stable_baselines.common.math_util import safe_mean
from stable_baselines.common.policies import ActorCriticPolicy, RecurrentActorCriticPolicy
from stable_baselines.common.runners import AbstractEnvRunner
from stable_baselines.common.schedules import get_schedule_fn
from stable_baselines.common.utils import explained_variance
from stable_baselines.common.vec_env import SubprocVecEnv, DummyVecEnv


class PPO(BaseRLModel):
    """
    Proximal Policy Optimization algorithm (PPO) (clip version)

    Paper: https://arxiv.org/abs/1707.06347
    Code: This implementation borrows code from OpenAI spinning-up (https://github.com/openai/spinningup/)
    https://github.com/ikostrikov/pytorch-a2c-ppo-acktr-gail and
    and Stable Baselines (PPO2 from https://github.com/hill-a/stable-baselines)

    Introduction to PPO: https://spinningup.openai.com/en/latest/algorithms/ppo.html

    :param policy: (PPOPolicy or str) The policy model to use (MlpPolicy, CnnPolicy, ...)
    :param env: (Gym environment or str) The environment to learn from (if registered in Gym, can be str)
    :param learning_rate: (float or callable) The learning rate, it can be a function
        of the current progress (from 1 to 0)
    :param n_steps: (int) The number of steps to run for each environment per update
        (i.e. batch size is n_steps * n_env where n_env is number of environment copies running in parallel)
    :param batch_size: (int) Mini-batch size
    :param n_epochs: (int) Number of epoch when optimizing the surrogate loss
    :param gamma: (float) Discount factor
    :param lam: (float) Factor for trade-off of bias vs variance for Generalized Advantage Estimator
    :param clip_range: (float or callable) Clipping parameter, it can be a function of the current progress
        (from 1 to 0).
    :param clip_range_vf: (float or callable) Clipping parameter for the value function,
        it can be a function of the current progress (from 1 to 0).
        This is a parameter specific to the OpenAI implementation. If None is passed (default),
        no clipping will be done on the value function.
        IMPORTANT: this clipping depends on the reward scaling.
    :param ent_coef: (float) Entropy coefficient for the loss calculation
    :param vf_coef: (float) Value function coefficient for the loss calculation
    :param max_grad_norm: (float) The maximum value for the gradient clipping
    :param target_kl: (float) Limit the KL divergence between updates,
        because the clipping is not enough to prevent large update
        see issue #213 (cf https://github.com/hill-a/stable-baselines/issues/213)
        By default, there is no limit on the kl div.
    :param tensorboard_log: (str) the log location for tensorboard (if None, no logging)
    :param create_eval_env: (bool) Whether to create a second environment that will be
        used for evaluating the agent periodically. (Only available when passing string for the environment)
    :param policy_kwargs: (dict) additional arguments to be passed to the policy on creation
    :param verbose: (int) the verbosity level: 0 none, 1 training information, 2 tensorflow debug
    :param seed: (int) Seed for the pseudo random generators
    :param _init_setup_model: (bool) Whether or not to build the network at the creation of the instance
    """

    def action_probability(self, observation, state=None, mask=None, actions=None, logp=False):
        pass

    def set_env(self, env):
        """
    Proximal Policy Optimization algorithm (PPO) (clip version)

    Paper: https://arxiv.org/abs/1707.06347
    Code: This implementation borrows code from OpenAI spinning-up (https://github.com/openai/spinningup/)
    https://github.com/ikostrikov/pytorch-a2c-ppo-acktr-gail and
    and Stable Baselines (PPO2 from https://github.com/hill-a/stable-baselines)

    Introduction to PPO: https://spinningup.openai.com/en/latest/algorithms/ppo.html

    :param policy: (PPOPolicy or str) The policy model to use (MlpPolicy, CnnPolicy, ...)
    :param env: (Gym environment or str) The environment to learn from (if registered in Gym, can be str)
    :param learning_rate: (float or callable) The learning rate, it can be a function
        of the current progress (from 1 to 0)
    :param n_steps: (int) The number of steps to run for each environment per update
        (i.e. batch size is n_steps * n_env where n_env is number of environment copies running in parallel)
    :param batch_size: (int) Mini-batch size
    :param n_epochs: (int) Number of epoch when optimizing the surrogate loss
    :param gamma: (float) Discount factor
    :param lam: (float) Factor for trade-off of bias vs variance for Generalized Advantage Estimator
    :param clip_range: (float or callable) Clipping parameter, it can be a function of the current progress
        (from 1 to 0).
    :param clip_range_vf: (float or callable) Clipping parameter for the value function,
        it can be a function of the current progress (from 1 to 0).
        This is a parameter specific to the OpenAI implementation. If None is passed (default),
        no clipping will be done on the value function.
        IMPORTANT: this clipping depends on the reward scaling.
    :param ent_coef: (float) Entropy coefficient for the loss calculation
    :param vf_coef: (float) Value function coefficient for the loss calculation
    :param max_grad_norm: (float) The maximum value for the gradient clipping
    :param target_kl: (float) Limit the KL divergence between updates,
        because the clipping is not enofrom stable_baselines.ppo.policies import PPOPolicy
ugh to prevent large update
        see issue #213 (cf https://github.com/hill-a/stable-baselines/issues/213)
        By default, there is no limit on the kl div.
    :param tensorboard_log: (str) the log location for tensorboard (if None, no logging)
    :param create_eval_env: (bool) Whether to create a second environment that will be
        used for evaluating the agent periodically. (Only available when passing string for the environment)
    :param policy_kwargs: (dict) additional arguments to be passed to the policy on creation
    :param verbose: (int) the verbosity level: 0 none, 1 training information, 2 tensorflow debug
    :param seed: (int) Seed for the pseudo random generators
    :param _init_setup_model: (bool) Whether or not to build the network at the creation of the instance
    """
        pass

    def __init__(self, policy: Union[ActorCriticPolicy, str], env: tp.Union[SubprocVecEnv, DummyVecEnv],
                 gamma: float = 0.99, n_steps: int = 512, ent_coef: float = 0,
                 learning_rate: Union[float, tp.Callable] = 3e-4,
                 vf_coef: float = 0.01, max_grad_norm: tp.Optional[float] = None, lam: float = 0.98,  # lam alias
                 n_mini_batches: int = 32, n_opt_epochs: int = 2, clip_range: Union[float, tp.Callable] = 0.2,
                 clip_range_vf: Union[float, tp.Callable] = None, verbose: int = 0, log_dir: str = "PPO",
                 checkpoint_dir: str = "PPO", _init_setup_model: bool = True, policy_kwargs: tp.Dict = None,
                 seed: int = 0, target_kl: float = None, create_eval_env: bool = False, requires_vec_env=False):

        self.learning_rate = learning_rate
        self.clip_range = clip_range
        self.clip_range_vf = clip_range_vf
        self.n_steps = n_steps
        self.ent_coef = ent_coef
        self.vf_coef = vf_coef
        self.max_grad_norm = max_grad_norm
        self.gamma = gamma
        self.lam = lam
        self.n_mini_batches = n_mini_batches
        self.n_opt_epochs = n_opt_epochs

        self.rollout_buffer = None
        self.target_kl = target_kl

        self.tb_writer = None
        self.entropy = None
        self.vf_loss = None
        self.pg_loss = None
        self.approx_kl = None
        self.clip_frac = None
        self.train_model = None
        self.act_model = None
        self.value = None
        self.n_batch = None
        self.summary = None
        self.optimizer = None

        super(PPO, self).__init__(policy=policy, env=env, policy_base=ActorCriticPolicy, policy_kwargs=policy_kwargs,
                                  verbose=verbose, seed=seed, log_dir=log_dir, checkpoint_dir=checkpoint_dir,
                                  requires_vec_env=requires_vec_env)
        if _init_setup_model:
            self.setup_model()
            self.runner = self._make_runner()

    def _make_runner(self):
        return Runner(env=self.env, model=self, n_steps=self.n_steps,
                      gamma=self.gamma, lam=self.lam)

    def setup_model(self):
        with SetVerbosity(self.verbose):
            assert issubclass(self.policy, ActorCriticPolicy), "Error: the input policy for the PPO2 model must be " \
                                                               "an instance of common.policies.ActorCriticPolicy."

            self.n_batch = self.n_envs * self.n_steps
            self._setup_learning_rate()
            self.set_random_seed(self.seed)

            n_batch_step = None
            n_batch_train = None
            if issubclass(self.policy, RecurrentActorCriticPolicy):
                assert self.n_envs % self.n_mini_batches == 0, "For recurrent policies, " \
                                                               "the number of environments " \
                                                               "run in parallel should be a " \
                                                               "multiple of n_mini_batches. "
                n_batch_step = self.n_envs
                n_batch_train = self.n_batch // self.n_mini_batches

            act_model = self.policy(self.observation_space, self.action_space, self.n_envs, 1,
                                    n_batch_step, **self.policy_kwargs)
            train_model = self.policy(self.observation_space, self.action_space,
                                      self.n_envs // self.n_mini_batches, self.n_steps, n_batch_train,
                                      **self.policy_kwargs)

            self.train_model = train_model
            self.act_model = act_model
            self.step = act_model.call
            self.proba_step = act_model.proba_step
            self.value = act_model.value
            self.initial_state = act_model.initial_state

            self.clip_range = get_schedule_fn(self.clip_range)
            if self.clip_range_vf is not None:
                self.clip_range_vf = get_schedule_fn(self.clip_range_vf)
            if self.optimizer is None:
                self.optimizer = Adam(learning_rate=self.learning_rate, epsilon=1e-5)

    def predict(self, observation, state=None, mask=None, deterministic=False):
        """
        Get the model's action from an observation

        :param observation: (np.ndarray) the input observation
        :param state: (np.ndarray) The last states (can be None, used in recurrent policies)
        :param mask: (np.ndarray) The last masks (can be None, used in recurrent policies)
        :param deterministic: (bool) Whether or not to return deterministic actions.
        :return: (np.ndarray, np.ndarray) the model's action and the next state (used in recurrent policies)
        """
        clipped_actions = self.step(observation, deterministic=deterministic)
        if isinstance(self.action_space, gym.spaces.Box):
            clipped_actions = np.clip(clipped_actions, self.action_space.low, self.action_space.high)
        return clipped_actions

    @tf.function
    def policy_loss(self, advantage, log_prob, old_log_prob, clip_range):
        # Normalize advantage
        advantage = (advantage - tf.reduce_mean(advantage)) / (tf.math.reduce_std(advantage) + 1e-8)
        # ratio between old and new policy, should be one at the first iteration
        ratio = tf.exp(log_prob - old_log_prob)
        # clipped surrogate loss
        policy_loss_1 = advantage * ratio
        policy_loss_2 = advantage * tf.clip_by_value(ratio, 1 - clip_range, 1 + clip_range)
        _loss = -tf.reduce_mean(tf.minimum(policy_loss_1, policy_loss_2))
        return _loss

    @tf.function
    def value_loss(self, values, old_values, return_batch, clip_range_vf):
        if clip_range_vf is None:
            # No clipping
            values_pred = values
        else:
            # Clip the different between old and new value
            # NOTE: this depends on the reward scaling
            values_pred = old_values + tf.clip_by_value(values - old_values, -clip_range_vf, clip_range_vf)
        # Value loss using the TD(lam) target
        _loss = tf.keras.losses.MSE(return_batch, values_pred)
        return _loss

    def train(self, gradient_steps, buffer, batch_size, mb_states, update_step):
        # Update optimizer learning rate
        self._update_learning_rate(self.optimizer, update_step)

        # Compute current clip range
        clip_range = self.clip_range(update_step)
        if self.clip_range_vf is not None:
            clip_range_vf = self.clip_range_vf(update_step)
        else:
            clip_range_vf = None

        for gradient_step in range(gradient_steps):
            approx_kl_divs = []
            # Sample replay buffer
            for replay_data in buffer.__iter__(batch_size, recurrent=True):
                # Unpack
                obs, mask, action, old_values, old_log_prob, advantage, return_batch = replay_data
                if isinstance(self.action_space, spaces.Discrete):
                    # Convert discrete action for float to long
                    action = action.astype(np.int64).flatten()

                with tf.GradientTape() as tape:
                    tape.watch(self.train_model.trainable_variables)
                    values, log_prob, entropy = self.train_model.evaluate_actions(obs, action, mb_states, mask)

                    values = tf.reshape(values, [-1])

                    policy_loss = self.policy_loss(advantage, log_prob, old_log_prob, clip_range)
                    value_loss = self.value_loss(values, old_values, return_batch, clip_range_vf)

                    entropy_loss = -tf.reduce_mean(entropy)

                    loss = policy_loss + self.ent_coef * entropy_loss + self.vf_coef * value_loss

                # Optimization step
                gradients = tape.gradient(loss, self.train_model.trainable_variables)
                # Clip grad norm
                if self.max_grad_norm is not None:
                    gradients, _ = tf.clip_by_global_norm(gradients, self.max_grad_norm)
                self.optimizer.apply_gradients(zip(gradients, self.train_model.trainable_variables))

                approx_kl_divs.append(tf.reduce_mean(old_log_prob - log_prob).numpy())

            if self.target_kl is not None and np.mean(approx_kl_divs) > 1.5 * self.target_kl:
                print("Early stopping at step {} due to reaching max kl: {:.2f}".format(gradient_step,
                                                                                        np.mean(approx_kl_divs)))
                break

        explained_var = explained_variance(buffer.returns.flatten(), buffer.values.flatten())

        logger.log_kv("clip_range", clip_range)
        if self.clip_range_vf is not None:
            logger.log_kv("clip_range_vf", clip_range_vf)

        logger.log_kv("Explained Variance", explained_var)
        logger.log_kv("Entropy", entropy.numpy().mean())
        logger.log_kv("Policy loss", policy_loss.numpy())
        logger.log_kv("value Loss", value_loss.numpy())
        if hasattr(self.policy, 'log_std'):
            logger.log_kv("pi/std", tf.exp(self.policy.log_std).numpy().mean())

    def learn(self, total_timesteps, callback=None, log_interval=1,
              eval_env=None, eval_freq=-1, n_eval_episodes=5, reset_num_timesteps=True):
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f')
        # Logger initialization
        logger_dir = f"log/{self.log_dir}/{timestamp}"
        logger.configure(logger_dir, self.logger_output_types)

        # Checkpoint initialization
        checkpoint_dir = f"checkpoints/{self.checkpoint_dir}"
        checkpoint_train = tf.train.Checkpoint(step=tf.Variable(1), optimizer=self.optimizer, net=self.train_model)
        checkpoint_manager_train = tf.train.CheckpointManager(checkpoint=checkpoint_train, directory=checkpoint_dir,
                                                              max_to_keep=self.max_checkpoint_to_keep)
        checkpoint_act = tf.train.Checkpoint(step=tf.Variable(1), optimizer=self.optimizer, net=self.act_model)
        checkpoint_manager_act = tf.train.CheckpointManager(checkpoint=checkpoint_act, directory=checkpoint_dir,
                                                            max_to_keep=self.max_checkpoint_to_keep)
        # Load weight previous checkpoint if exist
        checkpoint_act.restore(checkpoint_manager_act.latest_checkpoint)
        checkpoint_train.restore(checkpoint_manager_train.latest_checkpoint)

        if checkpoint_manager_train.latest_checkpoint:
            print("Restored from {}".format(checkpoint_manager_act.latest_checkpoint))
        else:
            print("Initializing from scratch.")

        new_tb_log = self._init_num_timesteps(reset_num_timesteps)
        callback = self._init_callback(callback)
        timesteps_since_eval, iteration, evaluations, _obs, eval_env = self._setup_learn(eval_env)
        t_first_start = time.time()
        n_updates = total_timesteps // self.n_batch

        callback.on_training_start(locals(), globals())
        for update in range(1, n_updates + 1):
            assert self.n_batch % self.n_mini_batches == 0, ("The number of mini-batches (`n_mini_batches`) "
                                                             "is not a factor of the total number of samples "
                                                             "collected per rollout (`n_batch`), "
                                                             "some samples won't be used."
                                                             )

            batch_size = self.n_batch // self.n_mini_batches
            t_start = time.time()
            callback.on_rollout_start()
            # true_reward is the reward without discount
            buffer, _obs, states, ep_infos = self.runner.run(callback)
            # Unpack
            callback.on_rollout_end()
            if not self.runner.continue_training:
                break
            self.ep_info_buf.extend(ep_infos)
            if states is None:  # non-recurrent version
                update_fac = ((self.n_batch // self.n_mini_batches) // self.n_opt_epochs + 1)
                self.train(self.n_opt_epochs, buffer, batch_size, states, update)

            else:  # recurrent version
                update_fac = (((self.n_batch // self.n_mini_batches) // self.n_opt_epochs) // self.n_steps + 1)
                assert self.n_envs % self.n_mini_batches == 0
                env_indices = np.arange(self.n_envs)
                flat_indices = np.arange(self.n_envs * self.n_steps).reshape(self.n_envs, self.n_steps)
                envs_per_batch = batch_size // self.n_steps
                print(f'envs_per_batch = {envs_per_batch}')
                print(f'batch_size = {batch_size}')

                for epoch_num in range(self.n_opt_epochs):
                    self.train(self.n_opt_epochs, buffer, batch_size, states, update)

            self.act_model.soft_update(self.train_model, 1)
            t_now = time.time()
            # fps = int(self.n_batch / (t_now - t_start))
            if self.verbose >= 1 and (update % log_interval == 0 or update == 1):
                logger.set_step(self.num_timesteps)
                logger.log_kv("Update runs", update)
                logger.log_kv("serial timesteps (update run times environment steps per update)", update * self.n_steps)
                logger.log_kv("total timesteps (equal serial timesteps if |env| = 1)", self.num_timesteps)
                # logger.log_kv("fps", fps)
                if len(self.ep_info_buf) > 0 and len(self.ep_info_buf[0]) > 0:
                    logger.log_kv('Episode reward mean', safe_mean([ep_info['r'] for ep_info in self.ep_info_buf]))
                    logger.log_kv('Episode length mean', safe_mean([ep_info['l'] for ep_info in self.ep_info_buf]))
                logger.log_kv('time elapsed (in seconds)', t_start - t_first_start)
                logger.dump_kvs()

            # checkpoints
            checkpoint_train.step.assign_add(1)
            checkpoint_act.step.assign_add(1)

            save_path_train = checkpoint_manager_train.save()
            save_path_act = checkpoint_manager_act.save()

            print("Saved checkpoint for step {}: {}".format(int(checkpoint_train.step), save_path_train))
            print("Saved checkpoint for step {}: {}".format(int(checkpoint_act.step), save_path_act))
        callback.on_training_end()
        return self


class Runner(AbstractEnvRunner):
    def __init__(self, *, env, model, n_steps, gamma, lam):
        """
        A runner to learn the policy of an environment for a model

        :param env: (Gym environment) The environment to learn from
        :param model: (Model) The model to learn
        :param n_steps: (int) The number of steps to run for each environment
        :param gamma: (float) Discount factor
        :param lam: (float) Factor for trade-off of bias vs variance for Generalized Advantage Estimator
        """
        super(Runner, self).__init__(env=env, model=model, n_steps=n_steps)
        self.lam = lam
        self.gamma = gamma
        self.buffer = RolloutBuffer(self.n_steps, env.observation_space.shape[-1], self.action_dim,
                                    gamma=self.gamma,
                                    gae_lambda=self.lam, n_envs=self.n_envs)
        self.obs[:] = self.env.reset()

    def _run(self):
        """
        Run a learning step of the model

        :return:
            - observations: (np.ndarray) the observations
            - rewards: (np.ndarray) the rewards
            - masks: (numpy bool) whether an episode is over or not
            - actions: (np.ndarray) the actions
            - values: (np.ndarray) the value function output
            - log probabilities: (np.ndarray)
            - states: (np.ndarray) the internal states of the recurrent policies
            - infos: (dict) the extra information of the model
        """
        self.buffer.reset()
        mb_states = self.states
        # self.states = self.model.initial_state
        for _ in range(self.n_steps):
            actions, values, self.states, log_prob = self.model.step(obs=self.obs, state=self.states, mask=self.dones)
            actions = actions.numpy()

            clipped_actions = actions
            # Clip the actions to avoid out of bound error
            if isinstance(self.env.action_space, gym.spaces.Box):
                clipped_actions = np.clip(actions, self.env.action_space.low, self.env.action_space.high)
            new_obs, rewards, self.dones, infos = self.env.step(clipped_actions)
            self._update_info_buffer(infos)
            self.buffer.add(self.obs, actions, rewards, self.dones, values, log_prob)
            self.model.num_timesteps += self.n_envs
            if self.callback is not None:
                # Abort training early
                if self.callback.on_step() is False:
                    self.continue_training = False
                    # Return dummy values
                    return [None] * 9
            self.obs[:] = new_obs
        self.buffer.compute_returns_and_advantage(values, self.dones)
        return self.buffer, self.obs, mb_states, self.ep_info_buffer


def swap_and_flatten(arr):
    """
    swap and then flatten axes 0 and 1

    :param arr: (np.ndarray)
    :return: (np.ndarray)
    """
    shape = arr.shape
    return arr.swapaxes(0, 1).reshape(shape[0] * shape[1], *shape[2:])
