import typing as tp
from abc import abstractmethod

import numpy as np
import tensorflow as tf
from gym.spaces import Discrete
from tensorflow.keras import Model
from tensorflow.keras.initializers import Orthogonal
from tensorflow.keras.layers import Dense

from stable_baselines.common.Input import ObservationInput
from stable_baselines.common.distributions import DiagGaussianDistribution, CategoricalDistribution, \
    make_proba_distribution
from stable_baselines.common.features_extractor import MLP, NatureCnn


class BasePolicy(Model):
    """
    The base policy object
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The n umber of batches to run (n_envs * n_steps)
    :param scale: (bool) whether or not to scale the input
    :param obs_layer: (Layer) a Layer containing an override for observation layer
    """

    recurrent = False

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, scale=False, obs_layer=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.n_env = n_env
        self.n_steps = n_steps
        self.n_batch = n_batch

        if obs_layer is None:
            self._obs_layer = ObservationInput(ob_space, scale=scale)
        else:
            self._obs_layer = obs_layer

            self._action_layer = None
            # if add_action_ph:
            #    self._action_ph = tf.placeholder(dtype=ac_space.dtype, shape=(n_batch,) + ac_space.shape,
            #                                     name="action_ph")
        self.ob_space = ob_space
        self.ac_space = ac_space

    @tf.function
    def soft_update(self, other_network: Model, tau: float) -> None:
        """
        Allow soft update between target network and source one

        :param Model other_network: target network
        :param float tau:  mixing parameter between 0 and 1
        """
        other_variables = other_network.trainable_variables
        current_variables = self.trainable_variables

        for (current_var, other_var) in zip(current_variables, other_variables):
            current_var.assign((1. - tau) * current_var + tau * other_var)

    def hard_update(self, other_network: Model) -> None:
        """
        Hard update: target network is the new network

        :param Model other_network: target model
        :return None:
        """
        self.soft_update(other_network, tau=1.)

    @property
    def is_discrete(self):
        """bool: is action space discrete."""
        return isinstance(self.ac_space, Discrete)

    @property
    def initial_state(self):
        """
        The initial state of the policy. For feedforward policies, None. For a recurrent policy,
        a NumPy array of shape (self.n_env, ) + state_shape.
        """
        assert not self.recurrent, "When using recurrent policies, you must overwrite `initial_state()` method"
        return None

    @property
    def obs_layer(self):
        """tf.Tensor: placeholder for observations, shape (self.n_batch, ) + self.ob_space.shape."""
        return self._obs_layer

    # @property
    # def processed_obs(self):
    #    """tf.Tensor: processed observations, shape (self.n_batch, ) + self.ob_space.shape.
    #    The form of processing depends on the type of the observation space, and the parameters
    #    whether scale is passed to the constructor; see observation_input for more information."""
    #    return self._processed_obs

    # @property
    # def action_ph(self):
    #    """tf.Tensor: placeholder for actions, shape (self.n_batch, ) + self.ac_space.shape."""
    #    return self._action_ph

    @staticmethod
    def _kwargs_check(feature_extraction, kwargs):
        """
        Ensure that the user is not passing wrong keywords
        when using policy_kwargs.
        :param feature_extraction: (str)
        :param kwargs: (dict)
        """
        # When using policy_kwargs parameter on model creation,
        # all keywords arguments must be consumed by the policy constructor except
        # the ones for the cnn_extractor network (cf nature_cnn()), where the keywords arguments
        # are not passed explicitly (using **kwargs to forward the arguments)
        # that's why there should be not kwargs left when using the mlp_extractor
        # (in that case the keywords arguments are passed explicitly)
        if feature_extraction == 'mlp' and len(kwargs) > 0:
            raise ValueError("Unknown keywords for policy: {}".format(kwargs))

    # @abstractmethod
    # def step(self, obs, state=None, mask=None):
    #     """
    #     Returns the policy for a single step
    #     :param obs: ([float] or [int]) The current observation of the environment
    #     :param state: ([float]) The last states (used in recurrent policies)
    #     :param mask: ([float]) The last masks (used in recurrent policies)
    #     :return: ([float], [float], [float], [float]) actions, values, states, neglogp
    #     """
    #     raise NotImplementedError

    def call(self, obs, training=True, state=None, mask=None, **kwargs):
        """
        Returns the policy for a single step
        :param training:
        :param obs: ([float] or [int]) The current observation of the environment
        :param state: ([float]) The last states (used in recurrent policies)
        :param mask: ([float]) The last masks (used in recurrent policies)
        :return: ([float], [float], [float], [float]) actions, values, states, neglogp
        """
        raise NotImplementedError

    @abstractmethod
    def proba_step(self, obs, state=None, mask=None):
        """
        Returns the action probability for a single step
        :param obs: ([float] or [int]) The current observation of the environment
        :param state: ([float]) The last states (used in recurrent policies)
        :param mask: ([float]) The last masks (used in recurrent policies)
        :return: ([float]) the action probability
        """
        raise NotImplementedError


class ActorCriticPolicy(BasePolicy):
    """
    Policy object that implements actor critic
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param scale: (bool) whether or not to scale the input
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, scale=False):
        super(ActorCriticPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch, scale=scale)
        self._pdtype = make_proba_distribution(ac_space)
        self._policy = None
        self._value_fn = None

    def _build_mlp(self):
        self.extractor = MLP(self.net_arch, self.act_fun)

    def _build_cnn(self, **kwargs):
        self.extractor = NatureCnn(**kwargs)

    def _build_action_net(self):
        if isinstance(self.pdtype, DiagGaussianDistribution):
            print("DiagGaussianDistribution")
            self._action_net, self._q_value = \
                self.pdtype.proba_distribution_net()
        elif isinstance(self.pdtype, CategoricalDistribution):
            print("CategoricalDistribution")
            self._action_net = self.pdtype.proba_distribution_net()

    def _get_action_dist_from_latent(self, latent_pi, deterministic=False):
        if isinstance(self._action_net, tuple):
            params = tuple(map(lambda fn: fn(latent_pi), self._action_net))
        else:
            params = self._action_net(latent_pi)

        if isinstance(self.pdtype, DiagGaussianDistribution):
            return self.pdtype.proba_distribution(*params, deterministic=deterministic)

        elif isinstance(self.pdtype, CategoricalDistribution):
            # Here mean_actions are the logits before the softmax
            return self.pdtype.proba_distribution(params, deterministic=deterministic)

    def _build(self):
        self._value_flat = self.value_fn[:, 0]

    @property
    def pdtype(self):
        """ProbabilityDistributionType: type of the distribution for stochastic actions."""
        return self._pdtype

    @property
    def value_fn(self):
        """tf.Tensor: value estimate, of shape (self.n_batch, 1)"""
        return self._value_fn

    @property
    def value_flat(self):
        """tf.Tensor: value estimate, of shape (self.n_batch, )"""
        return self._value_flat

    @abstractmethod
    def call(self, obs, training=True, state=None, mask=None, deterministic=False, **kwargs):
        """
        Returns the policy for a single step
        :param training:
        :param obs: ([float] or [int]) The current observation of the environment
        :param state: ([float]) The last states (used in recurrent policies)
        :param mask: ([float]) The last masks (used in recurrent policies)
        :param deterministic: (bool) Whether or not to return deterministic actions.
        :return: ([float], [float], [float], [float]) actions, values, states, neglogp
        """
        raise NotImplementedError

    @abstractmethod
    def evaluate_actions(self, obs, action, state=None, mask=None, deterministic=False):
        """
        Evaluate actions according to the current policy,
        given the observations.

        :param obs: (th.Tensor)
        :param action: (th.Tensor)
        :param state: (tf.Tensor)
        :param mask: (tf.Tensor)
        :param deterministic: (bool)
        :return: (th.Tensor, th.Tensor, th.Tensor) estimated value, log likelihood of taking those actions
            and entropy of the action distribution.
        """
        raise NotImplementedError

    @abstractmethod
    def proba_step(self, obs, state=None, mask=None):
        """
               Evaluate action probability to the current policy,
               given the observations.

               :param obs: (th.Tensor)
               :param state: (tf.Tensor)
               :param mask: (tf.Tensor)
               :return: (tf.Tensor) probability pi(.|obs)
               """
        raise NotImplementedError

    @abstractmethod
    def value(self, obs, state=None, mask=None):
        """
        Returns the value for a single step
        :param obs: ([float] or [int]) The current observation of the environment
        :param state: ([float]) The last states (used in recurrent policies)
        :param mask: ([float]) The last masks (used in recurrent policies)
        :return: ([float]) The associated value of the action
        """
        raise NotImplementedError


class RecurrentActorCriticPolicy(ActorCriticPolicy):
    """
    Actor critic policy object uses a previous state in the computation for the current step.
    NOTE: this class is not limited to recurrent neural network policies,
    see https://github.com/hill-a/stable-baselines/issues/241
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param state_shape: (tuple<int>) shape of the per-environment state space.
    :param scale: (bool) whether or not to scale the input
    """

    recurrent = True

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch,
                 state_shape, scale=False):
        super(RecurrentActorCriticPolicy, self).__init__(ob_space, ac_space, n_env, n_steps,
                                                         n_batch, scale=scale)

        self._initial_state_shape = (self.n_env,) + tuple(state_shape)
        self._initial_state = np.zeros(self._initial_state_shape, dtype=np.float32)

    @property
    def initial_state(self):
        return self._initial_state


class FeedForwardPolicy(ActorCriticPolicy):
    """
    Policy object that implements actor critic, using a feed forward neural network.
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param layers: ([int]) (deprecated, use net_arch instead) The size of the Neural network for the policy
        (if None, default to [64, 64])
    :param net_arch: (list) Specification of the actor-critic policy network architecture (see mlp_extractor
        documentation for details).
    :param act_fun: (tf.func) the activation function to use in the neural network.
    :param cnn_extractor: (function (TensorFlow Tensor, ``**kwargs``): (TensorFlow Tensor)) the CNN feature extraction
    :param feature_extraction: (str) The feature extraction type ("cnn" or "mlp")
    :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, net_arch=None,
                 act_fun='tanh', cnn_extractor=NatureCnn, feature_extraction="mlp", **kwargs):
        super(FeedForwardPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch,
                                                scale=(feature_extraction == "cnn"))

        self._kwargs_check(feature_extraction, kwargs)
        self.cnn_extractor = cnn_extractor
        self.pi_latent = None
        self.act_fun = act_fun

        self.net_arch = [128, 128, dict(vf=[64, 64], pi=[64, 64])] if net_arch is None else net_arch

        if feature_extraction == 'cnn':
            self._build_cnn(**kwargs)
        else:
            self._build_mlp()

        self._value_fn = Dense(1, name='vf', kernel_initializer=Orthogonal(np.sqrt(2)),
                               activation='tanh')

        self._build_action_net()

    def _get_latent(self, transform_obs: tf.Tensor) -> tp.Tuple[tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        Action and value-function latent features from observation

        :param tf.Tensor transform_obs: row observation
        :return tp.Tuple[tf.Tensor, tf.Tensor]: action network feature, value-function network feature
        """
        latent = self.extractor(transform_obs)
        # check if pi network is different from vf network
        if isinstance(latent, tuple):
            pi_latent, vf_latent, shared, _ = latent
        else:
            pi_latent = vf_latent = shared = latent

        return pi_latent, vf_latent, shared

    @tf.function
    def call(self, obs, training=True, state=None, mask=None, deterministic=False, **kwargs):
        _obs = self.obs_layer(obs)
        pi_latent, vf_latent, shared = self._get_latent(_obs)
        value = self.value_fn(vf_latent)
        action, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=deterministic)
        log_prob = action_distribution.log_prob(action)
        return action, value, self.initial_state, log_prob

    @tf.function
    def evaluate_actions(self, obs, action, state=None, mask=None, deterministic=False):

        _obs = self.obs_layer(obs)
        pi_latent, vf_latent, shared = self._get_latent(_obs)
        value = self.value_fn(vf_latent)
        _, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=deterministic)
        log_prob = action_distribution.log_prob(action)
        entropy = action_distribution.entropy()
        return value, log_prob, entropy

    @tf.function
    def proba_step(self, obs, state=None, mask=None):
        _obs = self.obs_layer(obs)
        pi_latent, _, _ = self._get_latent(_obs)
        _, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=False)
        return action_distribution

    @tf.function
    def value(self, obs, state=None, mask=None):
        _obs = self.obs_layer(obs)
        _, vf_latent, _ = self._get_latent(_obs)
        value = self.value_fn(vf_latent)
        return value


class CnnPolicy(FeedForwardPolicy):
    """
    Policy object that implements actor critic, using a CNN (the nature CNN)

    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param reuse: (bool) If the policy is reusable or not
    :param _kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, **_kwargs):
        super(CnnPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch,
                                        feature_extraction="cnn", **_kwargs)


class LstmPolicy(RecurrentActorCriticPolicy):
    """
   Policy object that implements actor critic, using LSTMs.

   :param sess: (TensorFlow session) The current TensorFlow session
   :param ob_space: (Gym Space) The observation space of the environment
   :param ac_space: (Gym Space) The action space of the environment
   :param n_env: (int) The number of environments to run
   :param n_steps: (int) The number of steps to run for each environment
   :param n_batch: (int) The number of batch to run (n_envs * n_steps)
   :param n_lstm: (int) The number of LSTM cells (for recurrent policies)
   :param reuse: (bool) If the policy is reusable or not
   :param layers: ([int]) The size of the Neural network before the LSTM layer  (if None, default to [64, 64])
   :param net_arch: (list) Specification of the actor-critic policy network architecture. Notation similar to the
       format described in mlp_extractor but with additional support for a 'lstm' entry in the shared network part.
   :param act_fun: (tf.func) the activation function to use in the neural network.
   :param cnn_extractor: (function (TensorFlow Tensor, ``**kwargs``): (TensorFlow Tensor)) the CNN feature extraction
   :param layer_norm: (bool) Whether or not to use layer normalizing LSTMs
   :param feature_extraction: (str) The feature extraction type ("cnn" or "mlp")
   :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
   """

    recurrent = True

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=128,
                 net_arch=None, act_fun=tf.tanh, cnn_extractor=NatureCnn, feature_extraction="mlp",
                 **kwargs):
        # state_shape = [n_lstm * 2] dim because of the cell and hidden states of the LSTM
        super(LstmPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch,
                                         state_shape=(2 * n_lstm,),
                                         scale=(feature_extraction == "cnn"))
        self._kwargs_check(feature_extraction, kwargs)
        self._initial_state = [np.zeros(shape=(n_env, n_lstm), dtype=np.float32),
                               np.zeros(shape=(n_env, n_lstm), dtype=np.float32)]
        self.cnn_extractor = cnn_extractor
        self.pi_latent = None
        self.act_fun = act_fun

        self.net_arch = [128, 128, dict(lstm=[n_lstm], vf=[64, 64], pi=[64, 64])] if net_arch is None else net_arch

        if feature_extraction == 'cnn':
            self._build_cnn(**kwargs)
        else:
            self._build_mlp()

        self._value_fn = Dense(1, name='vf', kernel_initializer=Orthogonal(np.sqrt(2)),
                               activation='tanh')
        self._build_action_net()

    def _get_latent(self, transform_obs: tf.Tensor, state: tf.Tensor, mask: tf.Tensor) \
            -> tp.Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        Action and value-function latent features from observation

        :param tf.Tensor transform_obs: row observation
        :return tp.Tuple[tf.Tensor, tf.Tensor]: action network feature, value-function network feature
        """
        print(f'state value from get_latent function {state}')
        latent = self.extractor(transform_obs, state, self.n_env, self.n_steps, mask)

        # check if pi network is different from vf network
        if isinstance(latent, tuple):
            pi_latent, vf_latent, shared, new_state = latent
        else:
            pi_latent = vf_latent = shared = latent
            # TODO: add recurrent network for CNN nature
            new_state = state

        return pi_latent, vf_latent, shared, new_state

    @tf.function
    def call(self, obs, training=True, state=None, mask=None, deterministic=False, **kwargs):
        _obs = self.obs_layer(obs)
        print(f'state form call function {state}')
        print(f'mask form call function {mask}')

        pi_latent, vf_latent, shared, new_state = self._get_latent(_obs, state, mask)
        value = self.value_fn(vf_latent)
        action, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=deterministic)
        log_prob = action_distribution.log_prob(action)
        return action, value, new_state, log_prob

    @tf.function
    def evaluate_actions(self, obs, action, state=None, mask=None, deterministic=False):
        _obs = self.obs_layer(obs)
        print(f'mask form evaluate_actions function {mask}')
        pi_latent, vf_latent, _, _ = self._get_latent(_obs, state, mask)
        value = self.value_fn(vf_latent)
        _, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=deterministic)
        log_prob = action_distribution.log_prob(action)
        entropy = action_distribution.entropy()
        return value, log_prob, entropy

    @tf.function
    def proba_step(self, obs, state=None, mask=None):
        _obs = self.obs_layer(obs)
        pi_latent, _, _, _ = self._get_latent(_obs, state, mask)
        _, action_distribution = self._get_action_dist_from_latent(pi_latent, deterministic=False)
        return action_distribution

    @tf.function
    def value(self, obs, state=None, mask=None):
        _obs = self.obs_layer(obs)
        _, vf_latent, _, _ = self._get_latent(_obs, state, mask)
        value = self.value_fn(vf_latent)
        return value


class CnnLstmPolicy(LstmPolicy):
    """
    Policy object that implements actor critic, using LSTMs with a CNN feature extraction

    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param n_lstm: (int) The number of LSTM cells (for recurrent policies)
    :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def call(self, obs, training=None, state=None, mask=None, deterministic=False, **kwargs):
        pass

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=256, **_kwargs):
        super(CnnLstmPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch, n_lstm,
                                            layer_norm=False, feature_extraction="cnn", **_kwargs)


class CnnLnLstmPolicy(LstmPolicy):
    """
    Policy object that implements actor critic, using a layer normalized LSTMs with a CNN feature extraction

    :param sess: (TensorFlow session) The current TensorFlow session
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param n_lstm: (int) The number of LSTM cells (for recurrent policies)
    :param reuse: (bool) If the policy is reusable or not
    :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=256, **_kwargs):
        super(CnnLnLstmPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch, n_lstm,
                                              layer_norm=True, feature_extraction="cnn", **_kwargs)


class MlpPolicy(FeedForwardPolicy):
    """
    Policy object that implements actor critic, using a MLP (2 layers of 64)

    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param reuse: (bool) If the policy is reusable or not
    :param _kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, **_kwargs):
        super(MlpPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch,
                                        feature_extraction="mlp", **_kwargs)


class MlpLstmPolicy(LstmPolicy):
    """
    Policy object that implements actor critic, using LSTMs with a MLP feature extraction

    :param sess: (TensorFlow session) The current TensorFlow session
    :param ob_space: (Gym Space) The observation space of the environment
    :param ac_space: (Gym Space) The action space of the environment
    :param n_env: (int) The number of environments to run
    :param n_steps: (int) The number of steps to run for each environment
    :param n_batch: (int) The number of batch to run (n_envs * n_steps)
    :param n_lstm: (int) The number of LSTM cells (for recurrent policies)
    :param reuse: (bool) If the policy is reusable or not
    :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
    """

    def __init__(self, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=128, **_kwargs):
        super(MlpLstmPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch, n_lstm,
                                            feature_extraction="mlp", **_kwargs)


# class MlpLnLstmPolicy(LstmPolicy):
#     """
#     Policy object that implements actor critic, using a layer normalized LSTMs with a MLP feature extraction
#
#     :param sess: (TensorFlow session) The current TensorFlow session
#     :param ob_space: (Gym Space) The observation space of the environment
#     :param ac_space: (Gym Space) The action space of the environment
#     :param n_env: (int) The number of environments to run
#     :param n_steps: (int) The number of steps to run for each environment
#     :param n_batch: (int) The number of batch to run (n_envs * n_steps)
#     :param n_lstm: (int) The number of LSTM cells (for recurrent policies)
#     :param reuse: (bool) If the policy is reusable or not
#     :param kwargs: (dict) Extra keyword arguments for the nature CNN feature extraction
#     """
#
#     def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=256, reuse=False, **_kwargs):
#         super(MlpLnLstmPolicy, self).__init__(ob_space, ac_space, n_env, n_steps, n_batch, n_lstm, reuse,
#                                               layer_norm=True, feature_extraction="mlp", **_kwargs)


_policy_registry = {
    ActorCriticPolicy: {
        "CnnPolicy": CnnPolicy,
        # "CnnLstmPolicy": CnnLstmPolicy,
        # "CnnLnLstmPolicy": CnnLnLstmPolicy,
        "MlpPolicy": MlpPolicy,
        "MlpLstmPolicy": MlpLstmPolicy,
        # "MlpLnLstmPolicy": MlpLnLstmPolicy,
    }
}


def get_policy_from_name(base_policy_type, name):
    """
    returns the registed policy from the base type and name
    :param base_policy_type: (BasePolicy) the base policy object
    :param name: (str) the policy name
    :return: (base_policy_type) the policy
    """
    if base_policy_type not in _policy_registry:
        raise ValueError("Error: the policy type {} is not registered!".format(base_policy_type))
    if name not in _policy_registry[base_policy_type]:
        raise ValueError("Error: unknown policy type {}, the only registed policy type are: {}!"
                         .format(name, list(_policy_registry[base_policy_type].keys())))
    return _policy_registry[base_policy_type][name]


def register_policy(name, policy):
    """
    returns the registed policy from the base type and name
    :param name: (str) the policy name
    :param policy: (subclass of BasePolicy) the policy
    """
    sub_class = None
    for cls in BasePolicy.__subclasses__():
        if issubclass(policy, cls):
            sub_class = cls
            break
    if sub_class is None:
        raise ValueError("Error: the policy {} is not of any known subclasses of BasePolicy!".format(policy))

    if sub_class not in _policy_registry:
        _policy_registry[sub_class] = {}
    if name in _policy_registry[sub_class]:
        raise ValueError("Error: the name {} is already registered for a different policy, will not override."
                         .format(name))
    _policy_registry[sub_class][name] = policy
