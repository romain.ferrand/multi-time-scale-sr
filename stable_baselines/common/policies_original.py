import typing as tp

import gym
import tensorflow as tf
import tensorflow.keras.layers as layers
from math import sqrt
from tensorflow.keras import Model
from tensorflow.keras.layers import Flatten, Dense, Activation, Conv2D, GlobalAveragePooling2D, Input
from tensorflow.keras.models import Sequential
from tensorflow.python.ops.init_ops_v2 import Orthogonal

from stable_baselines.common.distributions import DiagGaussianDistribution, CategoricalDistribution, \
    MultiCategoricalDistribution, BernoulliDistribution


def nature_cnn(scaled_images, **kwargs):
    """
    CNN from Nature paper.
    :param scaled_images: (Tuple int) Image input placeholder
    :param kwargs: (dict) Extra keywords parameters for the convolutional layers of the CNN
    :return: (TensorFlow Tensor) The CNN output layer
    """
    activ = 'relu'
    inputs = Input(shape=scaled_images, name='img')
    layer_1 = Conv2D(activation=activ, filters=32,
                     kernel_size=8, strides=4, name='c1',
                     kernel_initializer=Orthogonal(sqrt(2)), **kwargs)(inputs)
    layer_2 = Conv2D(activation=activ, filters=64,
                     kernel_size=4, strides=2, name='c2',
                     kernel_initializer=Orthogonal(sqrt(2)), **kwargs)(layer_1)
    layer_3 = Conv2D(activation=activ, filters=64,
                     kernel_size=3, strides=1, name='c3',
                     kernel_initializer=Orthogonal(sqrt(2)), **kwargs)(layer_2)
    layer_4 = GlobalAveragePooling2D()(layer_3)
    outputs = Dense(units=512, activation=activ, name='fc1',
                    kernel_initializer=Orthogonal(sqrt(2)), **kwargs)(layer_4)
    return outputs


def create_mlp(input_dim: int, output_dim: int, net_arch: tp.List[int],
               activation_fn: tp.Union[tp.Callable[..., tp.Any], str] = tf.nn.relu,
               squash_out: bool = False) -> tp.List[tp.Union[Flatten, Dense, Activation]]:
    """
    Create a multi layer perceptron (MLP), which is
    a collection of fully-connected layers each followed by an activation function.

    :param int input_dim:  Dimension of the input vector
    :param int output_dim: Dimension of the output vector
    :param tp.List[int] net_arch:  Architecture of the neural net
        It represents the number of units per layer.
        The length of this list is the number of layers.
    :param tp.Union[tp.Callable[..., tp.Any] activation_fn:  The activation function
    to use after each layer.
    :param bool squash_out: Whether to squash the output using a Tanh activation function
    """
    modules: tp.List[tp.Union[Flatten, Dense, tf.keras.applications]] = \
        [Flatten(input_shape=(input_dim,), dtype=tf.float32)]

    for idx in range(len(net_arch)):
        modules.append(Dense(net_arch[idx], activation=activation_fn,
                             kernel_initializer='orthogonal_initializer'))

    if output_dim > 0:
        modules.append(Dense(output_dim, activation=None))
    if squash_out:
        modules.append(Activation(activation='tanh'))
    return modules


class BasePolicy(Model):
    """
    The base policy object

    """

    def __init__(self, observation_space: tp.Optional[gym.Space], action_space: tp.Optional[gym.Space]):
        """
        :param gym.Space observation_space: The observation space of the environment
        :param gym.Space action_space: The action space of the environment
        """
        super(BasePolicy, self).__init__()
        self.observation_space = observation_space
        self.action_space = action_space

    def call(self, x, **kwargs):
        raise NotImplementedError()

    @tf.function
    def soft_update(self, other_network: Model, tau: float) -> None:
        """
        Allow soft update between target network and source one

        :param Model other_network: target network
        :param float tau:  mixing parameter between 0 and 1
        """
        other_variables = other_network.trainable_variables
        current_variables = self.trainable_variables

        for (current_var, other_var) in zip(current_variables, other_variables):
            current_var.assign((1. - tau) * current_var + tau * other_var)

    def hard_update(self, other_network: Model) -> None:
        """
        Hard update: target network is the new network

        :param Model other_network: target model
        :return None:
        """
        self.soft_update(other_network, tau=1.)

    def save(self, path: str, **kwargs) -> None:
        """
        Save model to a given location.
        :param path: (str) file location
        """
        raise NotImplementedError()

    def load(self, path: str) -> None:
        """
        Load saved model from path.

        :param path: (str)
        """
        raise NotImplementedError()


class Actor(BasePolicy):
    """
    Actor network (policy).

    :param tp.Optional[gym.Space] observation_space: Observation space
    :param tp.Optional[gym.Space] action_space: Action space
    :param tp.List[int] net_arch: Network architecture
    :param tp.Union[tp.Callable[..., tp.Any], str] activation_fn: Activation function
    """

    def __init__(self, observation_space: tp.Optional[gym.Space],
                 action_space: tp.Optional[gym.Space], net_arch: tp.List[int],
                 activation_fn: tp.Union[tp.Callable[..., tp.Any], str] = tf.nn.relu):
        super(Actor, self).__init__(observation_space, action_space)

        self.actor_net = create_mlp(observation_space.shape[0], action_space.shape[0], net_arch, activation_fn,
                                    squash_out=False)
        self.distribution = None
        self.mu = None
        self.last_action = None
        self.last_log_proba = None
        self.resolve_distribution_layer()

    def resolve_distribution_layer(self):

        latent_dim: int = self.actor_net[-1].output_shape[-1]
        action_dim: int = self.action_space.shape[-1]
        if isinstance(self.action_space, gym.spaces.Box):
            self.distribution = DiagGaussianDistribution(action_dim)
            (mean, log_std) = self.distribution.proba_distribution_net(latent_dim)
            self.actor_net.append(mean)
            mean_layer = Sequential(mean)
            mean_layer.build()
            self.mu = [mean_layer, log_std]
        elif isinstance(self.action_space, gym.spaces.Discrete):
            self.distribution = CategoricalDistribution(action_dim)
            logits_layer = self.distribution.proba_distribution_net(latent_dim)
            self.mu = self.actor_net.append(logits_layer)
            self.mu = Sequential(self.mu)
            self.mu.build()
        elif isinstance(self.action_space, gym.spaces.MultiDiscrete):
            self.distribution = MultiCategoricalDistribution(self.action_space.nvec)
            logits_layer = self.distribution.proba_distribution_net(latent_dim)
            self.mu = self.actor_net.append(logits_layer)
            self.mu = Sequential(self.mu)
            self.mu.build()
        elif isinstance(self.action_space, gym.spaces.MultiBinary):
            self.distribution = BernoulliDistribution(action_dim)
            logits_layer = self.distribution.proba_distribution_net(latent_dim)
            self.mu = self.actor_net.append(logits_layer)
            self.mu = Sequential(self.mu)
            self.mu.build()

    def act(self, obs, deterministic=False):
        if isinstance(self.action_space, gym.spaces.Box) \
                and isinstance(self.distribution, DiagGaussianDistribution):
            mean = self.mu[0](obs)
            log_std = self.mu[1] * obs
            action, _ = self.distribution.proba_distribution(mean, log_std, deterministic)
            self.last_action = action

            return action
        elif isinstance(self.action_space, gym.spaces.Discrete) \
                and isinstance(self.distribution, CategoricalDistribution):
            pass
        elif isinstance(self.action_space, gym.spaces.MultiDiscrete) \
                and isinstance(self.distribution, MultiCategoricalDistribution):
            pass
        elif isinstance(self.action_space, gym.spaces.MultiBinary) \
                and isinstance(self.distribution, BernoulliDistribution):
            pass
        return None

    def action_and_log_prob_from_obs(self, obs):
        if isinstance(self.action_space, gym.spaces.Box) \
                and isinstance(self.distribution, DiagGaussianDistribution):
            mean = self.mu[0](obs)
            log_std = self.mu[1] * obs
            action, log_prob = self.distribution.log_prob_from_params(mean, log_std)
            return action, log_prob
        elif isinstance(self.action_space, gym.spaces.Discrete) \
                and isinstance(self.distribution, CategoricalDistribution):
            pass
        elif isinstance(self.action_space, gym.spaces.MultiDiscrete) \
                and isinstance(self.distribution, MultiCategoricalDistribution):
            pass
        elif isinstance(self.action_space, gym.spaces.MultiBinary) \
                and isinstance(self.distribution, BernoulliDistribution):
            pass
        return None, None

    @tf.function
    def call(self, obs, **kwargs):
        if isinstance(self.mu, list):
            return [self.mu[0](obs), self.mu[1] * obs]
        else:
            return self.mu(obs)

    def save(self, path, **kwargs):
        pass

    def load(self, path):
        pass


class Critic_V_value(BasePolicy):

    def __init__(self, observation_space: tp.Optional[gym.Space], net_arch: tp.List[int],
                 activation_fn: tp.Union[tp.Callable[..., tp.Any], str] = tf.nn.relu):
        super(Critic_V_value, self).__init__(observation_space, None)
        v1_net = create_mlp(observation_space.shape[0], 1, net_arch, activation_fn, squash_out=True)
        self.v1_net = Sequential(v1_net)

        v2_net = create_mlp(observation_space.shape[0], 1, net_arch, activation_fn, squash_out=True)
        self.v2_net = Sequential(v2_net)

        self.v_networks = [self.v1_net, self.v2_net]

        for v_net in self.v_networks:
            v_net.build()

    @tf.function
    def call(self, obs, **kwargs):
        return [v_net(obs) for v_net in self.v_networks]

    @tf.function
    def v1_forward(self, obs):
        return self.v_networks[0](obs)

    def save(self, path, **kwargs):
        pass

    def load(self, path):
        pass


class Critic_Q_value(BasePolicy):
    """
    Critic_Q_value network,
    in fact it represents the action-state value function (Q-value function)

    :param tp.Optional[gym.Space] observation_space: Dimension of the observation
    :param tp.Optional[gym.Space] action_space: Dimension of the action space
    :param tp.List[int] net_arch:  mlp Network architecture
    :param tp.Union[tp.Callable[..., tp.Any], str] activation_fn: Activation function
    """

    def __init__(self, observation_space: tp.Optional[gym.Space],
                 action_space: tp.Optional[gym.Space], net_arch: tp.List[int],
                 activation_fn: tp.Union[tp.Callable[..., tp.Any], str] = tf.nn.relu):
        super(Critic_Q_value, self).__init__(observation_space, action_space)

        q1_net = create_mlp(observation_space.shape[0] + action_space.shape[0], 1, net_arch, activation_fn,
                            squash_out=True)
        self.q1_net = Sequential(q1_net)

        q2_net = create_mlp(observation_space.shape[0] + action_space.shape[0], 1, net_arch, activation_fn,
                            squash_out=True)
        self.q2_net = Sequential(q2_net)

        self.q_networks = [self.q1_net, self.q2_net]

        for q_net in self.q_networks:
            q_net.build()

    @tf.function
    def call(self, x, **kwargs):
        (obs, action) = x
        q_value_input = tf.concat([obs, action], axis=1)
        return [q_net(q_value_input) for q_net in self.q_networks]

    @tf.function
    def q1_forward(self, obs, action):
        return self.q_networks[0](tf.concat([obs, action], axis=1))

    def save(self, path, **kwargs):
        pass

    def load(self, path):
        pass


class BaseAC(BasePolicy):
    """
    Actor critic policy class with Q_value as critic

    :param tp.Optional[gym.Space] observation_space: (gym.spaces.Space) Observation space
    :param tp.Optional[gym.Space] action_space: (gym.spaces.Space) Action space
    :param tp.Callable learning_rate: (callable) Learning rate schedule (could be constant)
    :param tp.Dict[str, tp.List[Int]] net_arch: The specification of the policy and value networks.
    :param bool q_critic: check if we use Q or V as critic
    :param activation_fn: (str or tf.nn.activation) Activation function
    """

    def __init__(self, observation_space: tp.Optional[gym.Space], action_space: tp.Optional[gym.Space],
                 learning_rate: tp.Callable, net_arch=None, q_critic: bool = False,
                 activation_fn=tf.nn.relu):
        super(BaseAC, self).__init__(observation_space, action_space)

        # Default network architecture, from the original paper
        self.policy_network: tp.List[int]
        self.value_network: tp.List[int]

        if net_arch is None:
            # Default network architecture, from the original paper
            self.policy_network = [400, 300]
            self.value_function_network = [400, 300]
        else:
            self.policy_network = net_arch["policy_network"]
            self.value_function_network = net_arch["value_network"]
        self.activation_fn = activation_fn

        self.actor, self.actor_target = None, None
        self.critic, self.critic_target = None, None
        self._build(learning_rate, q_critic)

    def _build(self, learning_rate, q_critic):
        self.actor = self.make_actor()
        self.actor_target = self.make_actor()
        self.actor_target.hard_update(self.actor)
        self.actor.optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate(1))

        self.critic = self.make_critic(q_critic)
        self.critic_target = self.make_critic(q_critic)
        self.critic_target.hard_update(self.critic)
        self.critic.optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate(1))

    def make_actor(self):
        return Actor(observation_space=self.observation_space,
                     action_space=self.action_space,
                     net_arch=self.policy_network,
                     activation_fn=self.activation_fn)

    def make_critic(self, q_critic):
        if q_critic:
            return Critic_Q_value(observation_space=self.observation_space,
                                  action_space=self.action_space,
                                  net_arch=self.policy_network,
                                  activation_fn=self.activation_fn)
        else:
            return Critic_V_value(observation_space=self.observation_space,
                                  net_arch=self.policy_network,
                                  activation_fn=self.activation_fn)

    @tf.function
    def call(self, obs, **kwargs):
        return self.actor(obs)

    def save(self, path, **kwargs):
        pass

    def load(self, path):
        pass


_policy_registry = dict()


def get_policy_from_name(base_policy_type: tp.Type[BasePolicy], name: str) -> tp.Type[BasePolicy]:
    """
    returns the registered policy from the base type and name

    :param base_policy_type: (BasePolicy) the base policy object
    :param name: the policy name
    :return: (base_policy_type) the policy
    """
    if base_policy_type not in _policy_registry:
        raise ValueError("Error: the policy type {} is not registered!".format(base_policy_type))
    if name not in _policy_registry[base_policy_type]:
        raise ValueError("Error: unknown policy type {}, the only registered policy type are: {}!"
                         .format(name, list(_policy_registry[base_policy_type].keys())))
    return _policy_registry[base_policy_type][name]


def register_policy(name: str, policy: tp.Type[BasePolicy]) -> None:
    """
    returns the registered policy from the base type and name

    :param name: (str) the policy name
    :param policy: (subclass of BasePolicy) the policy
    """
    sub_class = None
    for cls in BasePolicy.__subclasses__():
        if issubclass(policy, cls):
            sub_class = cls
            break
    if sub_class is None:
        raise ValueError("Error: the policy {} is not of any known subclasses of BasePolicy!".format(policy))

    if sub_class not in _policy_registry:
        _policy_registry[sub_class] = {}
    if name in _policy_registry[sub_class]:
        raise ValueError("Error: the name {} is already registered for a different policy, will not override."
                         .format(name))
    _policy_registry[sub_class][name] = policy


register_policy("BaseAC", BaseAC)
