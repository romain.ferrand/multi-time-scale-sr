import typing as tp

import numpy as np
import tensorflow as tf


class BaseBuffer(object):
    """
    Base class that represent a buffer (rollout or replay)

    :param buffer_size: (int) Max number of element in the buffer
    :param obs_dim: (int) Dimension of the observation
    :param action_dim: (int) Dimension of the action space
    :param n_envs: (int) Number of parallel environments
    """

    def __init__(self, buffer_size: int, obs_dim: int, action_dim: int, n_envs: int = 1):
        super(BaseBuffer, self).__init__()
        self.buffer_size = buffer_size
        self.obs_dim = obs_dim
        self.action_dim = action_dim
        self.pos = 0
        self.full = False
        self.n_envs = n_envs

    def size(self) -> int:
        """
        :return: (int) The current size of the buffer
        """
        if self.full:
            return self.buffer_size
        return self.pos

    def add(self, *args, **kwargs):
        """
        Add elements to the buffer.
        """
        raise NotImplementedError()

    def reset(self) -> None:
        """
        Reset the buffer.
        """
        self.pos = 0
        self.full = False

    def sample(self, batch_size: int) -> tp.List[np.ndarray]:
        """
        :param batch_size: (int) Number of element to sample
        """
        upper_bound = self.buffer_size if self.full else self.pos
        batch_inds = np.random.randint(0, upper_bound, size=batch_size)
        return self._get_samples(batch_inds)

    def _get_samples(self, batch_inds) -> tp.List[np.ndarray]:
        """
        :param batch_inds: (np.ndarray)
        :return: ([np.ndarray])
        """
        raise NotImplementedError()


class ReplayBuffer(BaseBuffer):
    """
    Replay buffer used in off-policy algorithms like SAC/TD3.

    :param buffer_size: (int) Max number of element in the buffer
    :param obs_dim: (int) Dimension of the observation
    :param action_dim: (int) Dimension of the action space
    :param n_envs: (int) Number of parallel environments
    """

    def __init__(self, buffer_size: int, obs_dim: int, action_dim: int, n_envs: int = 1):
        super(ReplayBuffer, self).__init__(buffer_size, obs_dim, action_dim, n_envs=n_envs)

        assert n_envs == 1
        self.infos = {
            "obs": np.zeros((self.buffer_size, self.n_envs, self.obs_dim), dtype=np.float32),
            "action": np.zeros((self.buffer_size, self.n_envs, self.action_dim), dtype=np.float32),
            "next_obs": np.zeros((self.buffer_size, self.n_envs, self.obs_dim), dtype=np.float32),
            "reward": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "done": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32)
        }

    def add(self, obs, next_obs, action, reward, done):
        # get local variable for iteration purpose
        self.infos["obs"][self.pos] = np.array(obs).copy()
        self.infos["next_obs"][self.pos] = np.array(next_obs).copy()
        self.infos["action"][self.pos] = np.array(action).copy()
        self.infos["reward"][self.pos] = np.array(reward).copy()
        self.infos["done"][self.pos] = np.array(done).copy()

        self.pos += 1
        if self.pos == self.buffer_size:
            self.full = True
            self.pos = 0

    def _get_samples(self, batch_inds):
        return (self.infos["obs"][batch_inds, 0, :],
                self.infos["action"][batch_inds, 0, :],
                self.infos["next_obs"][batch_inds, 0, :],
                self.infos["done"][batch_inds],
                self.infos["reward"][batch_inds])

    """
    def _get_samples(self, batch_ind: int):
        keys_order = ("obs", "action", "next_obs", "done", "reward")
        return tuple(map(lambda key: self.infos[key][batch_ind, 0, :], keys_order))
    """


class RolloutBuffer(BaseBuffer):
    """
    Rollout buffer used in on-policy algorithms like A2C/PPO.

    :param buffer_size: (int) Max number of element in the buffer
    :param obs_dim: (int) Dimension of the observation
    :param action_dim: (int) Dimension of the action space
    :param gae_lambda: (float) Factor for trade-off of bias vs variance for Generalized Advantage Estimator
        Equivalent to classic advantage when set to 1.
    :param gamma: (float) Discount factor
    :param n_envs: (int) Number of parallel environments
    """

    def __init__(self, buffer_size: int,
                 gae_lambda: float = 1.0, gamma: float = 0.99, n_envs: int = 1):
        super(RolloutBuffer, self).__init__(buffer_size, obs_dim, action_dim, n_envs=n_envs)
        self.gae_lambda = gae_lambda
        self.gamma = gamma
        self.infos = None
        self.generator_ready = False
        self.reset()

    def reset(self):
        self.generator_ready = False
        self.infos = {
            "obs": np.zeros((self.buffer_size, self.n_envs, self.obs_dim), dtype=np.float32),
            "action": np.zeros((self.buffer_size, self.n_envs, self.action_dim), dtype=np.float32),
            "reward": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "done": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "value": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "return": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "log_prob": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32),
            "adv": np.zeros((self.buffer_size, self.n_envs), dtype=np.float32)
        }
        super(RolloutBuffer, self).reset()

    def compute_returns_and_advantage(self, last_value: tf.Tensor, dones: bool = False, use_gae: bool = True):
        """
        Post-processing step: compute the returns (sum of discounted rewards)
        and advantage (A(s) = R - V(S)).

        :param last_value: (tf.Tensor)
        :param dones: ([bool])
        :param use_gae: (bool) Whether to use Generalized Advantage Estimation
            or normal advantage for advantage computation.
        """
        if use_gae:
            last_gae_lam = 0
            for step in reversed(range(self.pos)):
                if step == self.pos - 1:
                    next_non_terminal = np.array(1.0 - dones)
                    next_value = last_value.numpy().flatten()
                else:
                    next_non_terminal = 1.0 - self.infos["done"][step + 1]
                    next_value = self.infos["value"][step + 1]
                delta = self.infos["reward"][step] + self.gamma * next_value * next_non_terminal \
                        - self.infos["value"][step]
                # keep episode independents by zeroing previous last_gae_lam when state is terminal
                last_gae_lam = delta + self.gamma * self.gae_lambda * next_non_terminal * last_gae_lam
                self.infos["adv"][step] = last_gae_lam
            self.infos["return"] = self.infos["adv"] + self.infos["value"]
        else:
            # Discounted return with value bootstrap
            # Note: this is equivalent to GAE computation
            # with lam = 1.0
            last_return = 0.0
            for step in reversed(range(self.pos)):
                if step == self.buffer_size - 1:
                    next_non_terminal = np.array(1.0 - dones)
                    next_value = last_value.numpy().flatten()
                    last_return = self.infos["reward"][step] + next_non_terminal * next_value
                else:
                    next_non_terminal = 1.0 - self.infos["done"][step + 1]
                    last_return = self.infos["return"][step] + self.gamma * last_return * next_non_terminal
                self.infos["return"][step] = last_return
            self.infos["adv"] = self.infos["return"] - self.infos["value"]

    def add(self, obs, action, reward, done, value, log_prob):
        """
        :param obs: Observation
        :param action: Action
        :param reward: (np.ndarray)
        :param done: (np.ndarray) End of episode signal.
        :param value: (np.Tensor) estimated value of the current state
            following the current policy.
        :param log_prob: (np.Tensor) log probability of the action
            following the current policy.
        """
        if len(log_prob.shape) == 0:
            # Reshape 0-d tensor to avoid error
            log_prob = log_prob.reshape(-1, 1)

        self.infos["obs"][self.pos] = np.array(obs).copy()
        self.infos["action"][self.pos] = np.array(action).copy()
        self.infos["reward"][self.pos] = np.array(reward).copy()
        self.infos["done"][self.pos] = np.array(done).copy()
        self.infos["value"][self.pos] = value.numpy().flatten().copy()
        self.infos["log_prob"][self.pos] = log_prob.numpy().copy()
        self.pos += 1
        if self.pos == self.buffer_size:
            self.full = True

    def get(self, batch_size=None):
        indices = np.random.permutation(self.buffer_size * self.n_envs)
        # Prepare the data
        if not self.generator_ready:
            for tensor in ['obs', 'action', 'value',
                           'log_prob', 'adv', 'return']:
                self.infos[tensor] = self.swap_and_flatten(self.infos[tensor])
            self.generator_ready = True

        # Return everything, don't create mini-batches
        if batch_size is None:
            batch_size = self.buffer_size * self.n_envs

        start_idx = 0
        while start_idx < self.buffer_size * self.n_envs:
            yield self._get_samples(indices[start_idx:start_idx + batch_size])
            start_idx += batch_size

    def _get_samples(self, batch_inds):
        return (self.infos["obs"][batch_inds],
                self.infos["action"][batch_inds],
                self.infos["value"][batch_inds].flatten(),
                self.infos["log_prob"][batch_inds].flatten(),
                self.infos["adv"][batch_inds].flatten(),
                self.infos["return"][batch_inds].flatten())

    @staticmethod
    def swap_and_flatten(tensor):
        """
        Swap and then flatten axes 0 (buffer_size) and 1 (n_envs)
        to convert shape from [n_steps, n_envs, ...] (when ... is the shape of the features)
        to [n_steps * n_envs, ...] (which maintain the order)

        :param tensor: (np.ndarray)
        :return: (np.ndarray)
        """
        shape = tensor.shape
        if len(shape) < 3:
            shape = shape + (1,)
        return tensor.swapaxes(0, 1).reshape(shape[0] * shape[1], *shape[2:])
