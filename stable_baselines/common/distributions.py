import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as layers
import tensorflow_probability as tfp
from gym import spaces
from tensorflow.keras.initializers import Orthogonal
from tensorflow.keras.layers import Dense


class Distribution(object):
    def __init__(self):
        super(Distribution, self).__init__()

    def log_prob(self, x):
        """
        returns the log likelihood

        :param x: (object) the taken action
        :return: (tf.Tensor) The log likelihood of the distribution
        """
        raise NotImplementedError

    def entropy(self):
        """
        Returns shannon's entropy of the probability

        :return: (tf.Tensor) the entropy
        """
        raise NotImplementedError

    def sample(self):
        """
        returns a sample from the probability distribution

        :return: (tf.Tensor) the stochastic action
        """
        raise NotImplementedError


class DiagGaussianDistribution(Distribution):
    """
    Gaussian distribution with diagonal covariance matrix,
    for continuous actions.

    :param action_dim: (int)  Number of continuous actions
    """

    def __init__(self, action_dim, log_std_init=0.0):
        super(DiagGaussianDistribution, self).__init__()
        self.distribution = None
        self.action_dim = action_dim
        self.mean_actions = None
        self.log_std = log_std_init

    def proba_distribution_net(self):
        """
        Create the layers and parameter that represent the distribution:
        one output will be the mean of the gaussian, the other parameter will be the
        standard deviation (log std in fact to allow negative values)

        :return:
        """
        mean_layer = Dense(self.action_dim, name='pi/mean', activation='tanh',
                           kernel_initializer=Orthogonal(np.sqrt(2)))
        # log_std = tf.Variable(tf.ones(self.action_dim) * log_std_init)
        log_std_layer = Dense(self.action_dim, name='pi/log_std', activation='linear',
                              kernel_initializer=Orthogonal(np.sqrt(2)))
        # scale q_value results to [-1,1] with hyperbolic tangent
        q_values = Dense(self.action_dim, name='q', activation='tanh',
                         kernel_initializer=Orthogonal(np.sqrt(2)))
        return (mean_layer, log_std_layer), q_values
        # return mean_layer, log_std, q_values

    def proba_distribution(self, mean_actions, log_std, deterministic=False):
        """
        Create and sample for the distribution given its parameters (mean, std)

        :param mean_actions: (tf.Tensor)
        :param log_std: (tf.Tensor)
        :param deterministic: (bool)
        :return: (tf.Tensor)
        """
        action_std = tf.ones_like(mean_actions) * tf.exp(log_std)
        self.distribution = tfp.distributions.Normal(mean_actions, action_std)
        if deterministic:
            action = self.mode()
        else:
            action = self.sample()
        return action, self

    def mode(self):
        return self.distribution.mode()

    def sample(self):
        return self.distribution.sample()

    def entropy(self):
        return self.distribution.entropy()

    def log_prob_from_params(self, mean_actions, log_std):
        """
        Compute the log probability of taking an action
        given the distribution parameters.

        :param mean_actions: (tf.Tensor)
        :param log_std: (tf.Tensor)
        :return: (tf.Tensor, tf.Tensor)
        """
        action, _ = self.proba_distribution(mean_actions, log_std)
        log_prob = self.log_prob(action)
        return log_prob, self

    def log_prob(self, action):
        """
        Get the log probability of an action given a distribution.
        Note that you must call `proba_distribution()` method
        before.

        :param action: (tf.Tensor)
        :return: (tf.Tensor)
        """
        log_prob = self.distribution.log_prob(action)
        if len(log_prob.shape) > 1:
            log_prob = tf.reduce_sum(log_prob, axis=1)
        else:
            log_prob = tf.reduce_sum(log_prob)
        return log_prob


class CategoricalDistribution(Distribution):
    """
    Categorical distribution for discrete actions.

    :param action_dim: (int) Number of discrete actions
    """

    def __init__(self, action_dim):
        super(CategoricalDistribution, self).__init__()
        self.distribution = None
        self.action_dim = action_dim

    def proba_distribution_net(self):
        """
        Create the layer that represents the distribution:
        it will be the logits of the Categorical distribution.
        You can then get probabilities using a softmax.

        :return: (tf.keras.models.Layer)
        """
        action_logits = Dense(self.action_dim, name='action_logits', activation='tanh',
                              kernel_initializer=Orthogonal(np.sqrt(2)))
        return action_logits

    def proba_distribution(self, action_logits, deterministic=False):
        self.distribution = tfp.distributions.Categorical(logits=action_logits)
        if deterministic:
            action = self.mode()
        else:
            action = self.sample()
        return action, self

    def mode(self):
        return self.distribution.mode()

    def sample(self):
        return self.distribution.sample()

    def entropy(self):
        return self.distribution.entropy()

    def log_prob_from_params(self, action_logits):
        action, _ = self.proba_distribution(action_logits)
        log_prob = self.log_prob(action)
        return action, log_prob

    def log_prob(self, action):
        log_prob = self.distribution.log_prob(action)
        return log_prob


class MultiCategoricalDistribution(Distribution):
    def __init__(self, action_dim_list):
        """
            Categorical distribution for list of discrete actions
        :param action_dim_list: (List[int]) Number of discrete action list
        """
        super(MultiCategoricalDistribution, self).__init__()
        self.action_dim_list = action_dim_list
        self.distribution = None

    def proba_distribution_net(self):
        """
        Create the layer that represents the distribution:
        it will be the logits of the Categorical distribution.
        You can then get probabilities using a softmax.

        :return: (tf.keras.models.Sequential)
        """
        action_logits = layers.Dense(sum(self.action_dim_list), activation='tanh')
        return action_logits

    def proba_distribution(self, action_logits_list: np.ndarray, deterministic=False):
        self.distribution = [tfp.distributions.Categorical(
            logits=action_logits) for action_logits in action_logits_list]
        if deterministic:
            action_list = self.mode()
        else:
            action_list = self.sample()
        return action_list, self

    def mode(self):
        return tf.stack([p.mode() for p in self.distribution], axis=-1)

    def sample(self):
        return tf.stack([p.sample() for p in self.distribution], axis=-1)

    def entropy(self):
        return tf.add_n([p.entropy() for p in self.distribution], axis=-1)

    def log_prob(self, x):
        return tf.add_n([p.log_prob(px) for p, px in zip(self.distribution, tf.unstack(x, axis=-1))])

    def log_prob_from_params(self, action_logits_list):
        actions, _ = self.proba_distribution(action_logits_list)
        log_prob = self.log_prob(actions)
        return actions, log_prob


class BernoulliDistribution(Distribution):
    def __init__(self, size: int):
        """
        The probability distribution type for Bernoulli input

        :param size: (int) the number of dimensions of the Bernoulli distribution
        """
        super(BernoulliDistribution, self).__init__()
        self.size = size
        self.distribution = None

    def proba_distribution_net(self):
        """
        Create the layer that represents the distribution:
        it will be the logits of the Categorical distribution.
        You can then get probabilities using a softmax.

        :return: (tf.keras.models.Layer)
        """
        action_logits = layers.Dense(self.size, activation='tanh')
        return action_logits

    def proba_distribution(self, action_logits, deterministic: bool = False):
        self.distribution = tfp.distributions.Bernoulli(logits=action_logits)
        if deterministic:
            action = self.mode()
        else:
            action = self.sample()
        return action, self

    def mode(self):
        return self.distribution.mode()

    def sample(self):
        return self.distribution.sample()

    def entropy(self):
        return self.distribution.entropy()

    def log_prob_from_params(self, action_logits):
        action, _ = self.proba_distribution(action_logits)
        log_prob = self.log_prob(action)
        return action, log_prob

    def log_prob(self, action):
        log_prob = self.distribution.log_prob(action)
        return log_prob


def make_proba_distribution(action_space):
    """
    Return an instance of Distribution for the correct type of action space

    :param action_space: (Gym Space) the input action space
    :return: (Distribution) the appropriate Distribution object
    """
    if isinstance(action_space, spaces.Box):
        assert len(action_space.shape) == 1, "Error: the action space must be a vector"
        return DiagGaussianDistribution(action_space.shape[0])
    elif isinstance(action_space, spaces.Discrete):
        return CategoricalDistribution(action_space.n)
    elif isinstance(action_space, spaces.MultiDiscrete):
        return MultiCategoricalDistribution(action_space.nvec)
    elif isinstance(action_space, spaces.MultiBinary):
        return BernoulliDistribution(action_space.n)
    else:
        raise NotImplementedError("Error: probability distribution, not implemented for action space of type {}."
                                  .format(type(action_space)) +
                                  " Must be of type Gym Spaces: Box, Discrete, MultiDiscrete or MultiBinary.")
