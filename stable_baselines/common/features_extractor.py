import typing as tp

import numpy as np
import tensorflow as tf
from itertools import zip_longest
from tensorflow.keras import Model
from tensorflow.keras.initializers import Orthogonal
from tensorflow.keras.layers import Dense, Conv2D, GlobalAveragePooling2D, LSTM
from tensorflow.keras.models import Sequential
from tensorflow.keras.regularizers import l1_l2

# define the variable type
from stable_baselines.common.utils import batch_to_seq


class BaseExtractor(Model):

    def __init__(self, name: str = "baseExtractor", **kwargs):
        super(BaseExtractor, self).__init__(name=name, **kwargs)

    def save(self, path: str, **kwargs) -> None:
        """
        Save model to a given location.
        :param path: (str) file location
        """
        raise NotImplementedError()

    def load(self, path: str) -> None:
        """
        Load saved model from path.
latent_policy
        :param path: (str)
        """
        raise NotImplementedError()

    def call(self, x, **kwargs):
        raise NotImplementedError()


class MLP(BaseExtractor):
    """
    Constructs an MLP that receives observations as an input and outputs a latent representation for the policy and
    a value network. The ``net_arch`` parameter allows to specify the network topology

    1. An arbitrary length (zero allowed) number of integers each specifying the number of units in a shared layer.
       If the number of ints is zero, there will be no shared layers.
    2. An optional dict, to specify the following non-shared layers for the value network and the policy network.
       It is formatted like ``dict(vf=[<value layer sizes>], pi=[<policy layer sizes>])``.
       If it is missing any of the keys (pi or vf), no non-shared layers (empty list) is assumed.

    For example to construct a network with one shared layer of size 55 followed by two non-shared layers for the value
    network of size 255 and a single non-shared layer of size 128 for the policy network, the following layers_spec
    would be used: ``[55, dict(vf=[255, 255], pi=[128])]``. A simple shared network topology with two layers of size 128
    would be specified as [128, 128].


    :param feature_dim: (int) Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: ([int or dict]) The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: () The activation function to use for the networks.
    """

    def __init__(self,
                 net_arch: tp.List[tp.Union[int, tp.Dict]],
                 activation_fn: tp.Union[tp.Callable[..., tp.Any], str],
                 name="MlpExtractor", **kwargs):
        super(BaseExtractor, self).__init__(name, **kwargs)
        # Declaration here
        self.recurrent_net = None
        self.shared_net: tp.List = []
        self.policy_net: tp.List = []
        self.value_net: tp.List = []

        # Layer sizes of the network that only belongs to the policy network
        policy_only_layers: tp.List[tp.List[int]] = []
        # Layer sizes of the network that only belongs to the value network
        value_only_layers: tp.List[tp.List[int]] = []
        last_layer_dim_pi: int
        last_layer_dim_vf: int

        # Iterate through the shared layers and build the shared parts of the network
        for idx, layer in enumerate(net_arch):
            if isinstance(layer, int):  # Check that this is a shared layer
                layer_size = layer

                self.shared_net.append(Dense(layer_size, name=f"shared_fc{idx}",
                                             kernel_initializer=Orthogonal(np.sqrt(2)),
                                             activation=activation_fn))
            else:
                assert isinstance(layer, dict), "Error: the net_arch list can only contain ints and dicts"
                if 'lstm' in layer:
                    self.recurrent_net = LSTM(units=layer['lstm'][0], name='lstm_0',
                                              return_state=True,
                                              kernel_initializer=Orthogonal(np.sqrt(2)),
                                              activation='tanh')
                if 'pi' in layer:
                    assert isinstance(layer['pi'],
                                      list), "Error: net_arch[-1]['pi'] must contain a list of integers."
                    policy_only_layers = layer['pi']

                if 'vf' in layer:
                    assert isinstance(layer['vf'],
                                      list), "Error: net_arch[-1]['vf'] must contain a list of integers."
                    value_only_layers = layer['vf']
                break  # From here on the network splits up in policy and value network

        # Build the non-shared part of the network
        for idx, (pi_layer_size, vf_layer_size) in enumerate(zip_longest(policy_only_layers, value_only_layers)):
            if pi_layer_size is not None:
                assert isinstance(pi_layer_size, int), "Error: net_arch[-1]['pi'] must only contain integers."
                self.policy_net.append(
                    Dense(pi_layer_size, name=f"pi_fc{idx}",
                          kernel_initializer=Orthogonal(np.sqrt(2)),
                          activation=activation_fn))

            if vf_layer_size is not None:
                assert isinstance(vf_layer_size, int), "Error: net_arch[-1]['vf'] must only contain integers."
                self.value_net.append(
                    Dense(vf_layer_size, name=f"vf_fc{idx}",
                          kernel_initializer=Orthogonal(np.sqrt(2)),
                          activation=activation_fn))

        self.shared_net.append(Dense(128, name=f"sparse_fc1",
                                     kernel_initializer=Orthogonal(np.sqrt(2)),
                                     activation='tanh', activity_regularizer=l1_l2(0.5, 0.5)))

        # Save dim, used to create the distributions

        # Create networks
        # If the list of layers is empty, the network will just act as an Identity module

    @tf.function
    def call(self, features, state=None, n_env=1, n_steps=None, mask=None, **kwargs) -> tp.Tuple[tf.Tensor, tf.Tensor,
                                                                                                 tf.Tensor, tf.Tensor]:
        """
        :param n_env:
        :type n_env:
        :param state:
        :type state:
        :param n_steps:
        :type n_steps:
        :param mask:
        :type mask:
        :param features:
        :return: (tf.Tensor, tf.Tensor) latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        print(f'state value from mlp extractor function {state}')
        prev_output = features
        for shared_layer in self.shared_net:
            prev_output = shared_layer(prev_output)
        shared_latent = prev_output
        if self.recurrent_net is not None:
            input_sequence = batch_to_seq(shared_latent, n_env, n_steps)
            mask = batch_to_seq(mask, n_env, n_steps)
            rnn_output, new_h, new_d = self.recurrent_net(input_sequence, initial_state=state, mask=mask)
            new_state = [new_h, new_d]
            shared_latent = tf.reshape(rnn_output, [n_env, -1])
            state = new_state

        value_latent = shared_latent
        policy_latent = shared_latent

        for vf_layer, pi_layer in zip_longest(self.value_net, self.policy_net):
            if vf_layer is not None:
                value_latent = vf_layer(value_latent)
            if pi_layer is not None:
                policy_latent = pi_layer(policy_latent)

        return policy_latent, value_latent, shared_latent, state

    def save(self, path: str, **kwargs) -> None:
        pass

    def load(self, path: str) -> None:
        pass


class NatureCnn(BaseExtractor):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        activ = 'relu'
        self.cnn = Sequential()
        self.cnn.add(Conv2D(activation=activ, filters=32,
                            kernel_size=8, strides=4, name='c1',
                            kernel_initializer=Orthogonal(np.sqrt(2)), **kwargs))
        self.cnn.add(Conv2D(activation=activ, filters=64,
                            kernel_size=4, strides=2, name='c2',
                            kernel_initializer=Orthogonal(np.sqrt(2)), **kwargs))
        self.cnn.add(Conv2D(activation=activ, filters=64,
                            kernel_size=3, strides=1, name='c3',
                            kernel_initializer=Orthogonal(np.sqrt(2)), **kwargs))
        self.cnn.add(GlobalAveragePooling2D())
        self.cnn.add(Dense(units=512, activation=activ, name='fc1',
                           kernel_initializer=Orthogonal(np.sqrt(2)), **kwargs))

    def save(self, path: str, **kwargs) -> None:
        pass

    def load(self, path: str) -> None:
        pass

    def call(self, x, **kwargs):
        return self.cnn(x)
