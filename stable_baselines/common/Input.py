from functools import partial

import gym
import numpy as np
import tensorflow as tf
from gym.spaces import Discrete, Box, MultiBinary, MultiDiscrete
from tensorflow.python.layers.base import Layer


def discrete_method(dimension, input_tensor):
    return tf.cast(tf.one_hot(tf.cast(input_tensor, dtype=tf.int32), dimension), tf.float32)


def box_method(scale, high, low, input_tensor):
    _obs = tf.cast(input_tensor, tf.float32)
    # rescale to [1, 0] if the bounds are defined
    if (scale and
            not np.any(np.isinf(low)) and not np.any(np.isinf(high)) and
            np.any((high - low) != 0)):
        # equivalent to processed_observations / 255.0 when bounds are set to [255, 0]
        return (_obs - low) / (high - low)
    return _obs


def multi_binary_method(inputs):
    return tf.cast(inputs, tf.float32)


def multi_discrete(nvec, inputs):
    return tf.concat(
        [tf.cast(tf.one_hot(tf.cast(input_split, dtype=tf.int32), nvec[i]), tf.float32) for i, input_split
         in enumerate(tf.split(inputs, len(nvec), axis=-1))
         ],
        axis=-1)


class ObservationInput(Layer):
    def __init__(self, ob_space: gym.spaces, scale=False, **kwargs):
        super(ObservationInput, self).__init__(**kwargs)
        if isinstance(ob_space, Discrete):
            self.method = partial(discrete_method, ob_space.n)
        elif isinstance(ob_space, Box):
            self.method = partial(box_method, scale, ob_space.high, ob_space.low)
        elif isinstance(ob_space, MultiBinary):
            self.method = multi_binary_method
        elif isinstance(ob_space, MultiDiscrete):
            self.method = partial(multi_discrete, ob_space.nvec)
        else:
            raise NotImplementedError("Error: the model does not support input space of type {}".format(
                type(ob_space).__name__))

    def call(self, inputs, **kwargs):
        return self.method(inputs)


if __name__ == '__main__':
    sp = gym.spaces.Discrete(10)
    tensor = tf.Variable([0, 1, 2, 0, 9, 7.5])
    print(tensor)
    obs_layer = ObservationInput(ob_space=sp)
    print(obs_layer(tensor))
    sp = gym.spaces.Box(-10, 2, shape=(1, 8))
    tensor = tf.Variable([[-10, 0.5, 0.1, 1, 2, -5, -2, 0], [-5, 2, 1, 1, 2, -5, -2, 0]])
    print(tensor)
    obs_layer = ObservationInput(ob_space=sp, scale=True)
    print(obs_layer(tensor))
    sp_multiDiscrete = gym.spaces.MultiDiscrete([5, 2, 2])
    tensor = tf.Variable([[4, 0, 0], [0, 0, 0]])
    print(tensor)
    obs_layer = ObservationInput(ob_space=sp_multiDiscrete)
    print(obs_layer(tensor))
