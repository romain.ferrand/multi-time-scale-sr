import typing as tp

import gym
import numpy as np
import tensorflow as tf
import time
from gym import spaces

from stable_baselines.common import logger
from stable_baselines.common.base_class import BaseRLModel
from stable_baselines.common.buffers import RolloutBuffer
from stable_baselines.common.utils import explained_variance
from stable_baselines.common.vec_env import SubprocVecEnv, DummyVecEnv
from stable_baselines.ppo.policies import PPOPolicy


class VanillaAC(BaseRLModel):
    def set_env(self, env):
        pass

    def __init__(self, policy: tp.Union[PPOPolicy, str], env: tp.Union[SubprocVecEnv, DummyVecEnv],
                 learning_rate: float = 1e-3,
                 n_steps: int = 2048, batch_size: int = 128, n_epochs: int = 100,
                 gamma: float = 0.99, gae_lambda: float = 0.95,
                 tensorboard_log: str = None, create_eval_env: bool = False,
                 policy_kwargs: tp.Dict = None, verbose: int = 0, seed: int = 0,
                 _init_setup_model: bool = True):

        super(VanillaAC, self).__init__(policy, env, PPOPolicy, policy_kwargs=policy_kwargs,
                                        verbose=verbose, seed=seed)

        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.n_steps = n_steps
        self.gamma = gamma
        self.gae_lambda = gae_lambda
        self.rollout_buffer = None
        self.tensorboard_log = tensorboard_log
        self.tb_writer = None

        if _init_setup_model:
            self._setup_model()

    def _setup_model(self):
        self._setup_learning_rate()
        # TODO: pre-processing: one hot vector for obs discrete
        state_dim = self.observation_space.shape[0]
        action_dim = 0
        if isinstance(self.action_space, spaces.Box):
            # Action is a 1D vector
            action_dim = self.action_space.shape[0]
        elif isinstance(self.action_space, spaces.Discrete):
            # Action is a scalar
            action_dim = 1

        # TODO: different seed for each env when n_envs > 1
        if self.n_envs == 1:
            self.set_random_seed(self.seed)

        self.rollout_buffer = RolloutBuffer(self.n_steps, state_dim, action_dim,
                                            gamma=self.gamma, gae_lambda=self.gae_lambda, n_envs=self.n_envs)
        self.policy = self.policy_class(self.observation_space, self.action_space,
                                        self.learning_rate, **self.policy_kwargs)

    def predict(self, observation, state=None, mask=None, deterministic=False):
        """
        Get the model's action from an observation

        :param observation: (np.ndarray) the input observation
        :param state: (np.ndarray) The last states (can be None, used in recurrent policies)
        :param mask: (np.ndarray) The last masks (can be None, used in recurrent policies)
        :param deterministic: (bool) Whether or not to return deterministic actions.
        :return: (np.ndarray, np.ndarray) the model's action and the next state (used in recurrent policies)
        """
        clipped_actions = self.policy.actor_forward(np.array(observation).reshape(1, -1), deterministic=deterministic)
        if isinstance(self.action_space, gym.spaces.Box):
            clipped_actions = np.clip(clipped_actions, self.action_space.low, self.action_space.high)
        return clipped_actions

    @tf.function
    def policy_loss(self, advantage, log_prob):

        policy_loss = advantage * log_prob
        return - tf.reduce_mean(policy_loss)

    @tf.function
    def value_loss(self, values, batch_return):
        return tf.keras.losses.MSE(values, batch_return)

    def train(self, gradient_steps, batch_size=64):
        # Update optimizer learning rate
        # self._update_learning_rate(self.policy.optimizer)

        for gradient_step in range(gradient_steps):
            approx_kl_divs = []
            # Sample replay buffer
            for replay_data in self.rollout_buffer.get(batch_size):
                # Unpack
                obs, action, old_values, old_log_prob, advantage, batch_return = replay_data

                if isinstance(self.action_space, spaces.Discrete):
                    # Convert discrete action for float to long
                    action = action.astype(np.int64).flatten()

                with tf.GradientTape() as tape:
                    tape.watch(self.policy.trainable_variables)
                    values, log_prob, entropy = self.policy.evaluate_actions(obs, action)
                    # Flatten
                    values = tf.reshape(values, [-1])
                    policy_loss = self.policy_loss(advantage, values)
                    value_loss = self.value_loss(values, batch_return)

                    loss = policy_loss + value_loss

                # Optimization step
                gradients = tape.gradient(loss, self.policy.trainable_variables)
                # Clip grad norm
                # gradients = tf.clip_by_norm(gradients, self.max_grad_norm)
                self.policy.optimizer.apply_gradients(zip(gradients, self.policy.trainable_variables))

        explained_var = explained_variance(self.rollout_buffer.infos["return"].flatten(),
                                           self.rollout_buffer.infos["value"].flatten())

        logger.log_kv("explained_variance", explained_var)
        # TODO: gather stats for the entropy and other losses?
        logger.log_kv("entropy", entropy.numpy().mean())
        logger.log_kv("policy_loss", policy_loss.numpy())
        logger.log_kv("value_loss", value_loss.numpy())
        if hasattr(self.policy, 'log_std'):
            logger.log_kv("std", tf.exp(self.policy.log_std).numpy().mean())

    def learn(self, total_timesteps, callback=None, log_interval=1,
              eval_env=None, eval_freq=-1, n_eval_episodes=5, tb_log_name="PPO", reset_num_timesteps=True):

        timesteps_since_eval, iteration, evaluations, obs, eval_env = self._setup_learn(eval_env)

        if self.tensorboard_log is not None:
            import os
            self.tb_writer = tf.summary.create_file_writer(os.path.join(self.tensorboard_log, tb_log_name))

        while self.num_timesteps < total_timesteps:

            if callback is not None:
                # Only stop training if return value is False, not when it is None.
                if callback(locals(), globals()) is False:
                    break

            obs = self.collect_rollouts(self.env, self.policy,
                                        self.rollout_buffer, n_rollout_steps=self.n_steps, obs=obs)
            iteration += 1
            self.num_timesteps += self.n_steps * self.n_envs
            timesteps_since_eval += self.n_steps * self.n_envs
            self._update_current_progress(self.num_timesteps, total_timesteps)

            # Display training infos
            if self.verbose >= 1 and log_interval is not None and iteration % log_interval == 0:
                fps = int(self.num_timesteps / (time.time() - self.start_time))
                logger.log_kv("iterations", iteration)
                if len(self.ep_info_buffer) > 0 and len(self.ep_info_buffer[0]) > 0:
                    logger.log_kv('ep_rew_mean', self.safe_mean([ep_info['r'] for ep_info in self.ep_info_buffer]))
                    logger.log_kv('ep_len_mean', self.safe_mean([ep_info['l'] for ep_info in self.ep_info_buffer]))
                logger.log_kv("fps", fps)
                logger.log_kv('time_elapsed', int(time.time() - self.start_time))
                logger.log_kv("total timesteps", self.num_timesteps)
                logger.dump_kvs()

            self.train(self.n_epochs, batch_size=self.batch_size)

            # Evaluate the agent
            timesteps_since_eval = self._eval_policy(eval_freq, eval_env, n_eval_episodes,
                                                     timesteps_since_eval, deterministic=True)
            # For tensorboard integration
            # if self.tb_writer is not None:
            #     with self.tb_writer.as_default():
            #         tf.summary.scalar('Eval/reward', mean_reward, self.num_timesteps)

        return self
