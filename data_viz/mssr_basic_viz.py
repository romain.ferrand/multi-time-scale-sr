import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from matplotlib import cm
import numpy as np
import pandas as pd
from scipy.stats import rankdata
from yaml import load, dump
from os import listdir
from os.path import isfile, join

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


def compute_viz(directory: str):
    files = [f for f in listdir(directory) if isfile(join(directory, f))]
    for file in files:
        if 'test_reward_signal' in file:
            viz_return_signal(join(directory, file))
        elif 'test_reward_penalty' in file:
            viz_penalty_signal(join(directory, file))
        elif 'test_signal_recovery' in file:
            viz_signal_recovery(join(directory, file))


def viz_entropy(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)
    result = dataset['results']
    for k, v in result.items():
        print(k)
        print(len(v))
    df = pd.DataFrame(result)
    fig = plt.figure()
    ax = sns.barplot(x="indices",
                     y="distance",
                     hue="entropy",
                     data=df)

    fig.tight_layout()
    plt.show()


def viz_signal_recovery(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)
    result = dataset['results']
    df = pd.DataFrame(result)
    ax = sns.barplot(x="distribution", y="reconstruction_error", hue="feature_length",
                     data=df)
    plt.axhline(y=dataset["exp_parameters"]["lower_bound"], c='black', linestyle='dashed')
    title = 'Mean reconstruction error in terms of distribution and feature length\n' \
            f'Initial reconstruction error after HP optimization is {dataset["exp_parameters"]["lower_bound"]}'
    ax.set_title(title)
    lgd = ax.legend()
    lgd.set_title('feature length')
    ax.set_ylabel('Reconstruction error')
    plt.show()


def viz_penalty_signal(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)

    fig = plt.figure()
    # use_entropy = dataset['structural_parameters']['use_entropy']
    title_rad = 'Exploration bonus results'
    # title_entropy = ' with entropy discount' if use_entropy else ' without entropy discount'
    title_distribution = f'\nwith {dataset["structural_parameters"]["distribution"]} distribution'
    title = title_rad + title_distribution
    result = dataset['results']
    for k, v in result.items():
        print(k)
        print(len(v))
    df = pd.DataFrame(result)
    ax = sns.barplot(x="indices", y="bonus", hue="surprise",
                     data=df)
    ax.set_xlabel('Features indices')
    ax.set_ylabel('Bonus signal')
    ax.set_title(title)
    fig.tight_layout()
    plt.show()

    # df = pd.DataFrame(result)
    # indices = df['indices']
    # pivot = df.pivot_table(
    #     values='penalty', index='batch_number',
    #     columns='indices', aggfunc='mean', fill_value=0
    # )
    # gp = df.groupby(
    #     ['batch_number', 'indices']).size().unstack('indices', fill_value=0)
    # fig = plt.figure()
    # cmap = plt.get_cmap('jet', np.max(indices) - np.min(indices) + 1)
    # cmap.set_under('gray')
    # ax = df.plot.scatter(x='index', y='penalty',
    #                      c='indices', s=10, cmap=cmap,
    #                      vmin=0, vmax=np.max(indices), title=title)
    # ax.set_xlabel('Time')
    # fig.tight_layout()
    # plt.show()
    # fig = plt.figure()
    # title_rad = 'Mean penalty per batch'
    # title = title_rad + title_entropy + title_distribution
    # ax = pivot.plot.bar(title=title)
    # ax.set_ylabel('Mean penalty')
    # ax.set_xlabel('Batch number')
    # fig.tight_layout()
    # plt.show()
    # fig = plt.figure()
    # title_rad = 'Feature frequency per batch'
    # title = title_rad + title_entropy + title_distribution
    # ax = gp.plot.bar(title=title)
    # ax.set_xlabel('Batch number')
    # ax.set_ylabel('Frequency')
    # fig.tight_layout()
    # plt.show()


def viz_immediate_reward_signal(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)
    result = dataset['results']

    df = pd.DataFrame(result)
    df['expected_time_from_central'] = \
        (df['expected_time_from_central'] - df['expected_time_from_central'].min()) / (
                df['expected_time_from_central'].max() - df['expected_time_from_central'].min())
    # use_entropy = dataset['structural_parameters']['use_entropy']
    # reward_is_dependent = dataset['experiment_parameters']['reward_dependency']

    title_rad = 'Return results thought time in term of central feature distance\n'
    # title_entropy = ' with entropy discount' if use_entropy else ' without entropy discount'
    # title_dependence = ' and reward-feature ' + ('dependence' if reward_is_dependent else 'independence')
    title = title_rad
    fig, ax = plt.subplots()
    c = ax.scatter(x=df['index'],
                   y=df['immediate_reward'],
                   c=df['similarities_from_central_feature'],
                   s=20, cmap=cm.get_cmap('magma'))
    ax.set_title(title)
    ax.set_xlabel('Time')
    ax.set_ylabel('Immediate reward value')
    cb = fig.colorbar(c, ax=ax)

    cb.set_label('Distance from central feature')
    fig.tight_layout()
    plt.show()

    # fig, ax = plt.subplots()
    # c = ax.scatter(x=df['index'],
    #                y=df['expected_time_from_central'],
    #                s=20)
    # title_rad = 'Expected time from central feature thought time\n'
    # title = title_rad + title_entropy + title_dependence
    # ax.set_title(title)
    # ax.set_xlabel('Time')
    # ax.set_ylabel('Expected time from central feature')
    # cb = fig.colorbar(c, ax=ax)
    # cb.set_label('Expected time from central feature')
    # fig.tight_layout()
    # plt.show()

    # fig, ax = plt.subplots()
    #
    # title_rad = 'Return results in term of time-distance from central feature\n'
    # title = title_rad  # + title_entropy + title_dependence
    # c = ax.scatter(x=df['similarities_from_central_feature'],
    #                y=df['expected_time_from_central'],
    #                c=df['immediate_reward'],
    #                s=20, cmap=cm.get_cmap('magma'))
    # ax.set_title(title)
    # ax.set_xlabel('Distance from central feature')
    # ax.set_ylabel('Relative time from central feature')
    # cb = fig.colorbar(c, ax=ax)
    # cb.set_label('Immediate reward value')
    # fig.tight_layout()
    # plt.show()


def viz_return_signal_sequence(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)
    result = dataset['results']

    df = pd.DataFrame(result)
    # use_entropy = dataset['structural_parameters']['use_entropy']
    # reward_is_dependent = dataset['experiment_parameters']['reward_dependency']

    title_rad = 'Returns estimations \nin terms of nodes indices\n'
    # title_entropy = ' with entropy discount' if use_entropy else ' without entropy discount'
    # title_dependence = ' and reward-feature ' + ('dependence' if reward_is_dependent else 'independence')
    title = title_rad
    fig, ax = plt.subplots()
    df['indices'] = df['indices'].astype(int)
    c = ax.scatter(x=df['index'],
                   y=df['returns'],
                   c=df['indices'],
                   s=20, cmap=cm.get_cmap('magma'))
    ax.set_title(title)
    cb = fig.colorbar(c, ax=ax)
    cb.set_label('Nodes indices')
    ax.set_title(title)
    ax.set_xlabel('Time')
    ax.set_ylabel('Return estimation value')
    fig.tight_layout()
    plt.show()
    fig, ax = plt.subplots()

    title = 'Average returns estimations \nin terms of nodes indices'
    ax = sns.barplot(x="indices", y="returns",
                     data=df)
    ax.set_xlabel('Nodes indices (DF notation)')
    ax.set_ylabel('Return estimation value')
    ax.set_title(title)
    fig.tight_layout()
    plt.show()
    title = 'Average immediate rewards \nestimations in terms of nodes indices'
    ax = sns.barplot(x="indices", y="immediate_rewards",
                     data=df)
    ax.set_xlabel('Nodes indices (DF notation)')
    ax.set_ylabel('Immediate reward estimation value')
    ax.set_title(title)
    fig.tight_layout()
    plt.show()
    title = 'True returns \nin terms of nodes indices'
    dataset['tree_indices'] = np.array(dataset['tree_indices']).astype(int)
    fig, ax = plt.subplots()
    ax = sns.barplot(dataset['tree_indices'], dataset['true_return'])
    ax.set_xlabel('Nodes indices (DF notation)')
    ax.set_ylabel('True return value')
    ax.set_title(title)
    fig.tight_layout()
    plt.show()


def viz_return_signal(file: str):
    with open(file, 'r') as fd_read:
        dataset = load(fd_read, Loader=Loader)
    result = dataset['results']

    df = pd.DataFrame(result)
    df['expected_time_from_central'] = \
        (df['expected_time_from_central'] - df['expected_time_from_central'].min()) / (
                df['expected_time_from_central'].max() - df['expected_time_from_central'].min())
    # use_entropy = dataset['structural_parameters']['use_entropy']
    # reward_is_dependent = dataset['experiment_parameters']['reward_dependency']

    title_rad = 'Return results thought time in term of central feature distance\n'
    title_distribution = f" with {dataset['structural_parameters']['distribution']} distribution"
    # title_entropy = ' with entropy discount' if use_entropy else ' without entropy discount'
    # title_dependence = ' and reward-feature ' + ('dependence' if reward_is_dependent else 'independence')
    title = title_rad + title_distribution
    fig, ax = plt.subplots()
    c = ax.scatter(x=df['index'],
                   y=df['returns'],
                   c=df['similarities_from_central_feature'],
                   s=20, cmap=cm.get_cmap('magma'))
    ax.set_title(title)
    ax.set_xlabel('Time')
    ax.set_ylabel('Return value')
    cb = fig.colorbar(c, ax=ax)

    cb.set_label('Distance from central feature')
    fig.tight_layout()
    plt.show()

    # fig, ax = plt.subplots()
    # c = ax.scatter(x=df['index'],
    #                y=df['expected_time_from_central'],
    #                s=20)
    # title_rad = 'Expected time from central feature thought time\n'
    # title = title_rad + title_entropy + title_dependence
    # ax.set_title(title)
    # ax.set_xlabel('Time')
    # ax.set_ylabel('Expected time from central feature')
    # cb = fig.colorbar(c, ax=ax)
    # cb.set_label('Expected time from central feature')
    # fig.tight_layout()
    # plt.show()

    # fig, ax = plt.subplots()
    #
    # title_rad = 'Return results in term of time-distance from central feature\n'
    # title = title_rad  # + title_entropy + title_dependence
    # c = ax.scatter(x=df['similarities_from_central_feature'],
    #                y=df['expected_time_from_central'],
    #                c=df['returns'],
    #                s=20, cmap=cm.get_cmap('magma'))
    # ax.set_title(title)
    # ax.set_xlabel('Distance from central feature')
    # ax.set_ylabel('Relative time from central feature')
    # cb = fig.colorbar(c, ax=ax)
    # cb.set_label('Return value')
    # fig.tight_layout()
    # plt.show()


if __name__ == '__main__':
    # compute_viz('../tests')
    # viz_return_signal('../tests/test_reward_signal_20200811-142936.yaml')
    # viz_return_signal_sequence('../tests/test_reward_signal_20200812-195930.yaml')
    # viz_immediate_reward_signal('../tests/test_immediate_reward_20200811-114105.yaml')
    #viz_penalty_signal('../tests/test_reward_bonus_20200813-230721.yaml')
    #viz_penalty_signal('../tests/test_reward_bonus_20200813-115813.yaml')
    viz_penalty_signal('../tests/test_reward_bonus_20200813-231436.yaml')

    # viz_entropy('../tests/test_entropy_20200624-045245.yaml')
