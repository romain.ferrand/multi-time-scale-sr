from typing import Tuple, Union, Any, List, Dict

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, Model, Input


def get_un_compiled_seq_model(input_shape: Tuple[int, ...], output_shape: int) -> Model:
    _inputs = Input(shape=input_shape, name="digits")
    x = layers.Dense(64, activation='relu', name='dense_1')(_inputs)
    x = layers.Dense(64, activation='relu', name='dense_2')(x)
    _outputs = layers.Dense(output_shape, name='predictions')(x)
    _model = Model(inputs=inputs, outputs=_outputs)
    _model.summary()
    return _model


def get_un_compiled_autoencoder_model(input_shape: Tuple[int, ...] = (28, 28, 1)) -> Model:
    encoder_input = keras.Input(shape=input_shape, name='img')
    x = layers.Conv2D(16, 3, activation='relu')(encoder_input)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.MaxPooling2D(3)(x)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.Conv2D(16, 3, activation='relu')(x)
    encoder_output = layers.GlobalMaxPooling2D()(x)

    encoder = keras.Model(encoder_input, encoder_output, name='encoder')
    encoder.summary()

    x = layers.Reshape((4, 4, 1))(encoder_output)
    x = layers.Conv2DTranspose(16, 3, activation='relu')(x)
    x = layers.Conv2DTranspose(32, 3, activation='relu')(x)
    x = layers.UpSampling2D(3)(x)
    x = layers.Conv2DTranspose(16, 3, activation='relu')(x)
    decoder_output = layers.Conv2DTranspose(1, 3, activation='relu')(x)
    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')
    autoencoder.summary()
    return autoencoder.inputs.get()


# Manipulate complex graph topologies

def multiple_input_output() -> Model:
    num_tags = 12  # Number of unique issue tags
    num_words = 10000  # Size of vocabulary obtained when pre-processing text data
    num_departments = 4  # Number of departments for predictions

    title_input = keras.Input(shape=(None,), name='title')  # Variable-length sequence of ints
    body_input = keras.Input(shape=(None,), name='body')  # Variable-length sequence of ints
    tags_input = keras.Input(shape=(num_tags,), name='tags')  # Binary vectors of size `num_tags`

    # Embed each word in the title into a 64-dimensional vector
    title_features = layers.Embedding(num_words, 64)(title_input)
    # Embed each word in the text into a 64-dimensional vector
    body_features = layers.Embedding(num_words, 64)(body_input)

    # Reduce sequence of embedded words in the title into a single 128-dimensional vector
    title_features = layers.LSTM(128)(title_features)
    # Reduce sequence of embedded words in the body into a single 32-dimensional vector
    body_features = layers.LSTM(32)(body_features)

    # Merge all available features into a single large vector via concatenation
    x = layers.concatenate([title_features, body_features, tags_input])

    # Stick a logistic regression for priority prediction on top of the features
    priority_pred = layers.Dense(1, name='priority')(x)
    # Stick a department classifier on top of the features
    department_pred = layers.Dense(num_departments, name='department')(x)

    # Instantiate an end-to-end _model predicting both priority and department
    _model = keras.Model(inputs=[title_input, body_input, tags_input],
                         outputs=[priority_pred, department_pred])

    _model.compile(optimizer=keras.optimizers.RMSprop(1e-3),
                   # priority = first output
                   # department = second output
                   loss={'priority': keras.losses.BinaryCrossentropy(from_logits=True),
                         'department': keras.losses.CategoricalCrossentropy(from_logits=True)},
                   loss_weights=[1., 0.2]
                   # => total loss = 1. * priority_loss + 0.2 * department loss
                   )
    keras.utils.plot_model(_model, 'multi_input_and_output_model.png', show_shapes=True)

    # Dummy input data
    title_data = np.random.randint(num_words, size=(1280, 10))
    body_data = np.random.randint(num_words, size=(1280, 100))
    tags_data = np.random.randint(2, size=(1280, num_tags)).astype('float32')

    # Dummy target data
    priority_targets = np.random.random(size=(1280, 1))
    dept_targets = np.random.randint(2, size=(1280, num_departments))

    _model.fit({'title': title_data, 'body': body_data, 'tags': tags_data},
               {'priority': priority_targets, 'department': dept_targets},
               epochs=2,
               batch_size=32)
    return _model


# ResNet toy example
def res_net(input_shape: Tuple[int, ...], output_shape: int):
    inputs = keras.Input(shape=input_shape, name='img')
    x = layers.Conv2D(32, 3, activation='relu')(inputs)
    x = layers.Conv2D(64, 3, activation='relu')(x)
    block_1_output = layers.MaxPooling2D(3)(x)

    x = layers.Conv2D(64, 3, activation='relu', padding='same')(block_1_output)
    x = layers.Conv2D(64, 3, activation='relu', padding='same')(x)
    block_2_output = layers.add([x, block_1_output])

    x = layers.Conv2D(64, 3, activation='relu', padding='same')(block_2_output)
    x = layers.Conv2D(64, 3, activation='relu', padding='same')(x)
    block_3_output = layers.add([x, block_2_output])

    x = layers.Conv2D(64, 3, activation='relu')(block_3_output)
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(256, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(output_shape)(x)

    _model = keras.Model(inputs, outputs, name='toy_res-net')
    _model.summary()
    keras.utils.plot_model(_model, 'mini_res-net.png', show_shapes=True)
    return _model


def toy_dataset_workflow(_model: Model, _x_train, _y_train, _x_test, _y_test):
    _model.compile(loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                   optimizer=keras.optimizers.RMSprop(),
                   metrics=['accuracy'])
    history = _model.fit(_x_train, _y_train,
                         batch_size=64, epochs=10,
                         validation_split=0.2)
    test_scores = _model.evaluate(_x_test, _y_test, verbose=2)
    print(f"Test loss: {test_scores[0]}\n"
          f"Test accuracy: {test_scores[1]}")


# Shared layers : layers that are use multiple times in the model
def shared_model():
    # Embedding for 1000 unique words mapped to 128-dimensional vectors
    shared_embedding = layers.Embedding(1000, 128)

    # Variable-length sequence of integers
    text_input_a = keras.Input(shape=(None,), dtype='int32')

    # Variable-length sequence of integers
    text_input_b = keras.Input(shape=(None,), dtype='int32')

    # Reuse the same layer to encode both inputs
    encoded_input_a = shared_embedding(text_input_a)
    encoded_input_b = shared_embedding(text_input_b)
    return encoded_input_a, encoded_input_b


# Extract and reuse nodes in the graph of layers
def vgg19_example():
    # model are static Data-structure with acces and inspection capacities
    # => extract intermediate layers ("nodes") and reuse them elsewhere
    # example: VGG19 model for feature extraction
    vgg19 = tf.keras.applications.VGG19()
    # intermediate activations of the model, obtained by querying the graph data structure:
    features_list = [layer.output for layer in vgg19]
    # create new-feature extraction model (neural transfer)
    feat_extract_model = tf.keras.Model(inputs=vgg19.input, outputs=features_list)


# Custom layers: allow to extends Keras api
# + Call: specificity the compute made by the layer
# + build: creates the weight (__init__ can be use for that build is a convention)

class Custom_Dense(layers.Layer):
    def __init__(self, units: int = 32):
        super(Custom_Dense, self).__init__()
        self.w: Union[None, layers.Layer, Any] = None
        self.b: Union[None, layers.Layer, Any] = None

        self.units = units

    def build(self, input_shape: Union[tf.TensorShape, List[tf.TensorShape]]):
        self.w = self.add_weight(shape=(input_shape[-1], self.units),
                                 initializer='random.normal',
                                 trainable=True)
        self.b = self.add_weight(shape=(self.units,),
                                 initializer='random.normal',
                                 trainable=True)

    def call(self, inputs: Union[tf.Tensor, Tuple[tf.Tensor, ...], List[tf.Tensor]], **kwargs):
        return tf.matmul(inputs, self.w) + self.b


# serialization support

class Custom_Dense_2(layers.Layer):
    def __init__(self, units: int = 32, **kwargs):
        super(Custom_Dense_2, self).__init__(**kwargs)
        self.units = units
        self.w: Union[None, layers.Layer, Any] = None
        self.b: Union[None, layers.Layer, Any] = None

    def build(self, input_shape: Union[tf.TensorShape, List[tf.TensorShape]]):
        self.w = self.add_weight(shape=(input_shape[-1], self.units),
                                 initializer='RandomNormal',
                                 trainable=True)
        self.b = self.add_weight(shape=(self.units,),
                                 initializer='RandomNormal',
                                 trainable=True)

    def call(self, inputs: Union[tf.Tensor, Tuple[tf.Tensor, ...], List[tf.Tensor]], **kwargs):
        return tf.matmul(inputs, self.w) + self.b

    # export all non-trainable parameters for serializable support

    def get_config(self) -> Dict:
        return {'units': self.units}


if __name__ == '__main__':
    """
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train_seq = x_train.reshape(60000, 784).astype('float32') / 255
    x_test_seq = x_test.reshape(10000, 784).astype('float32') / 255
    _model_seq = get_un_compiled_seq_model((784,), 10)
    # 0.97 accuracy
    toy_dataset_workflow(_model_seq, x_train_seq, y_train, x_test_seq, y_test)
    _model_res_net = res_net(input_shape=(28, 28, 1), output_shape=10)
    x_train_res_net = x_train.reshape(60000, 28, 28, 1) / 255
    x_test_res_net = x_test.reshape(10000, 28, 28, 1) / 255
    # 0.995 accuracy
    toy_dataset_workflow(_model_res_net, x_train_res_net, y_train, x_test_res_net, y_test)
    """
    inputs = keras.Input((4,))
    outputs = Custom_Dense_2(10)(inputs)
    model = keras.Model(inputs, outputs)
    config = model.get_config()
    new_model = keras.Model.from_config(config,
                                        custom_objects={Custom_dense_2})
    model.get_config()
