import numpy as np
import tensorflow as tf
from ray.rllib.models.modelv2 import ModelV2
from ray.rllib.models.tf.recurrent_tf_modelv2 import RecurrentTFModelV2
from ray.rllib.models.tf.tf_modelv2 import TFModelV2
from ray.rllib.utils import override
from tensorflow.keras.initializers import Orthogonal
from tensorflow.keras.regularizers import l1_l2

from Auto_encoder.AE.dense_SAE import DenseSAEncoder, DenseSADecoder
from Auto_encoder.VAE.dense_SVAE import DenseSVAEncoder, DenseSVADecoder
from distributions.hyperspherical_uniform import HypersphericalUniform
from distributions.von_mises_fisher import VonMisesFisher


class FcModel(TFModelV2):
    """Custom model for policy gradient algorithms."""

    def __init__(self, obs_space, action_space,
                 num_outputs, model_config, name):
        super(FcModel, self).__init__(
            obs_space, action_space,
            num_outputs, model_config, name
        )
        act_func = model_config['fcnet_activation']
        sparse_size = model_config['custom_options']['sparse_size']
        sparse_l1 = model_config['custom_options']['sparse_l1']
        sparse_l2 = model_config['custom_options']['sparse_l2']
        pfc_size = model_config['custom_options']['pfc_size']
        sparse_activity_regularizer = l1_l2(sparse_l1, sparse_l2)
        self.inputs = tf.keras.layers.Input(
            shape=(None, obs_space.shape[0]), name="observations")

        layer_n = self.inputs

        for i, layer in enumerate(model_config['fcnet_hiddens']):
            layer_n = tf.keras.layers.Dense(
                layer,
                name=f"fc_{i}",
                activation=act_func,
                kernel_initializer=Orthogonal(np.sqrt(2)))(layer_n)
        sparse_out = tf.keras.layers.Dense(
            sparse_size,
            name="sparse_layer",
            activation='tanh',
            kernel_initializer=Orthogonal(np.sqrt(2)),
            activity_regularizer=sparse_activity_regularizer)(layer_n)

        pfc_out = tf.keras.layers.Dense(
            pfc_size,
            name="pfc_layer",
            activation=act_func,
            kernel_initializer=Orthogonal(np.sqrt(2)))(sparse_out)
        layer_out = tf.keras.layers.Dense(
            num_outputs,
            name="actor_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        value_out = tf.keras.layers.Dense(
            1,
            name="value_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        self.base_model = tf.keras.Model(self.inputs,
                                         [layer_out, value_out,
                                          sparse_out])
        self.register_variables(self.base_model.variables)

    @override(ModelV2)
    def forward(self, input_dict, state, seq_lens):
        model_out, self._value_out, \
        self._sparse_out = self.base_model(input_dict["obs"])

        return model_out, state

    def value_function(self):
        return tf.reshape(self._value_out, [-1])

    def sparse_features(self):
        return self._sparse_out


class RnnPFC(RecurrentTFModelV2):
    """Example of using the Keras functional API to define a RNN model."""

    def __init__(self,
                 obs_space,
                 action_space,
                 num_outputs,
                 model_config,
                 name):
        super(RnnPFC, self).__init__(obs_space, action_space, num_outputs,
                                     model_config, name)
        act_func = model_config['fcnet_activation']
        sparse_size = model_config['custom_options']['sparse_size']
        sparse_l1 = model_config['custom_options']['sparse_l1']
        sparse_l2 = model_config['custom_options']['sparse_l2']
        sparse_activity_regularizer = l1_l2(sparse_l1, sparse_l2)
        self.cell_size = model_config['lstm_cell_size']
        # Define input layers
        self.inputs = tf.keras.layers.Input(
            shape=(None, obs_space.shape[0]), name="inputs")
        state_in_h = tf.keras.layers.Input(shape=(model_config['lstm_cell_size'],), name="h")
        state_in_c = tf.keras.layers.Input(shape=(model_config['lstm_cell_size'],), name="c")
        seq_in = tf.keras.layers.Input(shape=(), name="seq_in", dtype=tf.int32)

        # Preprocess observation with a hidden layer and send to LSTM cell
        layer_n = self.inputs
        for i, layer in enumerate(model_config['fcnet_hiddens']):
            layer_n = tf.keras.layers.Dense(
                layer,
                name=f"fc_{i}",
                activation=act_func,
                kernel_initializer=Orthogonal(np.sqrt(2)))(layer_n)
        sparse_out = tf.keras.layers.Dense(
            sparse_size,
            name="sparse_layer",
            activation='sigmoid',
            kernel_initializer=Orthogonal(np.sqrt(2)),
            activity_regularizer=sparse_activity_regularizer)(layer_n)
        lstm_out, state_h, state_c = tf.keras.layers.LSTM(
            model_config['lstm_cell_size'],
            return_sequences=True, return_state=True, name="pfc_lstm"
        )(inputs=sparse_out, mask=tf.sequence_mask(seq_in),
          initial_state=[state_in_h, state_in_c])

        logits = tf.keras.layers.Dense(
            self.num_outputs,
            activation=tf.keras.activations.linear,
            name="logits")(lstm_out)

        values = tf.keras.layers.Dense(
            1, activation=None, name="values")(lstm_out)

        # Create the RNN model
        self.rnn_model = tf.keras.Model(
            inputs=[self.inputs, seq_in, state_in_h, state_in_c],
            outputs=[logits, values, state_h, state_c, sparse_out])
        self.register_variables(self.rnn_model.variables)
        self.rnn_model.summary()

    @override(RecurrentTFModelV2)
    def forward_rnn(self, inputs, state, seq_lens):
        model_out, self._value_out, h, c, self._sparse_out = \
            self.rnn_model([inputs, seq_lens] + state)
        return model_out, [h, c]

    @override(ModelV2)
    def get_initial_state(self):
        return [
            np.zeros(self.cell_size, np.float32),
            np.zeros(self.cell_size, np.float32),
        ]

    @override(ModelV2)
    def value_function(self):
        return tf.reshape(self._value_out, [-1])

    def sparse_features(self):
        return self._sparse_out


class FcModelSAE(TFModelV2):
    """Custom model for policy gradient algorithms."""

    def __init__(self, obs_space, action_space,
                 num_outputs, model_config, name):
        super(FcModelSAE, self).__init__(
            obs_space, action_space,
            num_outputs, model_config, name
        )
        act_func = model_config['fcnet_activation']
        sparse_size = model_config['custom_options']['sparse_size']
        pfc_size = model_config['custom_options']['pfc_size']
        self.inputs = tf.keras.layers.Input(
            shape=(None, obs_space.shape[0]), name="observations")
        self.encoder = DenseSAEncoder(model_config['fcnet_hiddens'],
                                      activation=act_func,
                                      z_dim=sparse_size)
        self.decoder = DenseSADecoder(obs_space.shape[0],
                                      model_config['fcnet_hiddens'],
                                      activation=act_func)
        sparse_out = self.encoder(self.inputs, training=False)

        pfc_out = tf.keras.layers.Dense(
            pfc_size,
            name="pfc_layer",
            activation=act_func,
            kernel_initializer=Orthogonal(np.sqrt(2)))(sparse_out)
        layer_out = tf.keras.layers.Dense(
            num_outputs,
            name="actor_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        value_out = tf.keras.layers.Dense(
            1,
            name="value_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        self.base_model = tf.keras.Model(self.inputs,
                                         [layer_out, value_out,
                                          sparse_out])
        self.register_variables(self.base_model.variables)

    @override(ModelV2)
    def forward(self, input_dict, state, seq_lens):
        model_out, self._value_out, \
        self._sparse_out = self.base_model(input_dict["obs"])

        return model_out, state

    def value_function(self):
        return tf.reshape(self._value_out, [-1])

    def sparse_features(self):
        return self._sparse_out

    @override(ModelV2)
    def custom_loss(self, policy_loss, loss_inputs):
        # Create a new input reader per worker.
        z = self.encoder(loss_inputs["obs"])
        obs_logits = self.decoder(z)
        self.bce = tf.nn.sigmoid_cross_entropy_with_logits(logits=obs_logits, labels=loss_inputs["obs"])
        reconstruction_loss = tf.reduce_mean(tf.reduce_sum(self.bce, axis=-1))
        return policy_loss + reconstruction_loss


class FcModelSVAE(TFModelV2):
    """Custom model for policy gradient algorithms."""

    def __init__(self, obs_space, action_space,
                 num_outputs, model_config, name):
        super(FcModelSVAE, self).__init__(
            obs_space, action_space,
            num_outputs, model_config, name
        )
        act_func = model_config['fcnet_activation']
        sparse_size = model_config['custom_options']['sparse_size']
        pfc_size = model_config['custom_options']['pfc_size']
        self.p_z = HypersphericalUniform(sparse_size - 1, dtype=tf.float32)
        self.inputs = tf.keras.layers.Input(
            shape=(None, obs_space.shape[0]), name="observations")
        self.encoder = DenseSVAEncoder(model_config['fcnet_hiddens'], z_dim=sparse_size)
        self.decoder = DenseSVADecoder(obs_space.shape[0], model_config['fcnet_hiddens'])
        z_mean, z_log_var, sparse_out = self.encoder(self.inputs, training=False)

        pfc_out = tf.keras.layers.Dense(
            pfc_size,
            name="pfc_layer",
            activation=act_func,
            kernel_initializer=Orthogonal(np.sqrt(2)))(sparse_out)
        layer_out = tf.keras.layers.Dense(
            num_outputs,
            name="actor_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        value_out = tf.keras.layers.Dense(
            1,
            name="value_layer",
            activation=None,
            kernel_initializer=Orthogonal(np.sqrt(2)))(pfc_out)
        self.base_model = tf.keras.Model(self.inputs,
                                         [layer_out, value_out,
                                          sparse_out, z_mean, z_log_var])
        self.register_variables(self.base_model.variables)

    @override(ModelV2)
    def forward(self, input_dict, state, seq_lens):
        model_out, self._value_out, self._sparse_out, \
        self._z_mean, self._z_log_var = self.base_model(input_dict["obs"])

        return model_out, state

    def value_function(self):
        return tf.reshape(self._value_out, [-1])

    def sparse_features(self):
        return self._sparse_out

    def z_mean(self):
        return self._z_mean

    def z_log_var(self):
        return self._z_log_var

    @override(ModelV2)
    def custom_loss(self, policy_loss, loss_inputs):
        # Create a new input reader per worker.
        z_mean, z_log_var, z = self.encoder(loss_inputs["obs"])
        self.q_z = VonMisesFisher(z_mean, z_log_var)
        obs_logits = self.decoder(z)
        self.bce = tf.nn.sigmoid_cross_entropy_with_logits(logits=obs_logits, labels=loss_inputs["obs"])
        kl = - self.q_z.kl_divergence(self.p_z)
        reconstruction_loss = tf.reduce_mean(tf.reduce_sum(self.bce, axis=-1))
        self.ELBO = - reconstruction_loss - tf.reduce_mean(kl)
        return policy_loss - 1 * self.ELBO

    @override(ModelV2)
    def metrics(self):
        return {
            "bce": self.bce,
            "ELBO": self.ELBO
        }
