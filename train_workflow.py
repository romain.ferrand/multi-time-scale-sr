import os
from typing import Optional, List

from mlagents_envs.environment import UnityEnvironment
from mlagents_envs.side_channel.engine_configuration_channel import EngineConfig, EngineConfigurationChannel
# from stable_baselines import VanillaAC
# from stable_baselines.common.noise import NormalActionNoise
from mlagents_envs.side_channel.float_properties_channel import FloatPropertiesChannel

from stable_baselines import PPO
from stable_baselines.common import logger
from stable_baselines.common.monitor import Monitor
from stable_baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from stable_baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from unity_API.unity_wrapper import UnityEnv

try:
    from mpi4py import MPI
except ImportError:
    MPI = None


def set_env(binary_name: str, env_window: bool, env_params: EngineConfig) -> UnityEnvironment:
    """
    Find and configure environment, set graphical output if needed
    :param binary_name: environment binary
    :param env_window: output graphics
    :param env_params: environment parameters
    :return:
    """
    env_name = binary_name  # Name of the Unity environment binary to launch
    engine_configuration_channel = EngineConfigurationChannel()
    env = UnityEnvironment(file_name=env_name, no_graphics=env_window,
                           side_channels=[engine_configuration_channel])

    # Reset the environment
    env.reset()
    engine_configuration_channel.set_configuration(env_params)
    return env


def make_unity_env(env_directory: str = "", num_env: int = 1, visual: bool = False,
                   start_index: int = 0,
                   channels: Optional[List[EngineConfigurationChannel]] = None):
    """
    Create a wrapped, monitored Unity environment.
    """

    def make_env(_rank, use_visual=True):  # pylint: disable=C0111
        def _thunk():
            env = UnityEnv(env_directory, use_visual=use_visual, flatten_branched=True,
                           worker_id=_rank,
                           side_channels=channels)
            env = Monitor(env, logger.get_dir() and os.path.join(logger.get_dir(), str(_rank)))
            return env

        return _thunk

    if visual:
        return SubprocVecEnv([make_env(i + start_index, use_visual=visual) for i in range(num_env)])
    else:
        rank = MPI.COMM_WORLD.Get_rank() if MPI else 0
        return DummyVecEnv([make_env(rank, use_visual=False)])


def main():
    engine_configuration_channel: EngineConfigurationChannel = EngineConfigurationChannel()
    new_env_params: EngineConfig = EngineConfig(width=1600,
                                                height=900,
                                                quality_level=1,
                                                time_scale=100,
                                                target_frame_rate=-1)
    engine_configuration_channel.set_configuration(new_env_params)
    dynamic_unity_params_channel = FloatPropertiesChannel()
    dynamic_unity_params_channel.set_property("max_step", 640)
    binary_file = 'unity-envs-bin/Plane_env_hard_v1'

    env = make_unity_env(binary_file, 1,
                         False, channels=[engine_configuration_channel, dynamic_unity_params_channel])
    model = PPO(policy="MlpLstmPolicy", n_mini_batches=1, n_steps=64, env=env, verbose=1,
                log_dir=binary_file + "/MlpLstmPolicy/",
                checkpoint_dir=binary_file + "/MlpLstmPolicy/")
    model.learn(1000000)


if __name__ == '__main__':
    main()
