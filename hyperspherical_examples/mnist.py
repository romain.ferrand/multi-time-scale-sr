import numpy as np
import tensorflow as tf
from tensorflow import keras

from VAE.dense_SVAE import DenseSVAE
import matplotlib.pyplot as plt


def train():
    original_dim = 784
    vae = DenseSVAE(original_dim, 64, 32)

    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)

    loss_metric = tf.keras.metrics.Mean()

    (x_train, _), _ = tf.keras.datasets.mnist.load_data()
    x_train = x_train.reshape(60000, 784).astype("float32") / 255
    x_train = x_train[:10000]

    train_dataset = tf.data.Dataset.from_tensor_slices(x_train)
    train_dataset = train_dataset.shuffle(buffer_size=256).batch(64)

    epochs = 30

    # Iterate over epochs.
    for epoch in range(epochs):
        print("Start of epoch %d" % (epoch,))

        # Iterate over the batches of the dataset.
        for step, x_batch_train in enumerate(train_dataset):
            with tf.GradientTape() as tape:
                reconstructed = vae(x_batch_train)
                # Compute reconstruction loss
                print(x_batch_train.shape)
                print(reconstructed.shape)
                bce = tf.nn.sigmoid_cross_entropy_with_logits(x_batch_train, reconstructed)
                reconstruction_loss = tf.reduce_mean(tf.reduce_sum(bce, axis=-1))

                ELBO = reconstruction_loss + sum(vae.losses)  # Add KLD regularization loss

            grads = tape.gradient(ELBO, vae.trainable_weights)
            optimizer.apply_gradients(zip(grads, vae.trainable_weights))

            loss_metric(ELBO)
            print("step %d: mean loss = %.4f" % (step, loss_metric.result()))

    return vae


def test(model: DenseSVAE):
    (x_train, _), _ = tf.keras.datasets.mnist.load_data()
    x_train = x_train.reshape(60000, 784).astype("float32") / 255
    images_idx = np.random.choice(np.arange(len(x_train)), 10)
    images_list = x_train[images_idx]
    for image in images_list:
        reconstructed_image = model(image.reshape(1, 784))
        reconstructed_image = reconstructed_image.numpy()
        image = image.reshape(28, 28)
        reconstructed_image = reconstructed_image.reshape(28, 28)
        fig, ax = plt.subplots()
        im = ax.imshow(image)
        fig, ax = plt.subplots()
        im = ax.imshow(reconstructed_image)
        plt.show()


def log_likelihood(svae: DenseSVAE, inputs, reconstructed, n=10):
    """

    :param svae:
    :param model: model object
    :param optimizer: optimizer object
    :param n: number of MC samples
    :return: MC estimate of log-likelihood
    """
    bce = tf.nn.sigmoid_cross_entropy_with_logits(labels=inputs, logits=reconstructed)
    reconstruction_loss = tf.reduce_mean(tf.reduce_sum(bce, axis=-1))
    z = svae.q_z.sample(n)

    log_p_z = svae.p_z.log_prob(z)

    log_p_x_z = -tf.reduce_sum(bce, axis=-1)

    log_q_z_x = svae.q_z.log_prob(z)

    return tf.reduce_mean(tf.reduce_logsumexp(
        tf.transpose(log_p_x_z + log_p_z - log_q_z_x) - np.log(n), axis=-1))


#
# # hidden dimension and dimension of latent space
# H_DIM = 128
# Z_DIM = 2
#
# # digit placeholder
# x = tf.placeholder(tf.float32, shape=(None, 784))
#
# # normal VAE
# modelN = ModelVAE(x=x, h_dim=H_DIM, z_dim=Z_DIM, distribution='normal')
# optimizerN = OptimizerVAE(modelN)
#
# # hyper-spherical VAE
# modelS = ModelVAE(x=x, h_dim=H_DIM, z_dim=Z_DIM + 1, distribution='vmf')
# optimizerS = OptimizerVAE(modelS)
#
# session = tf.Session()
# session.run(tf.global_variables_initializer())
#
# print('##### Normal VAE #####')
# for i in range(1000):
#     # training
#     x_mb, _ = mnist.train.next_batch(64)
#     # dynamic binarization
#     x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
#     session.run(optimizerN.train_step, {modelN.x: x_mb})
#
#     # every 100 iteration plot validation
#     if i % 100 == 0:
#         x_mb = mnist.validation.images
#         # dynamic binarization
#         x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
#         print(i, session.run({**optimizerN.print}, {modelN.x: x_mb}))
#
# print('Test set:')
# x_mb = mnist.test.images
# # dynamic binarization
# x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
# print_ = {'LL': log_likelihood(modelN, optimizerN, n=100)}
# print(session.run(print_, {modelN.x: x_mb}))
#
# print()
# print('##### Hyper-spherical VAE #####')
# for i in range(1000):
#     # training
#     x_mb, _ = mnist.train.next_batch(64)
#     # dynamic binarization
#     x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
#     session.run(optimizerS.train_step, {modelS.x: x_mb})
#
#     # every 100 iteration plot validation
#     if i % 100 == 0:
#         x_mb = mnist.validation.images
#         # dynamic binarization
#         x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
#         print(i, session.run({**optimizerS.print}, {modelS.x: x_mb}))
#
# print('Test set:')
# x_mb = mnist.test.images
# # dynamic binarization
# x_mb = (x_mb > np.random.random(size=x_mb.shape)).astype(np.float32)
#
# print_ = {'LL': log_likelihood(modelS, optimizerS, n=100)}
# print(session.run(print_, {modelS.x: x_mb}))

if __name__ == '__main__':
    model = train()
    test(model)
