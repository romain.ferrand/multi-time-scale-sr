""""Example of a custom gym environment and model. Run this for a demo.
This example shows:
  - using a custom environment
  - using a custom model
  - using Tune for grid search
You can visualize experiment results in ~/ray_results using TensorBoard.
"""
import ray
from mlagents_envs.side_channel.engine_configuration_channel import EngineConfigurationChannel, EngineConfig
from mlagents_envs.side_channel.environment_parameters_channel import EnvironmentParametersChannel
from ray import tune
from ray.rllib.models import ModelCatalog
from ray.rllib.utils import try_import_tf
from ray.tune import register_env

from config.tune_config import test_fc_pfc, test_rnn_pfc
from models.custom_rllib_models import FcModel, RnnPFC, FcModelSVAE, FcModelSAE
from policies.custom_rrlib_policy import PPO_with_sparse_trainer
from unity_API.unity_wrapper import UnityEnv

tf = try_import_tf()


def env_Factory(env_config):
    engine_configuration_channel: EngineConfigurationChannel = EngineConfigurationChannel()
    new_env_params: EngineConfig = EngineConfig(**env_config['engine'])
    engine_configuration_channel.set_configuration(new_env_params)

    dynamic_unity_params_channel = EnvironmentParametersChannel()
    for k, v in env_config['env_parameters']['unity-parameters'].items():
        dynamic_unity_params_channel.set_float_parameter(k, v)

    worker_id = env_config["unity_worker_id"] + env_config.worker_index
    # Name of the Unity environment binary to launch
    absolute_path = env_config['env_parameters']['path']
    return UnityEnv(absolute_path, use_visual=False,
                    flatten_branched=False, worker_id=worker_id,
                    side_channels=[engine_configuration_channel,
                                   dynamic_unity_params_channel])


if __name__ == "__main__":
    # Can also register the env creator function explicitly with:
    register_env("unity_env", lambda config: env_Factory(config))
    ray.init()
    ModelCatalog.register_custom_model("fc_pfc", FcModel)
    ModelCatalog.register_custom_model("fc_pfc_SVAE", FcModelSVAE)
    ModelCatalog.register_custom_model("fc_pfc_SAE", FcModelSAE)
    ModelCatalog.register_custom_model("rnn_pfc", RnnPFC)

    tune.run(
        PPO_with_sparse_trainer,
        stop={
            "timesteps_total": 1000000,
        },
        config=test_fc_pfc,
        checkpoint_at_end=True,
    )
