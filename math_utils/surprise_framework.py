import numpy as np
from scipy.spatial.distance import cosine

from distributions.hyperspherical_uniform import HypersphericalUniform
import tensorflow as tf

from distributions.von_mises_fisher import VonMisesFisher
from functools import partial


class SurpriseFramework:
    def __init__(self, feature_dim: int, gamma: float, variational: bool):
        """
        Evaluate the distribution of the most frequent features by running exponential windows
        on each observed features
        Compute a surprise score for each features based on similarity metric
        The score is bounded by the surprise score of the most surprising features

        On the variational case we directly provide a von-Mises-Fisher distribution
        and the self information score
        :param feature_dim:
        :type feature_dim:
        :param gamma:
        :type gamma:
        """
        self.gamma = gamma
        self.feature_dim = feature_dim
        self.variational = variational
        self.mean = np.zeros(feature_dim, dtype=float)
        self.concentration: float = 0.0
        self.last_concentration = 0.0
        self.last_mean = None
        if self.variational:
            self.uniform_pdf = HypersphericalUniform(feature_dim, dtype=tf.float32)
            base_feature = np.zeros(shape=(feature_dim,))
            base_feature[0] = 1
            self.max_log_prob = self.uniform_pdf.log_prob(base_feature)

        self.processed_element = 0

    def process_determinist_batch(self, inplace, mean_batch):
        if not inplace:
            mean_batch_mean = np.mean(mean_batch, axis=0)
            if self.processed_element == 0:
                return mean_batch_mean
            mean = 0.5 * mean_batch_mean + 0.5 * self.mean
            return mean

        # most recent result is the last element of the batch
        mean_batch = mean_batch[::-1]
        if self.last_mean is not None:
            mean_batch = np.concatenate((self.last_mean, mean_batch))
        self.last_mean = mean_batch[-1].reshape(1, -1)
        mean_batch = mean_batch[:-1]
        batch_len = mean_batch.shape[0]
        if self.processed_element == 0:
            # prevent a bias toward zero mean and zero concentration
            self.mean = mean_batch[0]
            mean_batch = mean_batch[1:]
            batch_len -= 1
        self.processed_element += batch_len
        if len(mean_batch) != 0:

            last_approximation_coef = np.exp(batch_len * np.log((1 - self.gamma)))
            coefs = np.exp(np.arange(0, batch_len) * np.log((1 - self.gamma))
                           + np.log(self.gamma))
            self.mean = np.sum(coefs[..., np.newaxis] * mean_batch, axis=0) \
                        + last_approximation_coef * self.mean
        return self.mean

    def process_variational_batch(self, inplace, mean_batch, concentration_batch):
        if not inplace:
            mean, concentration = self.mean, self.concentration
            if len(mean_batch) != 0:
                mean_batch_mean = np.mean(mean_batch, axis=0)
                mean = 0.5 * mean_batch_mean + 0.5 * self.mean

                concentration_batch_mean = np.mean(concentration_batch, axis=0)
                concentration = 0.5 * concentration_batch_mean + 0.5 * self.concentration
            return mean, concentration
        # most recent result is the last element of the batch
        mean_batch = mean_batch[::-1]
        concentration_batch = concentration_batch[::-1]
        if self.last_mean is not None:
            # add the last element of the previous batch at the top of the new batch
            mean_batch = np.concatenate((self.last_mean, mean_batch))
            concentration_batch = np.concatenate(([self.last_concentration], concentration_batch))
        # Save the last element for using it at first element of the next batch
        # cf MS_SR code
        self.last_mean = mean_batch[-1].reshape(1, -1)
        self.last_concentration = concentration_batch[-1]
        mean_batch = mean_batch[:-1]
        concentration_batch = concentration_batch[:-1]
        batch_len = mean_batch.shape[0]

        if self.processed_element == 0:
            # prevent a bias toward zero mean and zero concentration
            self.mean = mean_batch[0]
            self.concentration = concentration_batch[0]
            mean_batch = mean_batch[1:]
            concentration_batch = concentration_batch[1:]
            batch_len -= 1
        self.processed_element += batch_len
        if len(mean_batch) != 0:
            last_approximation_coef = np.exp(batch_len * np.log((1 - self.gamma)))
            coefs = np.exp(np.arange(0, batch_len) * np.log((1 - self.gamma))
                           + np.log(self.gamma))
            self.mean = np.sum(coefs[..., np.newaxis] * mean_batch, axis=0) \
                        + last_approximation_coef * self.mean
            self.concentration = np.sum(coefs * concentration_batch) \
                                 + last_approximation_coef * self.concentration
        return self.mean, self.concentration

    def process_batch(self, inplace, mean_batch, concentration_batch=None):
        if self.variational:
            return self.process_variational_batch(inplace, mean_batch, concentration_batch)
        else:
            return self.process_determinist_batch(inplace, mean_batch)

    def surprise_variational_score(self, features_batch, concentration_batch, processed_batch):
        if processed_batch:
            mean, concentration = self.process_variational_batch(False, features_batch, concentration_batch)
        else:
            mean, concentration = self.mean, self.concentration
        directional_pdf = VonMisesFisher(mean, [concentration])
        log_prob_batch = directional_pdf.log_prob(features_batch)
        self_content_coef = -log_prob_batch
        self_content_coef = tf.maximum(
            self_content_coef,
            np.zeros(shape=(log_prob_batch.shape[0],)))
        max_log_prob = tf.reduce_max(self_content_coef)

        self_content_coef = tf.cond(tf.reduce_sum(self_content_coef) > 0,
                                    lambda: tf.divide(self_content_coef,
                                                      max_log_prob),
                                    lambda: tf.identity(self_content_coef))
        if tf.executing_eagerly():
            self_content_coef = self_content_coef.numpy() \
                                + np.ones(shape=(self_content_coef.shape[0],))
        else:
            self_content_coef = self_content_coef \
                                + tf.ones(shape=(self_content_coef.shape[0],))
        return self_content_coef

    def surprise_determinist_score(self, features_batch, processed_bach):
        if processed_bach:
            mean = self.process_determinist_batch(False, features_batch)
        else:
            mean = self.mean
        cosine_partial = partial(cosine, mean)
        scores = np.array(list(map(cosine_partial, features_batch)))
        scores = 1 - np.exp(-scores)
        return scores

    def surprise_score(self, features_batch, concentration_batch=None, processed_batch=False):
        if self.variational:
            return self.surprise_variational_score(features_batch,
                                                   concentration_batch, processed_batch)
        else:
            return self.surprise_determinist_score(features_batch, processed_batch)


def test_surprise(distribution, feature_dim,
                  batch_size, gamma, processed_batch, variational):
    _func = distribution['function']
    parameters = distribution['parameters']
    mean_b = _func(**parameters, size=(batch_size, feature_dim))
    mean_b_norm = np.linalg.norm(mean_b, axis=-1)
    mean_b /= mean_b_norm[..., np.newaxis]

    features_b = _func(**parameters, size=(batch_size, feature_dim))
    features_norm_b = np.linalg.norm(features_b, axis=-1)
    features_b /= features_norm_b[..., np.newaxis]
    surprise = SurpriseFramework(feature_dim, gamma, variational)

    if variational:
        print('Variational surprise score')
        concentration_b = np.random.uniform(10000, 20000, size=(batch_size,))
        mean, concentration = surprise.process_batch(True, mean_batch=mean_b,
                                                     concentration_batch=concentration_b)
        pdf = VonMisesFisher(mean, [concentration])
        print(f'Mean direction = {mean}')
        print(f'Direction concentration = {concentration}')
        print(f'Sampling from parameters')
        print(pdf.sample(10))
        score = surprise.surprise_score(features_b, concentration_b,
                                        processed_batch=processed_batch)
        print('Surprise score from ' + ('processed ' if processed_batch else 'unprocessed ') + 'features batch')
        print(score)
    else:
        print('Determinist surprise score')
        surprise.process_batch(True, mean_b)
        print(f'Mean = {surprise.mean}')
        score = surprise.surprise_score(features_b, processed_batch=processed_batch)
        print('Surprise score from ' + ('processed ' if processed_batch else 'unprocessed ') + 'features batch')
        print(score)


distributions = {
    'power': {
        'function': np.random.power,
        'parameters':
            {
                'a': 3,
            },
    },
    'beta_05': {
        # for the two districts "modes" of beta a=b=0.5
        'function': np.random.beta,
        'parameters':
            {
                'a': 0.5,
                'b': 0.5,
            }
    }
}

if __name__ == '__main__':
    test_surprise(distributions['power'], 128, 100, 0.9, processed_batch=True, variational=True)
    test_surprise(distributions['power'], 128, 100, 0.9, processed_batch=False, variational=True)

    test_surprise(distributions['power'], 128, 100, 0.9, processed_batch=True, variational=False)
    test_surprise(distributions['power'], 128, 100, 0.9, processed_batch=False, variational=False)

    test_surprise(distributions['beta_05'], 128, 100, 0.9, processed_batch=True, variational=True)
    test_surprise(distributions['beta_05'], 128, 100, 0.9, processed_batch=False, variational=True)

    test_surprise(distributions['beta_05'], 128, 100, 0.9, processed_batch=True, variational=False)
    test_surprise(distributions['beta_05'], 128, 100, 0.9, processed_batch=False, variational=False)

    # def set_vmf_pdf(self, features=None):
    #     print("Entering in vmf builder ...")
    #     if self.last_memory_status is not None:
    #         examples = self.last_memory_status
    #         if features is not None:
    #             if self.add_noise_to_features:
    #                 features += np.random.normal(0, 0.1, size=(features.shape[0], features.shape[-1]))
    #             if examples is not None:
    #                 examples = np.concatenate((examples, features))
    #             else:
    #                 examples = features
    #         normalized_examples = examples
    #         print(f"directions examples = {examples}")
    #         examples_norm = np.linalg.norm(examples, axis=-1)
    #         normalized_examples[examples_norm > 0] = \
    #             normalized_examples[examples_norm > 0] / examples_norm[examples_norm > 0][..., np.newaxis]
    #         print(f"Normalized examples = {normalized_examples}")
    #         norm_examples = np.linalg.norm(np.sum(normalized_examples, axis=0))
    #         r = norm_examples / len(normalized_examples)
    #         print(f"r coef = {r}")
    #         mean_vector = np.sum(normalized_examples, axis=0) / norm_examples
    #         print(f"Mean direction =  {mean_vector}")
    #         print(f"Mean direction norm = {np.linalg.norm(mean_vector)}")
    #         print(f"Dimension = {len(examples[0])}")
    #         k = r * (len(examples[0]) - r ** 2) / (1 - r ** 2)
    #         print(f" concentration coef = {k}")
    #         self.directional_pdf = VonMisesFisher(mean_vector, k)
    #     else:
    #         print("Memory is not set yet")
