import matplotlib.pyplot as plt
import numpy as np


def min_max_normalization(series):
    return (series - series.min(1)[..., np.newaxis]) \
           / (series.max(1)[..., np.newaxis] - series.min(1)[..., np.newaxis])


def plot_compare_series(true_series, pred_series, title='example', max_dimension=10):
    true_series = np.array(true_series)
    pred_series = np.array(pred_series)
    normalized_series = min_max_normalization(pred_series)
    true_series = min_max_normalization(true_series)
    max_dim = true_series.shape[-1]
    max_pred_step = normalized_series.shape[0]

    normalized_series = [true_series, normalized_series]
    fig, axis = plt.subplots(min(max_dim, max_dimension))
    fig.suptitle(title)
    for dimension, axe in enumerate(axis[:max_dimension]):
        for color, series in enumerate(normalized_series):
            if series.shape[0] > max_pred_step:
                x = np.arange(0, series.shape[0])
            else:
                x = np.arange(max_pred_step - series.shape[0], max_pred_step)
            axe.plot(x, series[:, dimension].ravel(), color=f'C{color}', label=f'C{color}')
        axe.legend()
    plt.show()
