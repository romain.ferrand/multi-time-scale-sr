import os

from hyper_optimizer.hyper_optimization import hp_objective_feature_recovery


def on_episode_start(info):
    pass


def on_sample_end_callback(info):
    pass


def on_episode_step(info):
    pass


def on_episode_end(info):
    policy = info['policy']['default_policy']
    if policy.use_sr:
        info['policy']['default_policy'].new_episodic_memory_state()


def on_postprocess_trajectory(info):
    print(info['result'].keys())


engine_parameter = {
    'width': 1200,
    'height': 700,
    'quality_level': 1,
    'time_scale': 10,
    'target_frame_rate': -1,
    'capture_frame_rate': 60,
}
envs_parameters = {
    'radial_maze': {
        'path': os.path.abspath('unity-envs-bin/RadialMaze'),
        'unity-parameters': {
            'alternateReward': 0,
            'numberOfPair': 3,
            'maxExpStep': 10,
            'preTrainPhase': 1.0,
        }
    },
    'ventro-dorsal': {
        'path': os.path.abspath('unity-envs-bin/ventrodorsal'),
        'unity-parameters': {
            'numberOfSubWall': 2,
            'spawningMode': 1,
            'decisionInterval': 10,
            'modalityDynamic': 3,
            'rewardMode': -1,
            'torus': 0,
        }
    },
    't_maze': {
        'path': os.path.abspath('unity-envs-bin/TMaze'),
        'unity-parameters': {
            'hardMode': 0,
            'hMazeHardTransitionProba': 0,
            'transitionFreq': 20,
        }
    },
    'open_world_maze': {
        'path': os.path.abspath('unity-envs-bin/OpenWorld'),
        'unity-parameters': {
            #  fixedFood is a float boolean
            'fixedFood': 1,
            # define the percentage of available to spawn the food
            # maybe named difficulty
            'spawnAreaMarginMultiplier': 0.9,
            # the number of retrieved reward until food change position
            'transitionFreq': 10,
            'directionalSpeed': 1,
            'rotationalSpeed': 200,
            'randomStartingAngle': 1,
        }
    },
    'hallway': {
        'path': os.path.abspath('unity-envs-bin/hallway'),
        'unity-parameters': {},
    },
}

test_rnn_pfc = {
    "env": "unity_env",
    "callbacks": {
        # "on_sample_end": partial(on_sample_end_partial_callback, multi_time_scale_sr),
        # "on_episode_step": on_episode_step,
        # "on_episode_end": on_episode_end,
        # "on_train_result": on_train_result,
        # "on_train_result": on_postprocess_trajectory,
    },
    "num_workers": 0,  # for multi env training
    "env_config": {
        'unity_worker_id': 52,
        'env_parameters': envs_parameters['t_maze'],
        'engine': engine_parameter,
    },
    # Number of steps after which the episode is forced to terminate. Defaults
    # to `env.spec.max_episode_steps` (if present) for Gym envs.
    # "horizon": 1.0e7,
    # Calculate rewards but don't reset the environment when the horizon is
    # hit. This allows value estimation and RNN state to span across logical
    # episodes denoted by horizon. This only has an effect if horizon != inf.
    "soft_horizon": True,
    # === Settings for the Trainer process ===
    # Number of GPUs to allocate to the trainer process. Note that not all
    # algorithms can take advantage of trainer GPUs. This can be fractional
    # (e.g., 0.3 GPUs),
    "num_gpus": 1,
    # Should use a critic as a baseline (otherwise don't use value baseline;
    # required for using GAE).
    "use_critic": True,
    # If true, use the Generalized Advantage Estimator (GAE)
    # with a value function, see https://arxiv.org/pdf/1506.02438.pdf.
    "use_gae": True,
    # The GAE(lambda) parameter.
    "lambda": 0.99,
    # Initial coefficient for KL divergence.
    "kl_coeff": 0.2,
    # Size of batches collected from each worker.
    "sample_batch_size": 1024,
    # Number of timesteps collected for each SGD round. This defines the size
    # of each SGD epoch.
    "train_batch_size": 128,
    # Total SGD batch size across all devices for SGD. This defines the
    # minibatch size within each epoch.
    "sgd_minibatch_size": 128,
    # Whether to shuffle sequences in the batch when training (recommended).
    "shuffle_sequences": True,
    # Number of SGD iterations in each outer loop (i.e., number of epochs to
    # execute per train batch).
    "num_sgd_iter": 20,
    # Stepsize of SGD.
    "lr": 1e-3,
    # Learning rate schedule.
    "lr_schedule": None,
    # Share layers for value function. If you set this to True, it's important
    # to tune vf_loss_coeff.
    "vf_share_layers": True,
    # Coefficient of the value function loss. IMPORTANT: you must tune this if
    # you set vf_share_layers: True.
    "vf_loss_coeff": 1,
    # Coefficient of the entropy regularizer.
    "entropy_coeff": 0.1,
    # Decay schedule for the entropy regularizer.
    "entropy_coeff_schedule": None,
    # PPO clip parameter.
    "clip_param": 0.3,
    # Clip param for the value function. Note that this is sensitive to the
    # scale of the rewards. If your expected V is large, increase this.
    "vf_clip_param": 10.0,
    # If specified, clip the global norm of gradients by this amount.
    "grad_clip": None,
    # Target value for KL divergence.
    "kl_target": 0.01,
    # Whether to rollout "complete_episodes" or "truncate_episodes".
    "batch_mode": "truncate_episodes",
    # Which observation filter to apply to the observation.
    "observation_filter": "NoFilter",
    # Uses the sync samples optimizer instead of the multi-gpu one. This is
    # usually slower, but you might want to try it if you run into issues with
    # the default optimizer.
    "simple_optimizer": False,
    # Use PyTorch as framework?
    "use_pytorch": False,
    "model": {
        # === Options for custom models ===
        # Name of a custom model to use
        "custom_model": 'rnn_pfc',
        # Whether to wrap the model with a LSTM
        "use_lstm": True,
        # Max seq len for training the LSTM, defaults to 20
        "max_seq_len": 64,
        # Size of the LSTM cell
        "lstm_cell_size": 128,
        # Whether to feed a_{t-1}, r_{t-1} to LSTM
        "lstm_use_prev_action_reward": False,
        "fcnet_activation": 'tanh',
        "fcnet_hiddens": [64],
        "custom_options": {
            "SR_config": {
                "use_SR": False,
                "structural_parameters": {
                    "feature_space_dim": 64,
                    "reward_space_dim": 16,
                    "nb_windows": 100,
                    "reward_estimator": True,
                    "use_entropy": False,
                    "add_noise_to_feature": False,
                    "hp_checking_scale_limit": False,
                    "hp_checking_rate": 10,
                },
                "hyperparameters": {
                    "hp_values": {
                        "t_star_min": 0.913,
                        "space_between_bins": 0.01,
                        "learning_rate": 0.005,
                        "precision": 24,
                    },
                    "study_db": "rllib_db",
                    "study_name": "fc_pfc",
                    "objective_func": hp_objective_feature_recovery,
                    "number_of_trial": 100,
                    "best_hp_result": 0.1,
                    "pre_train_optimization": False,
                    "recover_hp_from_study": False,
                },
                "use_penalty_bonus": False,
                "use_reward_from_memory": False,
                "direct_consume": False,
            },
            "sparse_size": 64,
            "sparse_l1": 0.5,
            "sparse_l2": 0.5,
        }
    }
}
test_fc_pfc = {
    "env": "unity_env",
    "callbacks": {
        # "on_sample_end": partial(on_sample_end_partial_callback, multi_time_scale_sr),
        # "on_episode_step": on_episode_step,
        # "on_episode_end": on_episode_end,
        # "on_train_result": on_train_result,
        # "on_train_result": on_postprocess_trajectory,
    },
    "num_workers": 0,  # for multi env training
    "env_config": {
        'unity_worker_id': 52,
        'env_parameters': envs_parameters['open_world_maze'],
        'engine': engine_parameter,
    },
    # === Environment Settings ===
    # Discount factor of the MDP.
    "gamma": 0.99,
    # Number of steps after which the episode is forced to terminate. Defaults
    # to `env.spec.max_episode_steps` (if present) for Gym envs.
    # "horizon": 3000,
    # Calculate rewards but don't reset the environment when the horizon is
    # hit. This allows value estimation and RNN state to span across logical
    # episodes denoted by horizon. This only has an effect if horizon != inf.
    "soft_horizon": False,
    # === Settings for the Trainer process ===
    # Number of GPUs to allocate to the trainer process. Note that not all
    # algorithms can take advantage of trainer GPUs. This can be fractional
    # (e.g., 0.3 GPUs).
    "num_gpus": 0,
    # Should use a critic as a baseline (otherwise don't use value baseline;
    # required for using GAE).
    "use_critic": True,
    # If true, use the Generalized Advantage Estimator (GAE)
    # with a value function, see https://arxiv.org/pdf/1506.02438.pdf.
    "use_gae": True,
    # The GAE(lambda) parameter.
    "lambda": 0.995,
    # Initial coefficient for KL divergence.
    "kl_coeff": 0.2,
    # Size of batches collected from each worker.
    "sample_batch_size": 1024,
    # Number of timesteps collected for each SGD round. This defines the size
    # of each SGD epoch.
    "train_batch_size": 1024,
    # Total SGD batch size across all devices for SGD. This defines the
    # minibatch size within each epoch.
    "sgd_minibatch_size": 128,
    # Whether to shuffle sequences in the batch when training (recommended).
    "shuffle_sequences": True,
    # Number of SGD iterations in each outer loop (i.e., number of epochs to
    # execute per train batch).
    "num_sgd_iter": 3,
    # Stepsize of SGD.
    "lr": 1e-4,
    # Learning rate schedule.
    "lr_schedule": None,
    # Share layers for value function. If you set this to True, it's important
    # to tune vf_loss_coeff.
    "vf_share_layers": True,
    # Coefficient of the value function loss. IMPORTANT: you must tune this if
    # you set vf_share_layers: True.
    "vf_loss_coeff": 1,
    # Coefficient of the entropy regularizer.
    "entropy_coeff": 0.01,
    # Decay schedule for the entropy regularizer.
    "entropy_coeff_schedule": None,
    # "eager": True,
    # PPO clip parameter.
    "clip_param": 0.3,
    # Clip param for the value function. Note that this is sensitive to the
    # scale of the rewards. If your expected V is large, increase this.
    "vf_clip_param": 10.0,
    # If specified, clip the global norm of gradients by this amount.
    "grad_clip": None,
    # Target value for KL divergence.
    "kl_target": 0.01,
    # Whether to rollout "complete_episodes" or "truncate_episodes".
    "batch_mode": "truncate_episodes",
    # Which observation filter to apply to the observation.
    "observation_filter": "NoFilter",
    # Uses the sync samples optimizer instead of the multi-gpu one. This is
    # usually slower, but you might want to try it if you run into issues with
    # the default optimizer.
    "simple_optimizer": True,
    # Use PyTorch as framework?
    "use_pytorch": False,
    "model": {
        # === Options for custom models ===
        # Name of a custom model to use
        "fcnet_activation": 'tanh',
        "fcnet_hiddens": [64],
        "custom_model": 'fc_pfc_SAE',
        # Max seq len for training the LSTM, defaults to 20
        "custom_options": {
            "SR_config": {
                "use_SR": True,
                "structural_parameters": {
                    "feature_space_dim": 64,
                    "reward_space_dim": 9,
                    "nb_windows": 100,
                    "reward_estimator": True,
                    "use_entropy": False,
                    "add_noise_to_feature": False,
                    "hp_checking_scale_limit": False,
                    "hp_checking_rate": 10,
                },
                "hyperparameters": {
                    "hp_values": {
                        "t_star_min": 1,
                        "space_between_bins": 0.001,
                        "learning_rate": 0.001,
                        "precision": 18,
                    },
                    "study_db": "rllib_db",
                    "study_name": "fc_pfc",
                    "objective_func": hp_objective_feature_recovery,
                    "number_of_trial": 100,
                    "best_hp_result": 0.1,
                    "pre_train_optimization": False,
                    "recover_hp_from_study": False,
                },
                "use_penalty_bonus": False,
                "use_reward_from_memory": True,
                "direct_consume": True,
            },
            "sparse_size": 64,
            "pfc_size": 128,
            "sparse_l1": 0.5,
            "sparse_l2": 0.5,
        }
    }
}
