import sys

import math
import numpy as np
import typing as typ
from scipy.interpolate import interp1d
from MSSR.multi_scale_SR import MultiScaleSR, TCM
from sklearn.metrics.pairwise import cosine_similarity


def recover_reward_from_reward_memory(reward_memory):
    number_of_reward_bins = reward_memory.shape[-1]
    reward_memory = np.where(reward_memory >= 0, reward_memory, 0)
    # print(f"reward_memory post = {reward_memory}")
    reward_norm = np.sum(reward_memory, axis=-1)
    reward_norm = np.where(reward_norm != 0, reward_norm, 1)
    reward_memory /= reward_norm[..., np.newaxis]
    step_btw_bin = 2 / (number_of_reward_bins - 1)
    bin_values = np.array([-1 + step_btw_bin * i for i in range(number_of_reward_bins)])
    recovered_reward = reward_memory.dot(bin_values)
    return recovered_reward


def memory_interpolation(memory: np.ndarray, new_signal_length: int):
    memory_len = memory.shape[0]
    x = np.linspace(0, new_signal_length, memory_len, endpoint=True)
    func_interpolation = interp1d(x, memory, kind='quadratic', axis=0)
    x_new = np.linspace(0, new_signal_length, new_signal_length, endpoint=True)
    new_memory = func_interpolation(x_new)
    return new_memory


def similarity_on_hypersphere(input_signal: np.ndarray, output_signal: np.ndarray, variance: bool = False):
    input_len = input_signal.shape[0]
    output_len = output_signal.shape[0]
    assert input_len >= output_len, "Input signal must be longer or equal than output signal"
    y_new = memory_interpolation(output_signal, input_len)
    # not really efficient but we prefer relying on sklearn implementation
    # could be use to compute a kind of variance by computing the mean per coll or row
    # from the diagonal
    pairewise_cosine_sims = 1 - cosine_similarity(input_signal, y_new)

    diag_sims = np.diagonal(pairewise_cosine_sims)
    score = np.mean(diag_sims)

    if variance:
        max_diag_distance = pairewise_cosine_sims - diag_sims
        max_diag_distance = np.linalg.norm(max_diag_distance, axis=-1)
        max_diag_distance = np.mean(max_diag_distance)
        score -= max_diag_distance
    print(diag_sims)
    if math.isnan(score) or math.isinf(score):
        return float('inf')
    return score


def cosine_similarity_from_memory(memory: np.ndarray, query_features: np.ndarray) -> np.ndarray:
    # distances_results = np.empty(shape=( memory.shape[0]))
    query_features_norm = np.linalg.norm(query_features, axis=-1)
    reference_norm = np.linalg.norm(memory, axis=-1)
    norm = reference_norm[..., np.newaxis] * query_features_norm
    norm = np.where(norm > 0, norm, 1)
    print(f"memory shape is = {memory.shape}")
    print(f"query shape is = {query_features.shape}")
    print(f"norm shape is = {norm.shape}")
    dot_prod = memory.dot(query_features.T)
    print(f"dot_prod shape is = {dot_prod.shape}")
    distances_results = (dot_prod / norm)
    distances_results = np.transpose(distances_results)
    print(f"transpose result shape is = {distances_results.shape}")

    assert distances_results.shape == (query_features.shape[0], memory.shape[0]), \
        f" shape are = {distances_results.shape}"
    return distances_results


def reward_bonus(tcm: typ.Union[MultiScaleSR, TCM], sparse_features_batch):
    bonus_batch = np.empty(shape=(len(sparse_features_batch, )))
    for feature_number, feature in enumerate(sparse_features_batch):
        sequence = tcm.set_episode(feature)
        distance = 1 - cosine_similarity(np.reshape(feature, (1, -1)), sequence)
        exp_distance = np.exp(-distance)
        bonus_batch[feature_number] = 1 - exp_distance.max()
    return bonus_batch


def reward_batch_to_one_hot(reward_space_dim, reward_batch: np.ndarray):
    step = 2 / (reward_space_dim - 1)
    bins = np.floor((reward_batch + 1) / step).astype(dtype=int)
    bins = np.maximum(bins, 0)
    bins = np.minimum(bins, reward_space_dim - 1)
    on_hot_vector = np.zeros(shape=(reward_batch.shape[0], reward_space_dim))
    on_hot_vector[np.arange(len(bins)), bins] = 1
    return on_hot_vector

def mssr_return_dep(mssr: MultiScaleSR, reward_dim: int,
                    feature_reward_batch: np.ndarray):
    if mssr.kernel is not None:
        features = feature_reward_batch[:, :-reward_dim]
        rewards = feature_reward_batch[:, -reward_dim:]
        one_step_rewards = recover_reward_from_reward_memory(rewards)

        return_approx = np.empty(shape=(len(feature_reward_batch, )))
        immediate_reward_approx = np.empty(shape=(len(feature_reward_batch, )))

        t_star = mssr.t_star
        scaled_t_star = 2 * (1 - 1 / (1 + t_star))
        print(f'scaled_t_star = {scaled_t_star}')
        for feature_number, (feature, reward, one_step_reward) in enumerate(zip(features, rewards, one_step_rewards)):
            # return sequences
            episodic_seq = mssr.set_episode(np.concatenate((feature, reward)))
            feature_seq = episodic_seq[:, :-reward_dim]
            reward_seq = episodic_seq[:, -reward_dim:]
            # time scale are supposed to be equal between reward and features MS_SR
            recovered_reward = recover_reward_from_reward_memory(reward_seq)
            print(f'reward_memory = {reward_seq}')
            print(f'recovered reward = {recovered_reward}')
            pairewise_cosine_sims = 1 - cosine_similarity(np.reshape(feature, (1, -1)), feature_seq)
            pairewise_cosine_sims = pairewise_cosine_sims.ravel()

            exp_distance = np.exp(-1 * (pairewise_cosine_sims + scaled_t_star))

            return_approx[feature_number] = one_step_reward + np.sum(exp_distance * recovered_reward)
            exp_distance_norm = np.sum(exp_distance)
            exp_distance_norm = 1 if exp_distance_norm <= 0 else exp_distance_norm
            immediate_reward_approx[feature_number] = np.sum(exp_distance * recovered_reward) / exp_distance_norm
        return return_approx, immediate_reward_approx
    else:
        return np.zeros(shape=(len(feature_reward_batch, )))


def mssr_return_tcm(mssr_features: MultiScaleSR,
                    mssr_reward: TCM,
                    sparse_features_batch: np.ndarray):
    """
    Compute return approximation in term of multi-timescale successor representation where features
    and reward signals are recorded in two independent MS_SR objects
    :param mssr_features: MS_SR for features
    :type mssr_features: MultiScaleSR
    :param mssr_reward: MS_SR for reward
    :type mssr_reward: MultiScaleSR
    :param sparse_features_batch: The batch to evaluate
    :type sparse_features_batch: np.ndarray
    :return: return approximation (time-space dependant return)
    :rtype:
    """
    if mssr_features.last_memory_status is not None and mssr_reward.last_memory_status is not None:
        return_approx = np.empty(shape=(len(sparse_features_batch, )))
        feature_memory = mssr_features.last_memory_status
        reward_memory = np.array(mssr_reward.last_memory_status)
        distance_metric = 1 - cosine_similarity(sparse_features_batch, feature_memory)

        for feature_number, feature in enumerate(sparse_features_batch):
            exp_distance = np.exp(-distance_metric[feature_number])
            return_approx[feature_number] = np.sum(exp_distance * reward_memory)
        return return_approx
    else:
        return np.zeros(shape=(len(sparse_features_batch, )))


def mssr_return_indep(mssr_features: MultiScaleSR, mssr_reward: MultiScaleSR, sparse_features_batch: np.ndarray):
    """
    Compute return approximation in term of multi-timescale successor representation where features
    and reward signals are recorded in two independent MS_SR objects
    :param mssr_features: MS_SR for features
    :type mssr_features: MultiScaleSR
    :param mssr_reward: MS_SR for reward
    :type mssr_reward: MultiScaleSR
    :param sparse_features_batch: The batch to evaluate
    :type sparse_features_batch: np.ndarray
    :return: return approximation (time-space dependant return)
    :rtype:
    """
    if mssr_features.last_memory_status is not None and mssr_reward.last_memory_status is not None:
        return_approx = np.empty(shape=(len(sparse_features_batch, )))
        feature_memory = mssr_features.last_memory_status
        reward_memory = np.array(mssr_reward.last_memory_status)
        # time scale are supposed to be equal between reward and features MS_SR
        gammas = mssr_features.discount_sr
        number_of_reward_bins = reward_memory.shape[-1]
        reward_norm = np.linalg.norm(reward_memory, axis=-1)
        reward_memory /= reward_norm[..., np.newaxis]
        step_btw_bin = 2 / (number_of_reward_bins - 1)
        bin_values = np.array([-1 + step_btw_bin * i for i in range(number_of_reward_bins)])
        recovered_reward = reward_memory.dot(bin_values)
        distance_metric = cosine_similarity(sparse_features_batch, feature_memory)

        for feature_number, feature in enumerate(sparse_features_batch):
            exp_distance = np.exp(-distance_metric[feature_number])
            return_approx[feature_number] = np.sum(exp_distance * (1 - gammas) * recovered_reward)
        return return_approx
    else:
        return np.zeros(shape=(len(sparse_features_batch, )))


def overlapping_feature_generation(distribution: str,
                                   number_of_one: int, space_power: int):
    features = np.zeros(shape=(number_of_one + 1, 2 ** space_power * number_of_one))
    idx = np.arange(0, 2 ** space_power * number_of_one)
    central_idx_one = np.random.choice(idx, number_of_one, replace=False)
    features[0, central_idx_one] = 1
    other_idx_one = np.setdiff1d(idx, central_idx_one)
    for number_of_one_from_central in range(number_of_one - 1, -1, -1):
        number_of_one_from_not_central = number_of_one - number_of_one_from_central
        idx_central = np.random.choice(central_idx_one, number_of_one_from_central, replace=False)
        features[number_of_one_from_not_central, idx_central] = 1
        idx_not_central = np.random.choice(other_idx_one, number_of_one_from_not_central, replace=False)
        features[number_of_one_from_not_central, idx_not_central] = 1

    if distribution == 'logarithmic':

        # distribution is set in term of benford's law:
        # p(i) = log_(number_of_features+1)(1 + 1/i),
        # for i \in [1,number_of_features]
        p = np.array([np.log(1 + 1 / i) / np.log(number_of_one + 2)
                      for i in range(1, number_of_one + 2)])
    elif distribution == 'exponential':
        p = np.array([1 / (i + 1) ** 2 for i in range(number_of_one + 1)])
    else:
        # by default the distribution is uniform
        p = np.repeat(1 / (number_of_one + 1), number_of_one + 1)

        # rescale probability
    p /= p.sum()
    return features, p


def mssr_immediate_reward_dep(mssr: MultiScaleSR, reward_dim: int,
                              feature_reward_batch: np.ndarray,
                              central_feature: np.ndarray):
    expected_time_batch = np.zeros(shape=(len(feature_reward_batch, )))

    if mssr.kernel is not None:
        features = feature_reward_batch[:, :-reward_dim]
        rewards = feature_reward_batch[:, -reward_dim:]
        one_step_rewards = recover_reward_from_reward_memory(rewards)

        immediate_reward = np.empty(shape=(len(feature_reward_batch, )))
        for feature_number, (feature, reward, one_step_reward) in enumerate(zip(features, rewards, one_step_rewards)):
            # return sequences
            episodic_seq = mssr.set_episode(np.concatenate((feature, reward)))
            feature_seq = episodic_seq[:, :-reward_dim]
            reward_seq = episodic_seq[:, -reward_dim:]
            # time scale are supposed to be equal between reward and features MS_SR
            gammas = mssr.discount_sr
            recovered_reward = recover_reward_from_reward_memory(reward_seq)
            print(f'reward_memory = {reward_seq}')
            print(f'recovered reward = {recovered_reward}')
            pairewise_cosine_sims = 1 - cosine_similarity(np.reshape(feature, (1, -1)), feature_seq)
            sim_from_central_feature = 1 - cosine_similarity(np.reshape(central_feature, (1, -1)), feature_seq)

            central_feature_sim = sim_from_central_feature.ravel()

            central_feature_sim_freq = 1 / (central_feature_sim + sys.float_info.epsilon)
            central_feature_sim_total_freq = np.sum(central_feature_sim_freq)
            central_feature_sim_total_freq = \
                1 if central_feature_sim_total_freq <= 0.0 else central_feature_sim_total_freq
            central_feature_sim_proba = central_feature_sim_freq / central_feature_sim_total_freq

            central_feature_args_with_proba = central_feature_sim_proba * np.arange(1, mssr.nb_windows + 1)
            expected_time_from_central = np.sum(central_feature_args_with_proba)
            expected_time_batch[feature_number] = expected_time_from_central
            print(pairewise_cosine_sims)
            exp_distance = np.exp(-pairewise_cosine_sims)
            total_sum_spatiotemporal = np.sum(exp_distance * (1 - gammas))
            total_sum_spatiotemporal = 1 if total_sum_spatiotemporal == 0 else total_sum_spatiotemporal
            immediate_reward[feature_number] = \
                np.sum(exp_distance * (1 - gammas) * recovered_reward) / total_sum_spatiotemporal
        return immediate_reward, expected_time_batch
    else:
        return np.zeros(shape=(len(feature_reward_batch, ))), expected_time_batch


def feature_generation(distribution: str,
                       feature_type: str,
                       number_of_features: int,
                       features_dim: int):
    possible_features = []
    if feature_type == 'binary':
        for i in range(number_of_features):
            new_features = np.random.choice([0., 1.],
                                            features_dim, p=[0.5, 0.5])
            while np.sum(new_features) == 0:
                new_features = np.random.choice([0., 1.],
                                                features_dim, p=[0.5, 0.5])
            possible_features.append(new_features)
    elif feature_type == 'binary_sparse':
        for i in range(number_of_features):
            new_features = np.random.choice([0., 1.],
                                            features_dim, p=[0.8, 0.2])
            while np.sum(new_features) == 0:
                new_features = np.random.choice([0., 1.],
                                                features_dim, p=[0.8, 0.2])
            possible_features.append(new_features)
    elif feature_type == 'one_hot':
        assert number_of_features <= features_dim, \
            "In one-hot encoded mode, " \
            "the number of features must be less or equal than feature dimension"
        for i in range(number_of_features):
            new_feature = np.zeros(shape=features_dim)
            new_feature[i] = 1
            possible_features.append(new_feature)
    possible_features = np.array(possible_features)

    if distribution == 'logarithmic':

        # distribution is set in term of benford's law:
        # p(i) = log_(number_of_features+1)(1 + 1/i),
        # for i \in [1,number_of_features]
        p = np.array([np.log(1 + 1 / i) / np.log(number_of_features + 1)
                      for i in range(1, number_of_features + 1)])
    elif distribution == 'exponential':
        p = np.array([1 / (i + 1) ** 2 for i in range(number_of_features)])
    else:
        # by default the distribution is uniform
        p = np.repeat(1 / number_of_features, number_of_features)

    # rescale probability
    p /= p.sum()
    return possible_features, p
