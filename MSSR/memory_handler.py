import sys

import numpy as np
import time
from sklearn.metrics.pairwise import cosine_similarity
import typing as typ
from MSSR.multi_scale_SR import MultiScaleSR, TCM
from MSSR.utils import memory_interpolation, \
    recover_reward_from_reward_memory, similarity_on_hypersphere
from hyper_optimizer.hyper_optimization import create_MSSR_study, \
    get_best_results_from_study
from math_utils.surprise_framework import SurpriseFramework


class MSSRHandler:

    @staticmethod
    def reward_bonus(sequence, feature):
        """
        Return a exploration score penalizing recent and well know feature
        :param batch: a batch of feature
        :type batch:
        :return:
        :rtype:
        """
        distance = 1 - cosine_similarity(np.reshape(feature, (1, -1)), sequence)
        exp_distance = np.exp(-distance)
        return 1 - exp_distance.max()

    def __init__(self, structural_parameters: dict, hyperparameters: dict):
        """
        Handle all functionality of the MS-SR memory system include:
        * Reward estimation from memory
        * Feature to feature comparison
        * sequence generation
        :param structural_parameters:
        :type structural_parameters:
        :param hyperparameters:
        :type hyperparameters:
        """

        self.structural_parameters = structural_parameters
        self.study_db = hyperparameters.get('study_db', 'default_db')
        self.study_name = hyperparameters.get('study_name',
                                              time.strftime("%Y%m%d-%H%M%S"))
        self.study_objective = hyperparameters.get('objective_func', None)
        self.hp_optimizer_number_of_trial = hyperparameters.get('number_of_trial', 100)
        self.hyperparameters = hyperparameters.get('hp_values', None)
        self.best_hp_result = hyperparameters.get('best_hp_result', 0)
        self.last_processed_feature_batch = None
        self.last_processed_reward_batch = None
        self.surprise_workflow = None
        self.processed_batch_count = 0

        if hyperparameters['pre_train_optimization']:
            self.hyperparameters, self.best_hp_result = \
                create_MSSR_study(self.study_db,
                                  self.study_name,
                                  self.study_objective,
                                  self.hp_optimizer_number_of_trial,
                                  structural_parameters)
        elif hyperparameters['recover_hp_from_study']:
            self.hyperparameters, self.best_hp_result = \
                get_best_results_from_study(self.study_db,
                                            self.study_name)
        self.use_reward_channel = True
        self.reward_estimator = \
            self.structural_parameters['reward_estimator']
        self.feature_space_dim = self.structural_parameters['feature_space_dim']
        self.reward_space_dim = self.structural_parameters['reward_space_dim']
        if self.reward_space_dim == 0:
            self.use_reward_channel = False

        nb_windows = self.structural_parameters['nb_windows']
        use_entropy = self.structural_parameters['use_entropy']

        self.hp_checking_scale_limit = \
            self.structural_parameters['hp_checking_scale_limit']
        self.hp_checking_rate = self.structural_parameters['hp_checking_rate']

        self.mssr_feature: typ.Optional[MultiScaleSR] = None
        self.mssr_reward: typ.Union[MultiScaleSR, TCM, None] = None
        if self.reward_estimator == 'reward_feature_dep':
            self.mssr_feature = MultiScaleSR(
                self.feature_space_dim + self.reward_space_dim,
                nb_windows,
                **self.hyperparameters)
        elif self.reward_estimator == 'reward_feature_indep':
            self.mssr_feature = MultiScaleSR(
                self.feature_space_dim,
                nb_windows,
                **self.hyperparameters)

            self.mssr_reward = MultiScaleSR(
                self.reward_space_dim,
                nb_windows,
                **self.hyperparameters)
        else:
            self.mssr_feature = MultiScaleSR(
                self.feature_space_dim,
                nb_windows,
                **self.hyperparameters)

            self.mssr_reward = TCM(
                nb_windows,
                **self.hyperparameters)
        if use_entropy:
            # create surprise framework  
            self.surprise_workflow = SurpriseFramework(
                self.feature_space_dim, self.mssr_feature.discount_sr[-1], False)

    def hp_optimization_is_needed(self):
        feature_memory = self.mssr_feature.last_memory_status
        if feature_memory is None:
            return False
        if self.reward_estimator:
            feature_memory = feature_memory[:, :-self.reward_space_dim]
        sim = similarity_on_hypersphere(
            self.last_processed_feature_batch, feature_memory)
        return not ((sim - self.best_hp_result) < self.hp_checking_scale_limit)

    def reward_batch_to_one_hot(self, reward_batch: np.ndarray):
        step = 2 / (self.reward_space_dim - 1)
        bins = np.floor((reward_batch + 1) / step).astype(dtype=int)
        bins = np.maximum(bins, 0)
        bins = np.minimum(bins, self.reward_space_dim - 1)
        on_hot_vector = np.zeros(shape=(reward_batch.shape[0], self.reward_space_dim))
        on_hot_vector[np.arange(len(bins)), bins] = 1
        return on_hot_vector

    def set_feature_reward_episode(self, feature, reward):
        reward = np.reshape(reward, newshape=(1, -1))
        on_hot_reward_batch = self.reward_batch_to_one_hot(reward)
        # print(f"on_hot_reward_batch = {on_hot_reward_batch}")
        on_hot_reward_batch = on_hot_reward_batch.squeeze()
        if self.reward_estimator == 'reward_feature_dep':
            feature_reward = np.concatenate((feature, on_hot_reward_batch))
            self.mssr_feature.set_episode(feature_reward)
        elif self.reward_estimator == 'reward_feature_indep':
            self.mssr_feature.set_episode(feature)
            self.mssr_reward.set_episode(on_hot_reward_batch)
        else:
            self.mssr_feature.set_episode(feature)
            self.mssr_reward.set_episode()

    def process_batch(self, feature_batch: np.ndarray,
                      reward_batch: np.ndarray, completed):
        """
        Process feature-reward batch in memory
        :param completed:
        :type completed:
        :param feature_batch: batch of feature
        :type feature_batch:
        :param reward_batch: batch of reward
        :type reward_batch:
        :return:
        :rtype:
        """
        # print(f"one_hot_reward_batch = {one_hot_reward_batch}")
        if self.surprise_workflow is not None:
            self_content_coef = self.surprise_workflow.surprise_score(feature_batch, processed_batch=True)
        else:
            self_content_coef = np.ones(shape=(feature_batch.shape[0],))

        if self.reward_space_dim != 0:
            if self.reward_estimator == 'reward_feature_dep':
                one_hot_reward_batch = self.reward_batch_to_one_hot(reward_batch)
                feature_batch = np.concatenate(
                    (feature_batch, one_hot_reward_batch), axis=1)
                self.mssr_feature.process_sample_batch(feature_batch, self_content_coef, completed)
            elif self.reward_estimator == 'reward_feature_indep':
                one_hot_reward_batch = self.reward_batch_to_one_hot(reward_batch)
                self.mssr_feature.process_sample_batch(feature_batch, self_content_coef, completed)
                self.mssr_reward.process_sample_batch(one_hot_reward_batch, self_content_coef, completed)
            else:
                self.mssr_feature.process_sample_batch(feature_batch, self_content_coef, completed)
                self.mssr_reward.process_sample_batch(reward_batch)
        else:
            self.mssr_feature.process_sample_batch(feature_batch, self_content_coef, completed)
        self.processed_batch_count += 1
        if self.surprise_workflow is not None:
            self.surprise_workflow.process_batch(inplace=True, mean_batch=feature_batch)

    def recover_episode(self, feature: np.ndarray, reward: np.ndarray):
        feature_memory = self.mssr_feature.set_episode(feature)
        if self.reward_space_dim == 0:
            reward_memory = None
        elif self.reward_estimator == 'reward_feature_dep':
            reward_memory = feature_memory[:, -self.reward_space_dim:]
        elif self.reward_estimator == 'reward_feature_indep':
            reward_memory = self.mssr_reward.set_episode(reward)
        else:
            reward_memory = self.mssr_reward.set_episode()

        return feature_memory, reward_memory

    def return_estimation(self, feature_batch: np.ndarray, reward_batch: np.ndarray):
        t_star = self.mssr_feature.t_star
        scaled_t_star = 2 * (1 - 1 / (1 + t_star))
        self.last_processed_feature_batch = feature_batch.copy()
        return_estimation = np.empty(shape=(feature_batch.shape[0],))
        for feature_number, (feature, reward) in enumerate(zip(feature_batch, reward_batch)):
            self.set_feature_reward_episode(feature, reward)
            feature_memory = self.mssr_feature.last_memory_status
            if self.reward_estimator == 'reward_feature_dep':
                feature_memory = feature_memory[:, :-self.reward_space_dim]
                reward_memory = feature_memory[:, -self.reward_space_dim:]
            else:
                reward_memory = self.mssr_reward.last_memory_status
            if self.reward_estimator == 'reward_tcm':
                recovered_reward = reward_memory
            else:
                recovered_reward = recover_reward_from_reward_memory(reward_memory)

            distance_metric = 1 - cosine_similarity(feature_memory, feature.reshape(1, -1))
            reward_discount = np.exp(-(distance_metric + scaled_t_star))
            return_estimation[feature_number] = reward + np.sum(reward_discount * recovered_reward)
        return return_estimation

    def immediate_reward_estimation(self, feature_batch: np.ndarray, reward_batch: np.ndarray):
        t_star = self.mssr_feature.t_star
        scaled_t_star = 2 * (1 - 1 / (1 + t_star))
        self.last_processed_feature_batch = feature_batch.copy()
        immediate_reward_estimation = np.empty(shape=(feature_batch.shape[0],))
        for feature_number, (feature, reward) in enumerate(zip(feature_batch, reward_batch)):
            self.set_feature_reward_episode(feature, reward)
            feature_memory = self.mssr_feature.last_memory_status
            if self.reward_estimator == 'reward_feature_dep':
                feature_memory = feature_memory[:, :-self.reward_space_dim]
                reward_memory = feature_memory[:, -self.reward_space_dim:]
            else:
                reward_memory = self.mssr_reward.last_memory_status
            if self.reward_estimator == 'reward_tcm':
                recovered_reward = reward_memory
            else:
                recovered_reward = recover_reward_from_reward_memory(reward_memory)

            distance_metric = 1 - cosine_similarity(feature_memory, feature.reshape(1, -1))
            reward_discount = np.exp(-(distance_metric + scaled_t_star))
            distance_norm_const = np.sum(reward_discount)
            distance_norm_const = 1 if distance_norm_const <= 0.0 else distance_norm_const
            immediate_reward_estimation[feature_number] = \
                np.sum(reward_discount * recovered_reward) / distance_norm_const
        return immediate_reward_estimation
