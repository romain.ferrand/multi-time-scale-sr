import typing as typ

import findiff
import numpy as np
from math import factorial
from matplotlib import pyplot as plt


class TCM:
    def __init__(self,
                 nb_windows: int,
                 t_star_min: float,
                 space_between_bins: float,
                 learning_rate: float,
                 precision: int):
        # number of time windows
        self.nb_windows: int = nb_windows
        # minimal time bin
        self.t_star_min: float = t_star_min
        # space btw bin
        self.space_between_bins: float = space_between_bins

        # learning rate for sr update gradient
        self.learning_rate: float = learning_rate
        # precision of post approximation
        self.precision: int = precision
        # Time bins, define the time scale of windows, high values means longueur time-scale
        self.t_star: np.ndarray = np.array(
            [self.t_star_min * (1 + self.space_between_bins) ** i for i in range(self.nb_windows)],
            dtype=float)
        self.sigma: np.ndarray = self.precision / self.t_star
        print(self.sigma)
        self.discount_sr: np.ndarray = np.exp(-self.sigma)
        self.F = np.zeros(shape=(self.nb_windows,))
        self.last_memory_status = np.zeros(shape=(self.nb_windows,))

    def process_sample_batch(self, input_signal_batch):
        for input_signal in input_signal_batch:
            self.F += -self.learning_rate * self.sigma * self.F + input_signal

    def set_episode(self):
        const_inv_laplace_coef = (-1) ** self.precision / factorial(self.precision) * self.sigma ** (
                self.precision + 1)
        d_dx = findiff.FinDiff(0, self.sigma, self.precision)
        f_hat = const_inv_laplace_coef * d_dx(self.F)
        self.last_memory_status = np.array(f_hat)
        return self.last_memory_status


class MultiScaleSR:
    def __init__(self,
                 feature_space_dim: int,
                 nb_windows: int,
                 t_star_min: float,
                 space_between_bins: float,
                 learning_rate: float,
                 precision: int):
        """
        Implementation of a multi-time scale successor feature representation.

        The MS-SR can compute successor features representation for a multiple time-windows.
        Each the successor features define the expected features to be perceived after a certain
        time, defined by the time-windows, when observing a specific feature at t0.

        By integrating thought time one can recover the sequences of past features that lead to
        the features observed at t0.

        Optimal choice of time-windows is taken from: https://arxiv.org/abs/1211.5189.
        Time integration is done by computing Laplace Inverse transform
        via Post inversion formula

        :param feature_space_dim: dimension of the feature space
        :type feature_space_dim: int
        :param nb_windows: number of time windows
        :type nb_windows: int
        :param t_star_min: minimal bin value
        :type t_star_min: float
        :param space_between_bins: space between between
        define as: t_star_min * (1 + space_between_bin)^i, i \in [0, nb_windows-1]
        :type space_between_bins: float
        :param learning_rate: learning rate for the TD-SR bootstrapping
        :type learning_rate: float
        :param precision: degree of derivation in Post inversion formulation
        :type precision: int

        """
        # feature space dimension
        self.feature_space_dim: int = feature_space_dim
        # number of time windows
        self.nb_windows: int = nb_windows
        # minimal time bin
        self.t_star_min: float = t_star_min
        # space btw bin
        self.space_between_bins: float = space_between_bins

        # learning rate for sr update gradient
        self.learning_rate: float = learning_rate
        # precision of post approximation
        self.precision: int = precision
        # Time bins, define the time scale of windows, high values means longueur time-scale
        self.t_star: np.ndarray = np.array(
            [self.t_star_min * (1 + self.space_between_bins) ** i for i in range(self.nb_windows)],
            dtype=float)
        self.sigma: np.ndarray = self.precision / self.t_star
        self.discount_sr: np.ndarray = np.exp(-self.sigma)

        self.w_ij_sr: np.ndarray = np.identity(n=self.feature_space_dim, dtype=float)
        self.w_ij_sr = np.tile(self.w_ij_sr, (self.nb_windows, 1))

        self.w_ij_sr = np.reshape(self.w_ij_sr, (self.nb_windows, self.feature_space_dim, self.feature_space_dim))

        self.last_feature: typ.Optional[np.ndarray] = None
        self.last_memory_status: typ.Optional[np.ndarray] = None
        self.kernel = None

    def reset(self):
        self.w_ij_sr = np.zeros(shape=(self.nb_windows, self.feature_space_dim, self.feature_space_dim),
                                dtype=float)
        self.last_feature = None

    def update_sr(self, features, self_content_coef, sr_state, sr_state_prime):
        if np.sum(features) > 0:
            # if self.add_noise_to_features:
            #     features += np.random.normal(0, 0.1, size=(features.shape[0]))
            delta = features + self.discount_sr[..., np.newaxis] * sr_state_prime - sr_state
            for i in range(self.nb_windows):
                self.w_ij_sr[i] += self.learning_rate * self_content_coef \
                                   * (features[..., np.newaxis] * delta[i])

    def new_sr_state(self, features):
        return np.dot(np.transpose(self.w_ij_sr, (0, 2, 1)), features)

    def process_step(self, self_content_coef, feature):
        if self.last_feature is not None:
            sr_state = self.new_sr_state(self.last_feature)
            next_sr_state = self.new_sr_state(feature)
            self.update_sr(self.last_feature, self_content_coef, sr_state, next_sr_state)
        self.last_feature = feature.reshape(1, self.feature_space_dim)

    def process_sample_batch(self, sample_batch: np.ndarray,
                             self_content_coef: np.ndarray, completed: bool):
        if self.last_feature is not None:
            sample_batch = np.concatenate((self.last_feature, sample_batch))

        for si_coef, prev_feature, next_feature in zip(self_content_coef[:-1],
                                                       sample_batch[:-1], sample_batch[1:]):
            sr_state = self.new_sr_state(prev_feature)
            next_sr_state = self.new_sr_state(next_feature)
            self.update_sr(prev_feature, si_coef, sr_state, next_sr_state)
        if completed:
            last_feature = sample_batch[-1]
            sr_state = self.new_sr_state(last_feature)
            next_sr_state = np.zeros_like(sr_state)
            self.update_sr(last_feature, self_content_coef[-1], sr_state, next_sr_state)
            self.last_feature = None
        else:
            self.last_feature = sample_batch[-1].reshape(1, self.feature_space_dim)

    def create_generators(self):
        const_inv_laplace_coef = (-1) ** self.precision / factorial(self.precision) * self.sigma ** (self.precision + 1)
        d_dx = findiff.FinDiff(0, self.sigma, self.precision)
        # for i in range(self.feature_space_dim):
        #     memory_seq[:, i] = const_inv_laplace_coef * d_dx(memory_seq[:, i])
        kernel = const_inv_laplace_coef[..., np.newaxis, np.newaxis] * d_dx(self.w_ij_sr)
        self.kernel = np.array(kernel)
        return self.kernel

    def set_episode(self, feature: np.ndarray, generate_kernel: bool = False):
        if generate_kernel:
            self.create_generators()
        self.last_memory_status = np.dot(np.transpose(self.kernel, (0, 2, 1)), feature)
        return self.last_memory_status


if __name__ == '__main__':
    # quick test
    tcm = MultiScaleSR(3, 10, 2, 1, 0.1, 2)
    batch = np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0],
                      [0, 1, 0], [0, 0, 1], [0, 0, 1],
                      [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
                      [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
                      [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
                      [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1]
                      ])
    coef = np.ones(shape=(batch.shape[0]))
    tcm.process_sample_batch(batch, coef, False)
    print(tcm.create_generators())
    print(tcm.set_episode(batch[0]))

    # print(tcm.w_ij_sr)
    # tcm = TCM(100, 1, 1, 1, 2)
    # signal = []
    # for _ in range(1):
    #     # generate reward
    #     reward_batch = [0] * 100 + [1] * 1 + [0] * 100 + [1] * 1 + [0] * 100
    #     reward_batch = np.array(reward_batch)
    #     signal.extend(reward_batch)
    #     tcm.process_sample_batch(reward_batch)
    # print(f'input signal = {signal}')
    # plt.figure()
    # plt.plot(signal)
    # decoded_signal = tcm.set_episode()
    # print(f'decoded signal = {decoded_signal}')
    # plt.plot(decoded_signal, scaley='log')
    # plt.show()
