import numpy as np
import time
from dtw import *
from yaml import dump

from MSSR.multi_scale_SR import MultiScaleSR, TCM
from MSSR.utils import feature_generation, similarity_on_hypersphere, \
    cosine_similarity_from_memory, mssr_return_indep, \
    reward_bonus, mssr_return_dep, overlapping_feature_generation, mssr_return_tcm, mssr_immediate_reward_dep, \
    reward_batch_to_one_hot
from math_utils.surprise_framework import SurpriseFramework
from tests.MSSR.tests_parameters import \
    hp_logarithmic_sr_1_overlapping_feature, experiment_parameters_return_dep, experiment_parameters_return_indep, \
    structural_parameters_logarithmic_overlapping_entropy, structural_parameters_logarithmic_overlapping_no_entropy, \
    hp_uniform_sr_1_overlapping_feature, structural_parameters_logarithmic_overlapping_penalty, \
    exp_parameters_reward_penalty, hp_standard, experiment_parameters_return_tmc, \
    structural_parameters_uniform_overlapping, experiment_parameters_sequence_generator, \
    structural_parameters_sequences_generator, hp_uniform_128d_20f_one_hot
from sklearn.metrics.pairwise import cosine_similarity

try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


def test_yaml():
    doc_name = 'test_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    with open(doc_name, 'w') as fd_write:
        _data = {'none': [None, None], 'int': 42, 'float': 3.1415899999999999,
                 'list': ['LITE', 'RES_ACID', 'SUS_DEXT'], 'dict': {'hp': 13, 'sp': 5},
                 'bool': [True, False, True, False]}
        dump(data=_data, stream=fd_write, Dumper=Dumper)


def binary_sequence_tree(number_of_bits: int):
    tree_depth = number_of_bits // 2
    number_of_node = 2 ** (tree_depth + 1) - 1
    tree = ['1' * number_of_bits] + [''] * (number_of_node - 1)
    number_of_leaf = (number_of_node + 1) // 2
    step_btw_bin = 2 / (number_of_leaf - 1)

    bin_values = [-1 + step_btw_bin * i for i in range(number_of_leaf)]
    true_return = [0] * (number_of_node - number_of_leaf) + bin_values
    for node in range(1, number_of_node):
        depth = int(np.floor(np.log2(node + 1)))
        print(depth)
        if node % 2 == 1:
            parent = tree[(node - 1) // 2]
        else:
            parent = tree[(node - 2) // 2]

        node_str = '10' if node % 2 == 1 else '01'
        tree[node] = parent[:(depth - 1) * 2] + node_str + parent[(depth - 1) * 2 + 2:]
    for node in range(number_of_node - 1, 0, -1):
        if node % 2 == 1:
            parent_idx = (node - 1) // 2
        else:
            parent_idx = (node - 2) // 2
        true_return[parent_idx] = 0.5 * true_return[parent_idx] + 0.5 * true_return[node]
    # int conversion
    tree = [list(map(int, list(i))) for i in tree]
    return tree_depth, tree, true_return, number_of_leaf


def binary_sequence_generator(
        number_of_sequences,
        tree, true_return):
    features_sequences = []
    true_return_sequences = []
    number_of_nodes = len(tree)
    for sequence_idx in range(number_of_sequences):
        node = 0
        features_sequence = [tree[0]]
        true_return_sequence = [true_return[0]]
        while 2 * node + 2 < number_of_nodes:
            if np.random.rand() < 0.5:
                node = 2 * node + 1
            else:
                node = 2 * node + 2
            features_sequence.append(tree[node])
            true_return_sequence.append(true_return[node])
        features_sequences.append(features_sequence)
        true_return_sequences.append(true_return_sequence)
    return np.array(features_sequences), np.array(true_return_sequences)


def test_signal_recovery(structural_parameters: dict,
                         experiment_parameters: dict,
                         hyperparameters: dict,
                         ):
    number_of_one = structural_parameters['number_of_one']
    number_of_windows = structural_parameters['number_of_windows']
    use_entropy = structural_parameters['use_entropy']
    use_noise = structural_parameters['use_noise']

    distributions = experiment_parameters['distributions']
    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']
    space_powers = experiment_parameters['space_powers']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    result = {
        'reconstruction_error': [],
        'distribution': [],
        'feature_length': []
    }
    for space_power in space_powers:
        features_dim = 2 ** space_power * number_of_one

        mssr = MultiScaleSR(features_dim,
                            number_of_windows,
                            t_min, t_btw, learning_rate, precision)
        for distribution in distributions:
            possible_features, p = overlapping_feature_generation(distribution, number_of_one, space_power)
            bounds = []
            for nb_batch in range(number_of_batch):
                idx_feature = np.random.choice(range(len(possible_features)), batch_size, p=p)
                features_batch = possible_features[idx_feature]
                mssr.process_sample_batch(features_batch)
                mssr.set_episode()
                memory = np.array(list(reversed(mssr.last_memory_status)))
                new_error = similarity_on_hypersphere(features_batch, memory)
                bounds.append(new_error)
                print(f' current reconstruction error = {new_error}')
            result['reconstruction_error'].extend(bounds)
            result['distribution'].extend([distribution] * number_of_batch)
            result['feature_length'].extend([features_dim] * number_of_batch)
    doc_name = 'test_signal_recovery_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_entropy(structural_parameters: dict,
                 experiment_parameters: dict,
                 hyperparameters: dict
                 ):
    feature_type = structural_parameters['feature_type']
    number_of_features = structural_parameters['number_of_features']
    number_of_windows = structural_parameters['number_of_windows']
    use_noise = structural_parameters['use_noise']

    distributions = experiment_parameters['distributions']
    features_dims = experiment_parameters['features_dims']
    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    result = {
        'indices': [],
        'entropy': [],
        'distance': [],
        'feature_length': []
    }
    for features_dim in features_dims:
        for distribution in distributions:
            possible_features, p = feature_generation(distribution,
                                                      feature_type, number_of_features, features_dim)

            mssr_with_entropy = MultiScaleSR(features_dim,
                                             number_of_windows,
                                             t_min, t_btw, learning_rate, precision,
                                             )
            mssr_without_entropy = MultiScaleSR(features_dim,
                                                number_of_windows,
                                                t_min, t_btw, learning_rate, precision,
                                                )

            for nb_batch in range(number_of_batch):
                idx_feature = np.random.choice(range(len(possible_features)), batch_size, p=p)
                features_batch = possible_features[idx_feature]

                mssr_without_entropy.process_sample_batch(features_batch)
                mssr_with_entropy.process_sample_batch(features_batch)

                mssr_without_entropy.set_episode()
                mssr_with_entropy.set_episode()

                memory_without_entropy = mssr_without_entropy.last_memory_status
                memory_with_entropy = mssr_with_entropy.last_memory_status

                sim_vector_without_entropy = cosine_similarity_from_memory(memory_without_entropy, possible_features)
                sim_vector_with_entropy = cosine_similarity_from_memory(memory_with_entropy, possible_features)

                indices = np.repeat(np.arange(0, number_of_features), repeats=number_of_windows)
                indices = indices.ravel()

                result['indices'].extend(indices)
                result['indices'].extend(indices)

                sim_vector_without_entropy = sim_vector_without_entropy.T
                sim_vector_without_entropy = sim_vector_without_entropy.ravel()
                sim_vector_with_entropy = sim_vector_with_entropy.T
                sim_vector_with_entropy = sim_vector_with_entropy.ravel()
                result['distance'].extend(sim_vector_without_entropy)
                result['distance'].extend(sim_vector_with_entropy)
                result['entropy'].extend([False] * number_of_features * number_of_windows)
                result['entropy'].extend([True] * number_of_features * number_of_windows)

                result['feature_length'].extend([features_dim] * number_of_features * 2 * number_of_windows)

    doc_name = 'test_entropy_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_entropy_overlapping(structural_parameters: dict,
                             experiment_parameters: dict,
                             hyperparameters: dict
                             ):
    number_of_one = structural_parameters['number_of_one']
    number_of_windows = structural_parameters['number_of_windows']
    use_noise = structural_parameters['use_noise']

    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']
    distributions = experiment_parameters['distributions']
    space_powers = experiment_parameters['space_powers']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    result = {
        'indices': [],
        'entropy': [],
        'distance': [],
        'feature_length': []
    }
    for space_power in space_powers:
        features_dim = 2 ** space_power * number_of_one
        for distribution in distributions:
            possible_features, p = overlapping_feature_generation(distribution, number_of_one, space_power)

            mssr_with_entropy = MultiScaleSR(features_dim,
                                             number_of_windows,
                                             t_min, t_btw, learning_rate, precision,
                                             )
            mssr_without_entropy = MultiScaleSR(features_dim,
                                                number_of_windows,
                                                t_min, t_btw, learning_rate, precision,
                                                )

            for nb_batch in range(number_of_batch):
                idx_feature = np.random.choice(range(len(possible_features)), batch_size, p=p)
                features_batch = possible_features[idx_feature]

                mssr_without_entropy.process_sample_batch(features_batch)
                mssr_with_entropy.process_sample_batch(features_batch)

                mssr_without_entropy.set_episode()
                mssr_with_entropy.set_episode()

                memory_without_entropy = mssr_without_entropy.last_memory_status
                memory_with_entropy = mssr_with_entropy.last_memory_status

                sim_vector_without_entropy = cosine_similarity_from_memory(memory_without_entropy, possible_features)
                sim_vector_with_entropy = cosine_similarity_from_memory(memory_with_entropy, possible_features)

                indices = np.repeat(np.arange(0, features_dim), repeats=batch_size)
                indices = indices.ravel()

                result['indices'].extend(indices)
                result['indices'].extend(indices)

                sim_vector_without_entropy = sim_vector_without_entropy.T
                sim_vector_without_entropy = sim_vector_without_entropy.ravel()
                sim_vector_with_entropy = sim_vector_with_entropy.T
                sim_vector_with_entropy = sim_vector_with_entropy.ravel()
                result['distance'].extend(sim_vector_without_entropy)
                result['distance'].extend(sim_vector_with_entropy)
                result['entropy'].extend([False] * features_dim * batch_size)
                result['entropy'].extend([True] * features_dim * batch_size)

                result['feature_length'].extend([features_dim] * features_dim * 2 * batch_size)

    doc_name = 'test_entropy_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_reward_bonus(structural_parameters: dict,
                      experiment_parameters: dict,
                      hyperparameters: dict):
    distribution = structural_parameters['distribution']
    feature_type = structural_parameters['feature_type']
    number_of_features = structural_parameters['number_of_features']
    features_dim = structural_parameters['features_dim']
    number_of_windows = structural_parameters['number_of_windows']

    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    mssr_without_entropy = MultiScaleSR(features_dim,
                                        number_of_windows,
                                        t_min, t_btw, learning_rate, precision,
                                        )
    mssr_with_entropy = MultiScaleSR(features_dim,
                                     number_of_windows,
                                     t_min, t_btw, learning_rate, precision,
                                     )
    possible_features, p = feature_generation(distribution, feature_type,
                                              number_of_features, features_dim)
    results = {
        'indices': [],
        'entropy': [],
        'bonus': [],
        'coef': [],
        'proba': [],
        'batch_number': [],
    }

    for i in range(number_of_batch):
        results['batch_number'].extend([i] * batch_size)
        results['batch_number'].extend([i] * batch_size)

        idx = np.random.choice(list(range(len(possible_features))), batch_size, p=p)
        results['indices'].extend(idx)
        results['indices'].extend(idx)

        results['proba'].extend(p[idx])
        results['proba'].extend(p[idx])

        batch = possible_features[idx]
        batch_penalty_without_entropy = reward_bonus(mssr_without_entropy, batch)
        batch_penalty_with_entropy = reward_bonus(mssr_with_entropy, batch)

        results['penalty'].extend(batch_penalty_without_entropy)
        results['penalty'].extend(batch_penalty_with_entropy)

        coef_without_entropy = mssr_without_entropy.process_sample_batch(batch)
        coef_with_entropy = mssr_with_entropy.process_sample_batch(batch)

        results['coef'].extend(coef_without_entropy)
        if i == number_of_batch - 1:
            # last feature coef of the last batch is not computed
            # we set it as NAN
            results['coef'].append(np.NAN)
        results['coef'].extend(coef_with_entropy)
        if i == number_of_batch - 1:
            # last feature coef of the last batch is not computed
            # we set it as NAN
            results['coef'].append(np.NAN)

        results['entropy'].extend([False] * batch_size)
        results['entropy'].extend([True] * batch_size)

        mssr_without_entropy.set_episode()
        mssr_with_entropy.set_episode()

    results['index'] = list(range(len(results['indices'])))
    doc_name = 'test_reward_penalty_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    output_data = {
        'results': results,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_reward_bonus_overlapping(structural_parameters: dict,
                                  experiment_parameters: dict,
                                  hyperparameters: dict):
    distribution = structural_parameters['distribution']
    space_power = structural_parameters['space_power']
    number_of_one = structural_parameters['number_of_one']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = 2 ** space_power * number_of_one

    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    mssr_without_surprise = MultiScaleSR(10,
                                         number_of_windows,
                                         t_min, t_btw, learning_rate, precision,
                                         )
    mssr_with_surprise = MultiScaleSR(10,
                                      number_of_windows,
                                      t_min, t_btw, learning_rate, precision,
                                      )
    gamma = mssr_with_surprise.discount_sr[-1]
    surprise_framework = SurpriseFramework(features_dim, gamma, False)
    # simulate 'number_of_features' sparse features
    possible_features, p = feature_generation(distribution, "one_hot", 10, 10)
    print(p)
    results = {
        'indices': [],
        'surprise': [],
        'bonus': [],
        # 'coef': [],
        'proba': [],
        'batch_number': [],
    }

    for i in range(number_of_batch):
        print(i)
        results['batch_number'].extend([i] * batch_size)
        results['batch_number'].extend([i] * batch_size)

        idx = np.random.choice(list(range(len(possible_features))), batch_size, p=p)
        results['indices'].extend(idx)
        results['indices'].extend(idx)

        results['proba'].extend(p[idx])
        results['proba'].extend(p[idx])

        batch = possible_features[idx]
        coef_with_surprise = surprise_framework.process_batch(inplace=False, mean_batch=batch)
        coef_without_surprise = np.ones_like(batch)
        surprise_framework.process_batch(inplace=True, mean_batch=batch)
        mssr_without_surprise.process_sample_batch(batch, coef_without_surprise, False)
        mssr_with_surprise.process_sample_batch(batch, coef_with_surprise, False)
        mssr_with_surprise.create_generators()
        mssr_without_surprise.create_generators()

        batch_penalty_without_surprise = reward_bonus(mssr_without_surprise, batch)
        batch_penalty_with_surprise = reward_bonus(mssr_with_surprise, batch)

        results['bonus'].extend(batch_penalty_without_surprise)
        results['bonus'].extend(batch_penalty_with_surprise)

        # results['coef'].extend(coef_without_surprise)
        # if i == number_of_batch - 1:
        #     # last feature coef of the last batch is not computed
        #     # we set it as NAN
        #     results['coef'].append(np.NAN)
        # results['coef'].extend(coef_with_surprise)
        # if i == number_of_batch - 1:
        #     # last feature coef of the last batch is not computed
        #     # we set it as NAN
        #     results['coef'].append(np.NAN)

        results['surprise'].extend([False] * batch_size)
        results['surprise'].extend([True] * batch_size)

    results['index'] = list(range(len(results['indices'])))
    doc_name = 'test_reward_bonus_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    output_data = {
        'results': results,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters
    }
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_return_on_sequences(structural_parameters: dict,
                             experiment_parameters: dict,
                             hyperparameters: dict):
    number_of_one = structural_parameters['number_of_one']
    number_of_windows = structural_parameters['number_of_windows']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    number_of_batch = experiment_parameters['number_of_batch']
    number_of_sequences = experiment_parameters['number_of_sequences']
    tree_depth, tree, true_return, reward_dim = binary_sequence_tree(number_of_one)
    mssr = MultiScaleSR(number_of_one + reward_dim, number_of_windows,
                        t_min, t_btw, learning_rate, precision)
    # zero reward is the closest bin from zero
    # first batch
    result = {
        'returns': [],
        'indices': [],
        'batch_number': [],
        'immediate_rewards': [],
    }

    def DFS_indices(k, indices, node):
        indices[node] = k
        if 2 * node + 1 < len(indices):
            k += 1
            k = DFS_indices(k, indices, 2 * node + 1)
        if 2 * node + 2 < len(indices):
            k += 1
            k = DFS_indices(k, indices, 2 * node + 2)
        return k

    tree_indices = np.zeros(shape=(len(tree)))
    DFS_indices(0, tree_indices, 0)

    for i in range(number_of_batch):
        features_sequences, true_return_sequences = binary_sequence_generator(number_of_sequences,
                                                                              tree, true_return)
        last_rewards = np.ravel(true_return_sequences[:, -1])
        sequence_rewards = np.zeros(shape=(number_of_sequences, tree_depth + 1))

        sequence_rewards[:, -1] = last_rewards
        for feature_sequence, reward_sequence in zip(features_sequences, sequence_rewards):
            rewards = reward_batch_to_one_hot(reward_dim, reward_sequence)
            batch_with_reward = np.concatenate((feature_sequence, rewards), axis=1)
            mssr.process_sample_batch(batch_with_reward, np.ones(shape=(len(feature_sequence),)), True)
            mssr.create_generators()
            tree_rewards = np.array(true_return)
            tree_rewards[:-reward_dim] = 0.0
            tree_rewards = reward_batch_to_one_hot(reward_dim, tree_rewards)
            batch_with_reward = np.concatenate((tree, tree_rewards), axis=1)
            batch_returns, immediate_reward_approx = mssr_return_dep(mssr, reward_dim, batch_with_reward)
            result['indices'].extend(tree_indices)
            result['batch_number'].extend([i] * len(tree))
            result['returns'].extend(batch_returns)
            result['immediate_rewards'].extend(immediate_reward_approx)

    result['index'] = list(range(len(result['indices'])))
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
        'true_return': true_return,
        'tree_indices': tree_indices,
    }
    doc_name = 'test_reward_signal_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_return_signal_indep(structural_parameters: dict,
                             experiment_parameters: dict,
                             hyperparameters: dict):
    distribution = structural_parameters['distribution']
    number_of_one = structural_parameters['number_of_one']
    space_power = structural_parameters['space_power']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = 2 ** space_power * number_of_one
    reward_dim = structural_parameters.get('reward_dim', features_dim)
    use_entropy = structural_parameters['use_entropy']
    use_noise = structural_parameters['use_noise']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    initial_batch_size = experiment_parameters['initial_batch_size']
    central_reward_bin_in_initial_batch = experiment_parameters['central_reward_bin_in_initial_batch']
    central_reward_bin_in_normal_batch = experiment_parameters['central_reward_bin_in_normal_batch']
    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']

    mssr_feature = MultiScaleSR(features_dim, number_of_windows,
                                t_min, t_btw, learning_rate, precision)
    mssr_reward = MultiScaleSR(reward_dim, number_of_windows,
                               t_min, t_btw, learning_rate, precision, )

    possible_features, p = overlapping_feature_generation(distribution,
                                                          number_of_one,
                                                          space_power)

    central_feature_idx = np.random.choice(list(range(len(possible_features))), size=1, p=p)
    central_feature = np.ravel(possible_features[central_feature_idx])
    rewards = np.empty(shape=(initial_batch_size, reward_dim))
    central_reward = np.zeros(shape=reward_dim)
    zero_reward = np.zeros(shape=reward_dim)
    # central reward = -1 reward
    central_reward[central_reward_bin_in_initial_batch] = 1
    # zero reward is the closest bin from zero
    zero_reward[int(np.floor(reward_dim / 2))] = 1
    # first batch
    batch_idx = np.random.choice(list(range(len(possible_features))),
                                 size=initial_batch_size, p=p)
    batch = possible_features[batch_idx]
    for i in range(initial_batch_size):
        rewards[i] = central_reward.copy() \
            if np.array_equal(batch[i], central_feature) else zero_reward.copy()
    mssr_feature.process_sample_batch(batch)
    mssr_reward.process_sample_batch(rewards)
    result = {
        'similarities_from_central_feature': [],
        'returns': [],
        'indices': [],
        'batch_number': [],
        'expected_time_from_central': [],
    }
    central_reward = np.zeros(shape=reward_dim)
    central_reward[central_reward_bin_in_normal_batch] = 1

    for i in range(number_of_batch):
        mssr_feature.set_episode()
        mssr_reward.set_episode()

        batch_idx = np.random.choice(list(range(len(possible_features))),
                                     size=batch_size, p=p)
        batch = possible_features[batch_idx]
        rewards = np.empty(shape=(batch_size, reward_dim))
        print(f'reward generation')
        for idx_reward in range(batch_size):
            rewards[idx_reward] = central_reward.copy() \
                if np.array_equal(batch[idx_reward], central_feature) else zero_reward.copy()
            print(np.sum(rewards[idx_reward]))
        feature_memory = mssr_feature.last_memory_status.copy()
        rewards_memory = mssr_reward.last_memory_status.copy()
        batch_returns = mssr_return_indep(mssr_feature, mssr_reward, batch)
        mssr_reward.process_sample_batch(rewards)
        mssr_feature.process_sample_batch(batch)

        central_feature_sim = cosine_similarity_from_memory(
            central_feature.reshape(1, -1), feature_memory)
        central_feature_sim = central_feature_sim.ravel()
        central_feature_sim_freq = 1 / (central_feature_sim + sys.float_info.epsilon)
        central_feature_sim_total_freq = np.sum(central_feature_sim_freq)
        central_feature_sim_proba = central_feature_sim_freq / central_feature_sim_total_freq
        central_feature_args_with_proba = central_feature_sim_proba * np.arange(1, number_of_windows + 1)
        expected_time_from_central = np.sum(central_feature_args_with_proba)
        print(f'expected_time_from_central = {expected_time_from_central}')

        similarities_from_batch = cosine_similarity_from_memory(
            central_feature.reshape(1, -1), batch)
        result['expected_time_from_central'].extend([expected_time_from_central] * batch_size)
        result['indices'].extend(batch_idx)
        result['similarities_from_central_feature'].extend(similarities_from_batch.ravel())
        result['batch_number'].extend([i] * batch_size)
        result['returns'].extend(batch_returns)

        print(f'feature memory len = {feature_memory.shape}')
        print(f'central feature shape = {central_feature.shape}')

        print(f" Reward memory = \n {rewards_memory}")
        print(f" Reward memory shape = {rewards_memory.shape}")

        print(f" Similarities = \n {similarities_from_batch.ravel()}")
        print(f" Similarities shape = {similarities_from_batch.shape}")
        print(f"Central feature times = \n {central_feature_sim.ravel()}")

        print(f" batch_returns = \n {batch_returns}")
        print(f" batch_returns shape = {batch_returns.shape}")

    result['index'] = list(range(len(result['indices'])))
    print(result['returns'])
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    doc_name = 'test_reward_signal_sequence_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_return_signal_dep(structural_parameters: dict,
                           experiment_parameters: dict,
                           hyperparameters: dict):
    distribution = structural_parameters['distribution']
    number_of_one = structural_parameters['number_of_one']
    space_power = structural_parameters['space_power']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = 2 ** space_power * number_of_one
    reward_dim = structural_parameters.get('reward_dim', features_dim)

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    initial_batch_size = experiment_parameters['initial_batch_size']
    central_reward_bin_in_initial_batch = experiment_parameters['central_reward_bin_in_initial_batch']
    central_reward_bin_in_normal_batch = experiment_parameters['central_reward_bin_in_normal_batch']
    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']
    use_entropy = experiment_parameters['use_entropy']
    use_noise = experiment_parameters['use_noise']

    mssr = MultiScaleSR(features_dim + reward_dim, number_of_windows,
                        t_min, t_btw, learning_rate, precision)

    possible_features, p = overlapping_feature_generation(distribution, number_of_one, space_power)

    central_feature = np.ravel(possible_features[0])
    rewards = np.empty(shape=(initial_batch_size, reward_dim))
    central_reward = np.zeros(shape=reward_dim)
    zero_reward = np.zeros(shape=reward_dim)
    # central reward
    central_reward[central_reward_bin_in_initial_batch] = 1
    # zero reward is the closest bin from zero
    zero_reward[int(np.floor(reward_dim / 2))] = 1
    # first batch
    p = p[::-1]
    batch_idx = np.random.choice(list(range(len(possible_features))), size=initial_batch_size, p=p)
    batch = possible_features[batch_idx]
    for i in range(initial_batch_size):
        rewards[i] = central_reward.copy() \
            if np.array_equal(batch[i], central_feature) else zero_reward.copy()
    batch_with_reward = np.concatenate((batch, rewards), axis=1)
    mssr.process_sample_batch(batch_with_reward, np.ones(shape=(batch.shape[0],)), True)
    mssr.create_generators()
    result = {
        'similarities_from_central_feature': [],
        'returns': [],
        'indices': [],
        'batch_number': [],
        'expected_time_from_central': [],
    }
    central_reward = np.zeros(shape=reward_dim)
    central_reward[central_reward_bin_in_normal_batch] = 1
    for i in range(number_of_batch):
        batch_idx = np.random.choice(list(range(len(possible_features))), size=batch_size, p=p)
        batch = possible_features[batch_idx]
        rewards = np.empty(shape=(batch_size, reward_dim))
        print(f'reward generation')
        for idx_reward in range(batch_size):
            rewards[idx_reward] = central_reward.copy() \
                if np.array_equal(batch[idx_reward], central_feature) else zero_reward.copy()
        batch_with_reward = np.concatenate((batch, rewards), axis=1)
        mssr.process_sample_batch(batch_with_reward, np.ones(shape=(batch.shape[0],)), False)
        mssr.create_generators()
        batch_returns, expected_time_batch = mssr_return_dep(mssr, reward_dim, batch_with_reward, central_feature)

        similarities_from_batch = 1 - cosine_similarity(
            central_feature.reshape(1, -1), batch)

        result['expected_time_from_central'].extend(expected_time_batch)
        result['indices'].extend(batch_idx)
        result['similarities_from_central_feature'].extend(similarities_from_batch.ravel())
        result['batch_number'].extend([i] * batch_size)
        result['returns'].extend(batch_returns)

    result['index'] = list(range(len(result['indices'])))
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    doc_name = 'test_reward_signal_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_return_signal(structural_parameters: dict,
                       experiment_parameters: dict,
                       hyperparameters: dict):
    if experiment_parameters['reward_estimator'] == 'reward_feature_dep':
        test_return_signal_dep(structural_parameters, experiment_parameters, hyperparameters)
    elif experiment_parameters['reward_estimator'] == 'reward_feature_indep':
        test_return_signal_indep(structural_parameters, experiment_parameters, hyperparameters)


def test_immediate_reward(structural_parameters: dict,
                          experiment_parameters: dict,
                          hyperparameters: dict):
    distribution = structural_parameters['distribution']
    number_of_one = structural_parameters['number_of_one']
    space_power = structural_parameters['space_power']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = 2 ** space_power * number_of_one
    reward_dim = structural_parameters.get('reward_dim', features_dim)

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    initial_batch_size = experiment_parameters['initial_batch_size']
    central_reward_bin_in_initial_batch = experiment_parameters['central_reward_bin_in_initial_batch']
    central_reward_bin_in_normal_batch = experiment_parameters['central_reward_bin_in_normal_batch']
    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']
    use_entropy = experiment_parameters['use_entropy']
    use_noise = experiment_parameters['use_noise']

    mssr = MultiScaleSR(features_dim + reward_dim, number_of_windows,
                        t_min, t_btw, learning_rate, precision)

    possible_features, p = overlapping_feature_generation(distribution, number_of_one, space_power)

    central_feature = np.ravel(possible_features[0])
    rewards = np.empty(shape=(initial_batch_size, reward_dim))
    central_reward = np.zeros(shape=reward_dim)
    zero_reward = np.zeros(shape=reward_dim)
    # central reward
    central_reward[central_reward_bin_in_initial_batch] = 1
    # zero reward is the closest bin from zero
    zero_reward[int(np.floor(reward_dim / 2))] = 1
    # first batch
    batch_idx = np.random.choice(list(range(len(possible_features))), size=initial_batch_size, p=p)
    batch = possible_features[batch_idx]
    for i in range(initial_batch_size):
        rewards[i] = central_reward.copy() \
            if np.array_equal(batch[i], central_feature) else zero_reward.copy()
    batch_with_reward = np.concatenate((batch, rewards), axis=1)
    mssr.process_sample_batch(batch_with_reward, np.ones(shape=(batch.shape[0],)), True)
    mssr.create_generators()
    result = {
        'similarities_from_central_feature': [],
        'immediate_reward': [],
        'indices': [],
        'batch_number': [],
        'expected_time_from_central': [],
    }
    central_reward = np.zeros(shape=reward_dim)
    central_reward[central_reward_bin_in_normal_batch] = 1
    for i in range(number_of_batch):
        batch_idx = np.random.choice(list(range(len(possible_features))), size=batch_size, p=p)
        batch = possible_features[batch_idx]
        rewards = np.empty(shape=(batch_size, reward_dim))
        print(f'reward generation')
        for idx_reward in range(batch_size):
            rewards[idx_reward] = central_reward.copy() \
                if np.array_equal(batch[idx_reward], central_feature) else zero_reward.copy()
        batch_with_reward = np.concatenate((batch, rewards), axis=1)
        mssr.process_sample_batch(batch_with_reward, np.ones(shape=(batch.shape[0],)), False)
        mssr.create_generators()
        immediate_reward, expected_time_batch = mssr_immediate_reward_dep(
            mssr, reward_dim, batch_with_reward, central_feature)

        similarities_from_batch = 1 - cosine_similarity(
            central_feature.reshape(1, -1), batch)

        result['expected_time_from_central'].extend(expected_time_batch)
        result['indices'].extend(batch_idx)
        result['similarities_from_central_feature'].extend(similarities_from_batch.ravel())
        result['batch_number'].extend([i] * batch_size)
        result['immediate_reward'].extend(immediate_reward)

    result['index'] = list(range(len(result['indices'])))
    output_data = {
        'results': result,
        'structural_parameters': structural_parameters,
        'hyperparameters': hyperparameters,
        'experiment_parameters': experiment_parameters,
    }
    doc_name = 'test_immediate_reward_' + time.strftime("%Y%m%d-%H%M%S") + '.yaml'
    with open(doc_name, 'w') as fd_write:
        dump(data=output_data, stream=fd_write, Dumper=Dumper)


def test_signal_recovery_with_HP(structural_parameters: dict, hyperparameters: dict,
                                 experiment_parameters: dict):
    distribution = structural_parameters['distribution']
    feature_type = structural_parameters['feature_type']
    number_of_features = structural_parameters['number_of_features']
    number_of_windows = structural_parameters['number_of_windows']
    features_dim = structural_parameters['features_dim']
    use_entropy = structural_parameters['use_entropy']
    use_noise = structural_parameters['use_noise']

    number_of_batch = experiment_parameters['number_of_batch']
    batch_size = experiment_parameters['batch_size']

    t_min = hyperparameters['t_min']
    t_btw = hyperparameters['t_btw']
    learning_rate = hyperparameters['learning_rate']
    precision = hyperparameters['precision']

    mssr = MultiScaleSR(features_dim,
                        number_of_windows,
                        t_min, t_btw, learning_rate, precision)
    possible_features, p = feature_generation(distribution,
                                              feature_type,
                                              number_of_features,
                                              features_dim)
    signal = []
    indices = []
    for i in range(number_of_batch):
        idx = np.random.choice(list(range(number_of_features)), batch_size, p=p)
        batch = possible_features[idx]
        indices.extend(idx)
        signal.extend(batch)
        mssr.process_sample_batch(batch)
    mssr.set_episode()
    memory = list(reversed(mssr.last_memory_status))
    alignment = dtw(memory, signal[:-1], dist_method='cosine',
                    keep_internals=True)
    alignment.plot(type='density')


if __name__ == '__main__':
    # test_yaml()
    # test_return_signal(structural_parameters=structural_parameters_logarithmic_overlapping_no_entropy,
    #                    experiment_parameters=experiment_parameters_return_dep,
    #                    hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_return_signal(structural_parameters=structural_parameters_logarithmic_overlapping_no_entropy,
    #                    experiment_parameters=experiment_parameters_return_indep,
    #                    hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_return_signal(structural_parameters=structural_parameters_logarithmic_overlapping_entropy,
    #                    experiment_parameters=experiment_parameters_return_dep,
    #                    hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_return_signal(structural_parameters=structural_parameters_logarithmic_overlapping_entropy,
    #                    experiment_parameters=experiment_parameters_return_indep,
    #                    hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_return_signal(structural_parameters=structural_parameters_logarithmic_overlapping_entropy,
    #                    experiment_parameters=experiment_parameters_return_dep,
    #                    hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_reward_penalty(structural_parameters=structural_parameters_uniform_sparse_log_penalty,
    #                     hyperparameters=hp_uniform_128d_20f_one_hot)
    # test_reward_penalty(structural_parameters=structural_parameters_exponential_overlapping_entropy_penalty,
    #                     hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # test_signal_recovery(structural_parameters=structural_parameters_sr_1_overlapping,
    #                      hyperparameters=hp_uniform_sr_1_overlapping_feature,
    #                      experiment_parameters=experiment_parameters_sr_1)
    test_reward_bonus_overlapping(structural_parameters_logarithmic_overlapping_penalty,
                                  exp_parameters_reward_penalty,
                                  hp_uniform_128d_20f_one_hot)
    # test_entropy(structural_parameters_entropy,
    #              experiment_parameters_entropy,
    #              hp_uniform_128d_20f_one_hot)
    # Immediate reward tests:
    # test_immediate_reward(structural_parameters=structural_parameters_logarithmic_overlapping_entropy,
    #                       experiment_parameters=experiment_parameters_return_dep,
    #                       hyperparameters=hp_logarithmic_sr_1_overlapping_feature)
    # tree_depth, tree, true_return, number_of_leaf = binary_sequence_tree(8)
    # print(true_return)
    # features_sequences, true_return_sequences = binary_sequence_generator(100, tree, true_return)
    # print(tree)
    # print(features_sequences)
    # print(true_return_sequences)
    # test_return_on_sequences(structural_parameters_sequences_generator, experiment_parameters_sequence_generator, hp_logarithmic_sr_1_overlapping_feature)
