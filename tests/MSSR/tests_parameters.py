#
# Generals hyperparameters
#
hp_standard = {
    'learning_rate': 0.01,
    'precision': 8,
    't_btw': 0.015,
    't_min': 1

}
hp_uniform_sparse_hd = {
    'learning_rate': 0.00010,
    'precision': 2,
    't_btw': 0.015,
    't_min': 0.75
}
hp_uniform_128d_20f_binary_sparse = {
    'learning_rate': 0.0046,
    'precision': 26,
    't_btw': 0.00995,
    't_min': 0.913}
hp_uniform_128d_20f_one_hot = {
    'learning_rate': 0.001,
    'precision': 18,
    't_btw': 0.5,
    't_min': 1.0
}
#
# Parameters for signal entropy tests:
#
structural_parameters_entropy = {
    'feature_type': 'one_hot',
    'number_of_features': 5,
    'number_of_windows': 100,
    'use_noise': False,
}
experiment_parameters_entropy = {
    'distributions': ['exponential'],
    'features_dims': [128],
    'number_of_batch': 10,
    'batch_size': 256
}
#
# Parameters for signal recovery tests:
#
structural_parameters_uniform_sparse = {
    'distribution': 'uniform',
    'feature_type': 'binary_sparse',
    'number_of_features': 20,
    'number_of_windows': 100,
    'features_dim': 128,
    'use_entropy': False, 'use_noise': False
}
structural_parameters_uniform_one_hot = {
    'distribution': 'uniform',
    'feature_type': 'one_hot',
    'number_of_features': 20,
    'number_of_windows': 100,
    'use_entropy': False, 'use_noise': False
}
signal_recovery_experiment_parameters = {
    'batch_size': 1001,
    'number_of_batch': 1,
}
#
# Parameters for MSSR return tests
#
structural_parameters_sequences_generator = {
    'number_of_one': 8,
    'number_of_windows': 100,

}
structural_parameters_uniform_sparse_with_reward_dim = {
    'distribution': 'uniform',
    'feature_type': 'binary_sparse',
    'number_of_features': 20,
    'reward_dim': 9,
    'number_of_windows': 100,
    'features_dim': 128,
    'number_of_batch': 10,
    'batch_size': 10,
    'use_entropy': True, 'use_noise': False,
}
experiment_parameters_sequence_generator = {
    'number_of_batch': 10,
    'number_of_sequences': 100,
}
experiment_parameters_return_dep = {
    'number_of_batch': 10,
    'batch_size': 100,
    'initial_batch_size': 100,
    'central_reward_bin_in_initial_batch': 4,
    'central_reward_bin_in_normal_batch': -1,
    'reward_estimator': 'reward_feature_dep',
    'use_entropy': True,
    'use_noise': False
}
experiment_parameters_return_indep = {
    'number_of_batch': 100,
    'batch_size': 100,
    'initial_batch_size': 1000,
    'central_reward_bin_in_initial_batch': 4,
    'central_reward_bin_in_normal_batch': -1,
    'reward_estimator': 'reward_feature_indep',
    'use_entropy': True,
    'use_noise': False
}
experiment_parameters_return_tmc = {
    'number_of_batch': 100,
    'batch_size': 100,
    'initial_batch_size': 1000,
    'initial_central_reward': 0,
    'batch_central_reward': 1,

    'reward_estimator': 'tcm',
    'use_entropy': True,
    'use_noise': False
}
#
# Parameters for handcrafted overlapping features
#
structural_parameters_logarithmic_overlapping_no_entropy = {
    'distribution': 'logarithmic',
    'space_power': 1,
    'number_of_one': 10,
    'reward_dim': 9,
    'number_of_windows': 100,
    'number_of_batch': 100,
    'batch_size': 100,
    'use_entropy': False, 'use_noise': False,
}
structural_parameters_logarithmic_overlapping_entropy = {
    'distribution': 'logarithmic',
    'space_power': 1,
    'number_of_one': 10,
    'reward_dim': 9,
    'number_of_windows': 100,
}

structural_parameters_uniform_overlapping = {
    'distribution': 'uniform',
    'space_power': 1,
    'number_of_one': 10,
    'reward_dim': 9,
    'number_of_windows': 100,
}

#
# Test penalty signal
#

# Structural parameters:
structural_parameters_logarithmic_overlapping_penalty = {
    'distribution': 'logarithmic',
    'space_power': 1,
    'number_of_one': 10,
    'reward_dim': 9,
    'number_of_windows': 100,
}
structural_parameters_exponential_overlapping_penalty = {
    'distribution': 'exponential',
    'space_power': 1,
    'number_of_one': 10,
    'reward_dim': 9,
    'number_of_windows': 100,
}

# Experiments parameters
exp_parameters_reward_penalty = {
    'number_of_batch': 20,
    'batch_size': 1000,
    'use_noise': False,
}

#
# Test signal reconstruction 1
#

# Structural parameters
structural_parameters_sr_1_overlapping = {
    'number_of_one': 10,
    'number_of_windows': 100,
    'use_entropy': False, 'use_noise': False,
}
structural_parameters_uniformHypersphere_1 = {
    "number_of_windows": 100,
    "features_dim": 128,
    "number_of_batch": 10,
    "batch_size": 512,
}
structural_parameters_uniformHypersphere_low_D = {
    "number_of_windows": 100,
    "features_dim": 5,
    "number_of_batch": 100,
    "batch_size": 128,
    "min_max_uniform_sample": (10, 100),
    "number_of_mixtures": 10,
}

# Hyperparameters
hp_uniform_sr_1_overlapping_feature = {
    't_min': 0.74,
    't_btw': 0.05,
    'learning_rate': 0.00024,
    'precision': 26
}
hp_logarithmic_sr_1_overlapping_feature = {
    'learning_rate': 0.001,
    'precision': 8,
    't_btw': 0.05,
    't_min': 1}
# Experiment parameters
experiment_parameters_sr_1 = {
    'distributions': ['uniform', 'logarithmic', 'exponential'],
    'space_powers': list(range(1, 5)),
    'number_of_batch': 100,
    'batch_size': 256,
    'lower_bound': 0.288,
    'bound_precision': 0.1,
}

